﻿# <pep8 compliant>
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

import bpy
import bmesh
import math
import mathutils
from .utils import *


# ============================================================================
# DEFINE FUNCTIONS
# ============================================================================

rigifyRig = None
genesisRig = None
is_below_2_79 = float(bpy.app.version_string.split()[0]) < 2.79

# metarig bone names for compatibility with new rigify in 2.79
metarig_head = "spine.006"
metarig_neck_upper = "spine.005"
metarig_neck_base = "spine.004"
metarig_chest_upper = "spine.003"
metarig_chest = "spine.002"
metarig_spine = "spine.001"
metarig_hips = "spine"


def createVgroups(obj, bm, vgroups):
    for vg in vgroups:
        createVgroup(obj, bm, vg["name"], vg["vertList"])


def deleteVgroups(obj, vgroups):
    for vg in vgroups:
        delVgroup(obj, vg["name"])


def copyMeshPos(metarig, genesis, targetBone, headOrTail, vgroupName, offset=(0, 0, 0)):
    if targetBone not in metarig.data.bones.keys():
        print("copyMeshPos aborted: " + targetBone)
        return
    context = bpy.context
    vgroupIndex = genesis.vertex_groups[vgroupName].index
    bpy.ops.object.mode_set(mode='OBJECT')
    context.view_layer.objects.active = genesis
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_all(action='DESELECT')
    genesis.vertex_groups.active_index = vgroupIndex
    bpy.ops.object.vertex_group_select()
    bm = bmesh.from_edit_mesh(genesis.data)
    if len([v for v in bm.verts if v.select]) == 0:
        print("copyMeshPos aborted: " + vgroupName)
        return
    bpy.ops.view3d.snap_cursor_to_selected()
    cursorLoc = context.scene.cursor.location
    cursorLoc[0] += offset[0]
    cursorLoc[1] += offset[1]
    cursorLoc[2] += offset[2]
    context.scene.cursor.location = cursorLoc
    bpy.ops.mesh.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')
    context.view_layer.objects.active = metarig
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.armature.select_all(action='DESELECT')

    if headOrTail == "head":
        metarig.data.edit_bones[targetBone].select_head = True
    elif headOrTail == "tail":
        metarig.data.edit_bones[targetBone].select_tail = True

    bpy.ops.view3d.snap_selected_to_cursor()
    bpy.ops.armature.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')


def copyBonePos(metarig, genesisRig, targetBone, targetHeadOrTail, sourceBone, sourceHeadOrTail, offset=(0, 0, 0)):
    if sourceBone not in genesisRig.data.bones.keys():
        print("copyBonePos aborted. sourceBone: " + sourceBone)
        return
    if targetBone not in metarig.data.bones.keys():
        print("copyBonePos aborted. targetBone: " + targetBone)
        return
    context = bpy.context
    bpy.ops.object.mode_set(mode='OBJECT')
    context.view_layer.objects.active = genesisRig
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.armature.select_all(action='DESELECT')

    if sourceHeadOrTail == "head":
        genesisRig.data.edit_bones[sourceBone].select_head = True
    elif sourceHeadOrTail == "tail":
        genesisRig.data.edit_bones[sourceBone].select_tail = True

    bpy.ops.view3d.snap_cursor_to_selected()
    cursorLoc = context.scene.cursor.location
    cursorLoc[0] += offset[0]
    cursorLoc[1] += offset[1]
    cursorLoc[2] += offset[2]
    print("copy bone pos source: " + sourceBone)
    print("copy bone pos: " + sourceHeadOrTail)
    print("copy bone pos target: " + targetBone)
    print("copy bone pos: " + targetHeadOrTail)
    print("cursor: " + str(cursorLoc))
    context.scene.cursor.location = cursorLoc
    bpy.ops.armature.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')
    context.view_layer.objects.active = metarig
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.armature.select_all(action='DESELECT')

    if targetHeadOrTail == "head":
        metarig.data.edit_bones[targetBone].select_head = True
    elif targetHeadOrTail == "tail":
        metarig.data.edit_bones[targetBone].select_tail = True

    bpy.ops.view3d.snap_selected_to_cursor()
    bpy.ops.armature.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')


def metarigPrep(metarig):
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.context.view_layer.objects.active = metarig
    bpy.ops.object.mode_set(mode='EDIT')
    metarig.data.use_mirror_x = True
    bpy.ops.object.mode_set(mode='OBJECT')


def metarigFinishingTouches(metarig):
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.context.view_layer.objects.active = metarig
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.armature.select_all(action='DESELECT')
    metarig.data.edit_bones["heel.02.L"].tail[2] = 0
    metarig.data.edit_bones["heel.02.R"].tail[2] = 0
    metarig.data.edit_bones["heel.02.L"].tail[1] = metarig.data.edit_bones["heel.02.L"].head[1]
    metarig.data.edit_bones["heel.02.R"].tail[1] = metarig.data.edit_bones["heel.02.R"].head[1]
    # push knee forward if needed. this can help with IK
    shin = metarig.data.edit_bones["shin.L"]
    thigh = metarig.data.edit_bones["thigh.L"]
    thighR = metarig.data.edit_bones["thigh.R"]
    kneeAngle = math.degrees(
        (shin.head - shin.tail).angle(shin.head - thigh.head))
    if(kneeAngle > 175 and kneeAngle < 180):
        diff = shin.tail - thigh.head
        thigh.tail = thigh.tail + \
            mathutils.Vector((0, -diff.magnitude / 40, 0))
        thighR.tail = thighR.tail + \
            mathutils.Vector((0, -diff.magnitude / 40, 0))

    bpy.ops.armature.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')


def createFaceRig(g3):
    # we need to know if it's a g3 because of its unique skeleton
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.armature_add(
        view_align=False, enter_editmode=False, location=(0, 0, 0))
    faceRig = bpy.context.active_object
    bpy.ops.object.mode_set(mode='EDIT')

    # add tongue bones
    # the default bone is the tongue's base
    bpy.context.scene.cursor.location[0] = 0
    bpy.ops.armature.bone_primitive_add()
    bpy.context.scene.cursor.location[0] = 1
    bpy.ops.armature.bone_primitive_add()
    bpy.context.scene.cursor.location[0] = 2
    bpy.ops.armature.bone_primitive_add()
    if(g3 == False):
        bpy.context.scene.cursor.location[0] = 3
        bpy.ops.armature.bone_primitive_add()
        bpy.context.scene.cursor.location[0] = 4
        bpy.ops.armature.bone_primitive_add()
        bpy.context.scene.cursor.location[0] = 5
        bpy.ops.armature.bone_primitive_add()
    # add eye bones
    bpy.context.scene.cursor.location[0] = 6
    bpy.ops.armature.bone_primitive_add()
    bpy.context.scene.cursor.location[0] = 7
    bpy.ops.armature.bone_primitive_add()
    bpy.context.scene.cursor.location[0] = 8
    bpy.ops.armature.bone_primitive_add()
    bpy.context.scene.cursor.location[0] = 9
    bpy.ops.armature.bone_primitive_add()
    bpy.context.scene.cursor.location[0] = 10
    bpy.ops.armature.bone_primitive_add()
    bpy.context.scene.cursor.location[0] = 11

    # names
    i = 0
    if(g3 == False):
        faceRig.data.edit_bones[i].name = "DEF-tonguebase"
        i += 1
    faceRig.data.edit_bones[i].name = "DEF-tongue.01"
    i += 1
    faceRig.data.edit_bones[i].name = "DEF-tongue.02"
    i += 1
    faceRig.data.edit_bones[i].name = "DEF-tongue.03"
    i += 1
    faceRig.data.edit_bones[i].name = "DEF-tongue.04"
    i += 1
    if(g3 == False):
        faceRig.data.edit_bones[i].name = "DEF-tongue.05"
        i += 1
        faceRig.data.edit_bones[i].name = "DEF-tonguetip"
        i += 1
    faceRig.data.edit_bones[i].name = "DEF-eye.L"
    i += 1
    faceRig.data.edit_bones[i].name = "DEF-eye.R"
    i += 1
    faceRig.data.edit_bones[i].name = "IK-eye.L"
    i += 1
    faceRig.data.edit_bones[i].name = "IK-eye.R"
    i += 1
    faceRig.data.edit_bones[i].name = "IK-eyes_lookat"

    # disable deform
    faceRig.data.edit_bones["IK-eye.L"].use_deform = False
    faceRig.data.edit_bones["IK-eye.R"].use_deform = False
    faceRig.data.edit_bones["IK-eyes_lookat"].use_deform = False

    # constraints
    bpy.ops.object.mode_set(mode='POSE')

    faceRig.data.bones.active = faceRig.data.bones["DEF-eye.L"]
    bpy.ops.pose.constraint_add(type='IK')
    faceRig.pose.bones["DEF-eye.L"].constraints[-1].target = faceRig
    faceRig.pose.bones["DEF-eye.L"].constraints[-1].subtarget = "IK-eye.L"
    faceRig.pose.bones["DEF-eye.L"].constraints[-1].chain_count = 1

    faceRig.data.bones.active = faceRig.data.bones["DEF-eye.R"]
    bpy.ops.pose.constraint_add(type='IK')
    faceRig.pose.bones["DEF-eye.R"].constraints[-1].target = faceRig
    faceRig.pose.bones["DEF-eye.R"].constraints[-1].subtarget = "IK-eye.R"
    faceRig.pose.bones["DEF-eye.R"].constraints[-1].chain_count = 1

    bpy.ops.object.mode_set(mode='OBJECT')

    return faceRig


def faceRigSetParents(faceRig, g3):
    # we need to know if it's a g3 because of its unique skeleton
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.context.view_layer.objects.active = faceRig
    bpy.ops.object.mode_set(mode='EDIT')

    #faceRig.data.edit_bones["DEF-tongue.01"].parent = faceRig.data.edit_bones["DEF-head"]
    #faceRig.data.edit_bones["DEF-tongue.01"].use_connect = False

    faceRig.data.edit_bones["DEF-tongue.02"].parent = faceRig.data.edit_bones["DEF-tongue.01"]
    faceRig.data.edit_bones["DEF-tongue.02"].use_connect = True

    faceRig.data.edit_bones["DEF-tongue.03"].parent = faceRig.data.edit_bones["DEF-tongue.02"]
    faceRig.data.edit_bones["DEF-tongue.03"].use_connect = True

    faceRig.data.edit_bones["DEF-tongue.04"].parent = faceRig.data.edit_bones["DEF-tongue.03"]
    faceRig.data.edit_bones["DEF-tongue.04"].use_connect = True

    if(g3 == False):
        faceRig.data.edit_bones["DEF-tongue.01"].parent = faceRig.data.edit_bones["DEF-tonguebase"]
        faceRig.data.edit_bones["DEF-tongue.01"].use_connect = True

        faceRig.data.edit_bones["DEF-tongue.05"].parent = faceRig.data.edit_bones["DEF-tongue.04"]
        faceRig.data.edit_bones["DEF-tongue.05"].use_connect = True

        faceRig.data.edit_bones["DEF-tonguetip"].parent = faceRig.data.edit_bones["DEF-tongue.05"]
        faceRig.data.edit_bones["DEF-tonguetip"].use_connect = True

    faceRig.data.edit_bones["IK-eye.L"].parent = faceRig.data.edit_bones["IK-eyes_lookat"]
    faceRig.data.edit_bones["IK-eye.R"].parent = faceRig.data.edit_bones["IK-eyes_lookat"]


def faceRigFinishingTouches(faceRig):
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.context.view_layer.objects.active = faceRig
    bpy.ops.object.mode_set(mode='EDIT')

    # Left Eye IK bone
    bpy.ops.armature.select_all(action='DESELECT')
    bpy.context.active_object.data.edit_bones["DEF-eye.L"].select_tail = True
    bpy.ops.view3d.snap_cursor_to_selected()

    bpy.ops.armature.select_all(action='DESELECT')
    bpy.context.active_object.data.edit_bones["IK-eye.L"].select_head = True
    cursorY = bpy.context.scene.cursor.location[1]
    bpy.context.scene.cursor.location[1] = cursorY - 0.5
    bpy.ops.view3d.snap_selected_to_cursor()
    bpy.ops.armature.select_all(action='DESELECT')
    bpy.context.active_object.data.edit_bones["IK-eye.L"].select_tail = True
    cursorY = bpy.context.scene.cursor.location[1]
    bpy.context.scene.cursor.location[1] = cursorY - 0.05
    bpy.ops.view3d.snap_selected_to_cursor()

    # Right Eye IK bone
    bpy.ops.armature.select_all(action='DESELECT')
    bpy.context.active_object.data.edit_bones["DEF-eye.R"].select_tail = True
    bpy.ops.view3d.snap_cursor_to_selected()

    bpy.ops.armature.select_all(action='DESELECT')
    bpy.context.active_object.data.edit_bones["IK-eye.R"].select_head = True
    cursorY = bpy.context.scene.cursor.location[1]
    bpy.context.scene.cursor.location[1] = cursorY - 0.5
    bpy.ops.view3d.snap_selected_to_cursor()
    bpy.ops.armature.select_all(action='DESELECT')
    bpy.context.active_object.data.edit_bones["IK-eye.R"].select_tail = True
    cursorY = bpy.context.scene.cursor.location[1]
    bpy.context.scene.cursor.location[1] = cursorY - 0.05
    bpy.ops.view3d.snap_selected_to_cursor()

    # Eyes Look-at Bone
    bpy.ops.armature.select_all(action='DESELECT')
    bpy.context.active_object.data.edit_bones["DEF-eye.L"].select_tail = True
    bpy.context.active_object.data.edit_bones["DEF-eye.R"].select_tail = True
    bpy.ops.view3d.snap_cursor_to_selected()

    bpy.ops.armature.select_all(action='DESELECT')
    bpy.context.active_object.data.edit_bones["IK-eyes_lookat"].select_head = True
    cursorY = bpy.context.scene.cursor.location[1]
    bpy.context.scene.cursor.location[1] = cursorY - 0.5
    bpy.ops.view3d.snap_selected_to_cursor()

    bpy.ops.armature.select_all(action='DESELECT')
    bpy.context.active_object.data.edit_bones["IK-eyes_lookat"].select_tail = True
    cursorY = bpy.context.scene.cursor.location[1]
    bpy.context.scene.cursor.location[1] = cursorY - 0.1
    bpy.ops.view3d.snap_selected_to_cursor()

    bpy.ops.armature.select_all(action='SELECT')
    bpy.ops.armature.calculate_roll(type='POS_X')


def setGenesisRolls(metarig):
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.context.view_layer.objects.active = metarig
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.armature.select_all(action='DESELECT')
    bpy.context.active_object.data.edit_bones[metarig_head].roll = 3.141593
    bpy.context.active_object.data.edit_bones[metarig_neck_upper].roll = 3.141593
    bpy.context.active_object.data.edit_bones[metarig_chest_upper].roll = 3.141593
    bpy.context.active_object.data.edit_bones[metarig_neck_base].roll = 3.141593
    bpy.context.active_object.data.edit_bones[metarig_chest].roll = 3.141593
    bpy.context.active_object.data.edit_bones[metarig_spine].roll = 3.141593
    bpy.context.active_object.data.edit_bones[metarig_hips].roll = 3.141593
    bpy.context.active_object.data.edit_bones["thigh.L"].roll = -0.050401
    bpy.context.active_object.data.edit_bones["shin.L"].roll = -0.015987
    bpy.context.active_object.data.edit_bones["foot.L"].roll = 1.658602
    # 2.005925
    bpy.context.active_object.data.edit_bones["toe.L"].roll = 1.658602
    bpy.context.active_object.data.edit_bones["heel.02.L"].roll = 0.0000
    bpy.context.active_object.data.edit_bones["shoulder.L"].roll = -0.006807
    bpy.context.active_object.data.edit_bones["upper_arm.L"].roll = 0.037177
    bpy.context.active_object.data.edit_bones["forearm.L"].roll = 1.635199
    bpy.context.active_object.data.edit_bones["hand.L"].roll = -0.394818
    bpy.context.active_object.data.edit_bones["thumb.01.L"].roll = 3.296754
    bpy.context.active_object.data.edit_bones["thumb.02.L"].roll = 1.938887
    bpy.context.active_object.data.edit_bones["thumb.03.L"].roll = 2.002941
    bpy.context.active_object.data.edit_bones["palm.01.L"].roll = -0.152829
    bpy.context.active_object.data.edit_bones["f_index.01.L"].roll = -0.089205
    bpy.context.active_object.data.edit_bones["f_index.02.L"].roll = 0.446884
    bpy.context.active_object.data.edit_bones["f_index.03.L"].roll = 0.419287
    bpy.context.active_object.data.edit_bones["palm.02.L"].roll = -0.230173
    bpy.context.active_object.data.edit_bones["f_middle.01.L"].roll = -0.134652
    bpy.context.active_object.data.edit_bones["f_middle.02.L"].roll = 0.169072
    bpy.context.active_object.data.edit_bones["f_middle.03.L"].roll = 0.037349
    bpy.context.active_object.data.edit_bones["palm.03.L"].roll = -0.232888
    bpy.context.active_object.data.edit_bones["f_ring.01.L"].roll = -0.05762
    bpy.context.active_object.data.edit_bones["f_ring.02.L"].roll = 0.096312
    bpy.context.active_object.data.edit_bones["f_ring.03.L"].roll = 0.185756
    bpy.context.active_object.data.edit_bones["palm.04.L"].roll = -0.082646
    bpy.context.active_object.data.edit_bones["f_pinky.01.L"].roll = -0.235231
    bpy.context.active_object.data.edit_bones["f_pinky.02.L"].roll = 0.008269
    bpy.context.active_object.data.edit_bones["f_pinky.03.L"].roll = 0.182735
    bpy.ops.armature.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')


def joinFaceRig(faceRig, rigifyRig, g3):
    # we need to know if it's g3
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')
    faceRig.select_set(True)
    rigifyRig.select_set(True)
    bpy.context.view_layer.objects.active = rigifyRig
    bpy.ops.object.join()

    bpy.ops.object.mode_set(mode='EDIT')
    rigifyRig.data.edit_bones["DEF-eye.L"].parent = rigifyRig.data.edit_bones["DEF-head"]
    rigifyRig.data.edit_bones["DEF-eye.R"].parent = rigifyRig.data.edit_bones["DEF-head"]
    rigifyRig.data.edit_bones["IK-eyes_lookat"].parent = rigifyRig.data.edit_bones["DEF-head"]
    if(g3 == False):
        rigifyRig.data.edit_bones["DEF-tonguebase"].parent = rigifyRig.data.edit_bones["DEF-head"]
    else:
        rigifyRig.data.edit_bones["DEF-tongue.01"].parent = rigifyRig.data.edit_bones["DEF-head"]

    rigifyRig.data.edit_bones["DEF-eye.L"].layers = [False] * \
        23 + [True] + [False] * 8
    rigifyRig.data.edit_bones["DEF-eye.R"].layers = [False] * \
        23 + [True] + [False] * 8
    rigifyRig.data.edit_bones["IK-eye.L"].layers = [False] * \
        23 + [True] + [False] * 8
    rigifyRig.data.edit_bones["IK-eye.R"].layers = [False] * \
        23 + [True] + [False] * 8

    rigifyRig.data.edit_bones["IK-eyes_lookat"].layers = [False] * \
        22 + [True] + [False] * 9
    rigifyRig.data.edit_bones["DEF-tongue.01"].layers = [False] * \
        23 + [True] + [False] * 8
    rigifyRig.data.edit_bones["DEF-tongue.02"].layers = [False] * \
        23 + [True] + [False] * 8
    rigifyRig.data.edit_bones["DEF-tongue.03"].layers = [False] * \
        23 + [True] + [False] * 8
    rigifyRig.data.edit_bones["DEF-tongue.04"].layers = [False] * \
        23 + [True] + [False] * 8
    if(g3 == False):
        rigifyRig.data.edit_bones["DEF-tonguebase"].layers = [False] * \
            23 + [True] + [False] * 8
        rigifyRig.data.edit_bones["DEF-tongue.05"].layers = [False] * \
            23 + [True] + [False] * 8
        rigifyRig.data.edit_bones["DEF-tonguetip"].layers = [False] * \
            23 + [True] + [False] * 8

    bpy.context.object.data.layers[22] = True
    bpy.context.object.show_in_front = True
    bpy.ops.object.mode_set(mode='OBJECT')


def parentWGTs():
    bpy.ops.object.mode_set(mode='OBJECT')
    objNameList = bpy.data.objects.keys()
    wgtList = []
    for name in objNameList:
        obj = bpy.data.objects[name]
        if (name.startswith("WGT-")):
            wgtList.append(obj)

    if (len(wgtList) > 0):
        bpy.ops.view3d.snap_cursor_to_center()
        bpy.ops.object.empty_add(type='PLAIN_AXES')
        wgtParent = bpy.context.active_object
        wgtParent.name = "WGTs"
        for obj in wgtList:
            obj.parent = wgtParent

def unhideObject(obj):
    obj.hide_viewport = False
    if obj.name in bpy.context.scene.collection.objects.keys():
        # obj is in master collection, therefore not hidden
        return
    colls = findCollections(obj)
    if len([c for c in colls if not c.exclude]) > 0:
        # obj is in at least one visible collection
        return
    # unhide first collection obj is present in
    colls[0].exclude = False

def rigifySetup(operator, genesisGeneration, vgroupData, setMetarigRolls):
    """
    genesisGeneration: '1' | '2' | '3' | '8'
    """
    global genesisRig
    global rigifyRig
    genesis = bpy.context.active_object
    genesisRig = genesis.find_armature()
    genesisRig.hide_viewport = False
    findCollections(genesisRig)[0].exclude = False

    # VERTEX GROUPS
    bpy.ops.object.mode_set(mode='EDIT')
    bm = bmesh.from_edit_mesh(genesis.data)
    vgroups = [
        {"name": "metarig_head_tail", "vertList": vgroupData["head_tail"]},
        {"name": "metarig_neck_upper_head", "vertList": vgroupData["neck_upper_head"]},
        {"name": "metarig_chest_upper_head", "vertList": vgroupData["chest_upper_head"]},
        {"name": "metarig_breast_tail.L", "vertList": vgroupData["breast_tail_L"]},
        {"name": "metarig_spine_head", "vertList": vgroupData["spine_head"]},
        {"name": "metarig_hips_head", "vertList": vgroupData["hips_head"]},
        {"name": "metarig_pelvis_tail.L", "vertList": vgroupData["pelvis_tail_L"]},
        {"name": "metarig_toe_tail.L", "vertList": vgroupData["toe_tail_L"]},
        {"name": "metarig_heel_tail.L", "vertList": vgroupData["heel_tail_L"]},
        {"name": "metarig_heel02_head.L", "vertList": vgroupData["heel02_head_L"]},
        {"name": "metarig_heel02_tail.L", "vertList": vgroupData["heel02_tail_L"]},
        {"name": "metarig_hand_tail.L", "vertList": vgroupData["hand_tail_L"]},
        {"name": "metarig_thumbtip.L", "vertList": vgroupData["thumbtip_L"]},
        {"name": "metarig_indextip.L", "vertList": vgroupData["indextip_L"]},
        {"name": "metarig_midtip.L", "vertList": vgroupData["midtip_L"]},
        {"name": "metarig_ringtip.L", "vertList": vgroupData["ringtip_L"]},
        {"name": "metarig_pinkytip.L", "vertList": vgroupData["pinkytip_L"]},
        {"name": "metarig_indexcarp.L", "vertList": vgroupData["indexcarp_L"]},
        {"name": "metarig_midcarp.L", "vertList": vgroupData["midcarp_L"]},
        {"name": "metarig_ringcarp.L", "vertList": vgroupData["ringcarp_L"]},
        {"name": "metarig_pinkycarp.L", "vertList": vgroupData["pinkycarp_L"]},
        # face rig
        {"name": "metarig_eye_head.L", "vertList": vgroupData["eye_head_L"]},
        {"name": "metarig_eye_head.R", "vertList": vgroupData["eye_head_R"]},
        {"name": "metarig_eye_tail.L", "vertList": vgroupData["eye_tail_L"]},
        {"name": "metarig_eye_tail.R", "vertList": vgroupData["eye_tail_R"]},
        {"name": "metarig_jaw_head", "vertList": vgroupData["jaw_head"]},
        {"name": "metarig_chin_head", "vertList": vgroupData["chin_head"]},
        {"name": "metarig_chin_001_head", "vertList": vgroupData["chin_001_head"]},
        {"name": "metarig_chin_001_tail", "vertList": vgroupData["chin_001_tail"]},
        {"name": "metarig_lip_head.B.L", "vertList": vgroupData["lip_head_B_L"]},
        {"name": "metarig_lip_001_head.B.L", "vertList": vgroupData["lip_001_head_B_L"]},
        {"name": "metarig_lip_head.T.L", "vertList": vgroupData["lip_head_T_L"]},
        {"name": "metarig_lip_001_head.T.L", "vertList": vgroupData["lip_001_head_T_L"]},
        {"name": "metarig_lips_corner.L", "vertList": vgroupData["lips_corner_L"]},
        {"name": "metarig_nose_004_tail", "vertList": vgroupData["nose_004_tail"]},
        {"name": "metarig_nose_004_head", "vertList": vgroupData["nose_004_head"]},
        {"name": "metarig_nose_003_head", "vertList": vgroupData["nose_003_head"]},
        {"name": "metarig_nose_002_head", "vertList": vgroupData["nose_002_head"]},
        {"name": "metarig_nose_001_head", "vertList": vgroupData["nose_001_head"]},
        {"name": "metarig_nose_head", "vertList": vgroupData["nose_head"]},
        {"name": "metarig_nose_001_head.L", "vertList": vgroupData["nose_001_head_L"]},
        {"name": "metarig_nose_head.L", "vertList": vgroupData["nose_head_L"]},
        {"name": "metarig_brow_head.T.L", "vertList": vgroupData["brow_head_T_L"]},
        {"name": "metarig_brow_001_head.T.L", "vertList": vgroupData["brow_001_head_T_L"]},
        {"name": "metarig_brow_002_head.T.L", "vertList": vgroupData["brow_002_head_T_L"]},
        {"name": "metarig_brow_003_head.T.L", "vertList": vgroupData["brow_003_head_T_L"]},
        {"name": "metarig_cheek_001_head.T.L", "vertList": vgroupData["cheek_001_head_T_L"]},
        {"name": "metarig_cheek_001_head.B.L", "vertList": vgroupData["cheek_001_head_B_L"]},
        {"name": "metarig_chin_head.L", "vertList": vgroupData["chin_head_L"]},
        {"name": "metarig_jaw_001_head.L", "vertList": vgroupData["jaw_001_head_L"]},
        {"name": "metarig_jaw_head.L", "vertList": vgroupData["jaw_head_L"]},
        {"name": "metarig_temple_head.L", "vertList": vgroupData["temple_head_L"]},
        {"name": "metarig_forehead_head.L", "vertList": vgroupData["forehead_head_L"]},
        {"name": "metarig_forehead_001_head.L", "vertList": vgroupData["forehead_001_head_L"]},
        {"name": "metarig_forehead_002_head.L", "vertList": vgroupData["forehead_002_head_L"]},
        {"name": "metarig_ear_head.L", "vertList": vgroupData["ear_head_L"]},
        {"name": "metarig_ear_001_head.L", "vertList": vgroupData["ear_001_head_L"]},
        {"name": "metarig_ear_002_head.L", "vertList": vgroupData["ear_002_head_L"]},
        {"name": "metarig_ear_003_head.L", "vertList": vgroupData["ear_003_head_L"]},
        {"name": "metarig_ear_004_head.L", "vertList": vgroupData["ear_004_head_L"]},
        {"name": "metarig_eyelid_head.B.L", "vertList": vgroupData["eyelid_head_B_L"]},
        {"name": "metarig_eyelid_001_head.B.L", "vertList": vgroupData["eyelid_001_head_B_L"]},
        {"name": "metarig_eyelid_002_head.B.L", "vertList": vgroupData["eyelid_002_head_B_L"]},
        {"name": "metarig_eyelid_003_head.B.L", "vertList": vgroupData["eyelid_003_head_B_L"]},
        {"name": "metarig_eyelid_head.T.L", "vertList": vgroupData["eyelid_head_T_L"]},
        {"name": "metarig_eyelid_001_head.T.L", "vertList": vgroupData["eyelid_001_head_T_L"]},
        {"name": "metarig_eyelid_002_head.T.L", "vertList": vgroupData["eyelid_002_head_T_L"]},
        {"name": "metarig_eyelid_003_head.T.L", "vertList": vgroupData["eyelid_003_head_T_L"]},
        {"name": "metarig_teeth_head.B", "vertList": vgroupData["teeth_head_B"]},
        {"name": "metarig_teeth_tail.B", "vertList": vgroupData["teeth_tail_B"]},
        {"name": "metarig_teeth_head.T", "vertList": vgroupData["teeth_head_T"]},
        {"name": "metarig_teeth_tail.T", "vertList": vgroupData["teeth_tail_T"]},
        {"name": "metarig_tongue_head", "vertList": vgroupData["tongue_head"]},
        {"name": "metarig_tongue_001_head", "vertList": vgroupData["tongue_001_head"]},
        {"name": "metarig_tongue_002_head", "vertList": vgroupData["tongue_002_head"]},
        {"name": "metarig_tongue_002_tail", "vertList": vgroupData["tongue_002_tail"]},
        {"name": "metarig_brow_head.B.L", "vertList": vgroupData["brow_head_B_L"]},
        {"name": "metarig_brow_001_head.B.L", "vertList": vgroupData["brow_001_head_B_L"]},
        {"name": "metarig_brow_002_head.B.L", "vertList": vgroupData["brow_002_head_B_L"]},
        {"name": "metarig_brow_003_head.B.L", "vertList": vgroupData["brow_003_head_B_L"]},
        {"name": "metarig_brow_003_tail.B.L", "vertList": vgroupData["brow_003_tail_B_L"]},
    ]
    createVgroups(genesis, bm, vgroups)
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.view3d.snap_cursor_to_center()
    try:
        if is_below_2_79:
            bpy.ops.object.armature_pitchipoy_human_metarig_add()
        else:
            bpy.ops.object.armature_human_metarig_add()
    except AttributeError:
        operator.report({'ERROR'}, "Missing Addon: 'Rigify'")
        return {'CANCELLED'}
    except:
        operator.report(
            {'ERROR'}, "Rigify: Broken... Something's wrong with Rigify. Please report this")
        return {'CANCELLED'}
    
    metarig = bpy.context.active_object
    metarigPrep(metarig)

    copyMeshPos(metarig, genesis, metarig_head, "tail", "metarig_head_tail")
    copyMeshPos(metarig, genesis, metarig_neck_upper, "head", "metarig_neck_upper_head")
    copyMeshPos(metarig, genesis, metarig_chest_upper, "head", "metarig_chest_upper_head")
    copyMeshPos(metarig, genesis, "breast.L", "tail", "metarig_breast_tail.L", offset=(0, -0.01, 0))
    copyMeshPos(metarig, genesis, "breast.L", "head", "metarig_breast_tail.L", offset=(0, 0.07, 0))
    copyMeshPos(metarig, genesis, metarig_spine, "head", "metarig_spine_head")
    copyMeshPos(metarig, genesis, metarig_hips, "head", "metarig_hips_head")
    copyMeshPos(metarig, genesis, "pelvis.L", "head", "metarig_hips_head")
    copyMeshPos(metarig, genesis, "pelvis.L", "tail", "metarig_pelvis_tail.L")
    copyMeshPos(metarig, genesis, "toe.L", "tail", "metarig_toe_tail.L")
    # copyMeshPos(metarig, genesis, "heel.L", "tail", "metarig_heel_tail.L")
    copyMeshPos(metarig, genesis, "heel.02.L", "head", "metarig_heel_tail.L", offset=(-0.02, 0, 0))
    copyMeshPos(metarig, genesis, "heel.02.L", "tail", "metarig_heel_tail.L", offset=(0.02, 0, 0))
    copyMeshPos(metarig, genesis, "hand.L", "tail", "metarig_hand_tail.L")
    copyMeshPos(metarig, genesis, "thumb.03.L", "tail", "metarig_thumbtip.L")
    copyMeshPos(metarig, genesis, "f_index.03.L", "tail", "metarig_indextip.L")
    copyMeshPos(metarig, genesis, "f_middle.03.L", "tail", "metarig_midtip.L")
    copyMeshPos(metarig, genesis, "f_ring.03.L", "tail", "metarig_ringtip.L")
    copyMeshPos(metarig, genesis, "f_pinky.03.L", "tail", "metarig_pinkytip.L")
    copyMeshPos(metarig, genesis, "palm.01.L", "head", "metarig_indexcarp.L")
    copyMeshPos(metarig, genesis, "palm.02.L", "head", "metarig_midcarp.L")
    copyMeshPos(metarig, genesis, "palm.03.L", "head", "metarig_ringcarp.L")
    copyMeshPos(metarig, genesis, "palm.04.L", "head", "metarig_pinkycarp.L")
    # face rig
    copyMeshPos(metarig, genesis, "eye.L", "head", "metarig_eye_head.L")
    copyMeshPos(metarig, genesis, "eye.L", "tail", "metarig_eye_tail.L")
    copyMeshPos(metarig, genesis, "jaw", "head", "metarig_jaw_head")
    copyMeshPos(metarig, genesis, "chin", "head", "metarig_chin_head")
    copyMeshPos(metarig, genesis, "chin.001", "head", "metarig_chin_001_head")
    copyMeshPos(metarig, genesis, "chin.001", "tail", "metarig_chin_001_tail")
    copyMeshPos(metarig, genesis, "lip.B.L", "head", "metarig_lip_head.B.L")
    copyMeshPos(metarig, genesis, "lip.B.L.001", "head", "metarig_lip_001_head.B.L")
    copyMeshPos(metarig, genesis, "lip.B.L.001", "tail", "metarig_lips_corner.L")
    copyMeshPos(metarig, genesis, "lip.T.L", "head", "metarig_lip_head.T.L")
    copyMeshPos(metarig, genesis, "lip.T.L.001", "head", "metarig_lip_001_head.T.L")
    copyMeshPos(metarig, genesis, "lip.T.L.001", "tail", "metarig_lips_corner.L")
    copyMeshPos(metarig, genesis, "cheek.B.L", "head", "metarig_lips_corner.L")
    copyMeshPos(metarig, genesis, "nose.004", "tail", "metarig_nose_004_tail")
    copyMeshPos(metarig, genesis, "nose.004", "head", "metarig_nose_004_head")
    copyMeshPos(metarig, genesis, "nose.003", "head", "metarig_nose_003_head")
    copyMeshPos(metarig, genesis, "nose.002", "head", "metarig_nose_002_head")
    copyMeshPos(metarig, genesis, "nose.001", "head", "metarig_nose_001_head")
    copyMeshPos(metarig, genesis, "nose", "head", "metarig_nose_head")
    copyMeshPos(metarig, genesis, "nose.L", "head", "metarig_nose_head.L")
    copyMeshPos(metarig, genesis, "nose.L.001", "head", "metarig_nose_001_head.L")
    copyMeshPos(metarig, genesis, "nose.L.001", "tail", "metarig_nose_002_head")
    copyMeshPos(metarig, genesis, "brow.T.L.003", "tail", "metarig_nose_head")
    copyMeshPos(metarig, genesis, "brow.T.L.003", "head", "metarig_brow_003_head.T.L")
    copyMeshPos(metarig, genesis, "brow.T.L.002", "head", "metarig_brow_002_head.T.L")
    copyMeshPos(metarig, genesis, "brow.T.L.001", "head", "metarig_brow_001_head.T.L")
    copyMeshPos(metarig, genesis, "brow.T.L", "head", "metarig_brow_head.T.L")
    copyMeshPos(metarig, genesis, "cheek.T.L", "head", "metarig_brow_head.T.L")
    copyMeshPos(metarig, genesis, "cheek.T.L.001", "head", "metarig_cheek_001_head.T.L")
    copyMeshPos(metarig, genesis, "cheek.B.L.001", "head", "metarig_cheek_001_head.B.L")
    copyMeshPos(metarig, genesis, "chin.L", "head", "metarig_chin_head.L")
    copyMeshPos(metarig, genesis, "jaw.L.001", "head", "metarig_jaw_001_head.L")
    copyMeshPos(metarig, genesis, "jaw.L", "head", "metarig_jaw_head.L")
    copyMeshPos(metarig, genesis, "temple.L", "head", "metarig_temple_head.L")
    copyMeshPos(metarig, genesis, "forehead.L", "head", "metarig_forehead_head.L")
    copyMeshPos(metarig, genesis, "forehead.L", "tail", "metarig_brow_003_head.T.L")
    copyMeshPos(metarig, genesis, "forehead.L.001", "head", "metarig_forehead_001_head.L")
    copyMeshPos(metarig, genesis, "forehead.L.001", "tail", "metarig_brow_002_head.T.L")
    copyMeshPos(metarig, genesis, "forehead.L.002", "head", "metarig_forehead_002_head.L")
    copyMeshPos(metarig, genesis, "forehead.L.002", "tail", "metarig_brow_001_head.T.L")
    copyMeshPos(metarig, genesis, "ear.L", "head", "metarig_ear_head.L")
    copyMeshPos(metarig, genesis, "ear.L.001", "head", "metarig_ear_001_head.L")
    copyMeshPos(metarig, genesis, "ear.L.002", "head", "metarig_ear_002_head.L")
    copyMeshPos(metarig, genesis, "ear.L.003", "head", "metarig_ear_003_head.L")
    copyMeshPos(metarig, genesis, "ear.L.004", "head", "metarig_ear_004_head.L")
    copyMeshPos(metarig, genesis, "ear.L.004", "tail", "metarig_ear_head.L")
    copyMeshPos(metarig, genesis, "lid.B.L", "head", "metarig_eyelid_head.B.L")
    copyMeshPos(metarig, genesis, "lid.B.L.001", "head", "metarig_eyelid_001_head.B.L")
    copyMeshPos(metarig, genesis, "lid.B.L.002", "head", "metarig_eyelid_002_head.B.L")
    copyMeshPos(metarig, genesis, "lid.B.L.003", "head", "metarig_eyelid_003_head.B.L")
    copyMeshPos(metarig, genesis, "lid.B.L.003", "tail", "metarig_eyelid_head.T.L")
    copyMeshPos(metarig, genesis, "lid.T.L", "head", "metarig_eyelid_head.T.L")
    copyMeshPos(metarig, genesis, "lid.T.L.001", "head", "metarig_eyelid_001_head.T.L")
    copyMeshPos(metarig, genesis, "lid.T.L.002", "head", "metarig_eyelid_002_head.T.L")
    copyMeshPos(metarig, genesis, "lid.T.L.003", "head", "metarig_eyelid_003_head.T.L")
    copyMeshPos(metarig, genesis, "teeth.B", "head", "metarig_teeth_head.B")
    copyMeshPos(metarig, genesis, "teeth.B", "tail", "metarig_teeth_tail.B")
    copyMeshPos(metarig, genesis, "teeth.T", "head", "metarig_teeth_head.T")
    copyMeshPos(metarig, genesis, "teeth.T", "tail", "metarig_teeth_tail.T")
    copyMeshPos(metarig, genesis, "tongue", "head", "metarig_tongue_head")
    copyMeshPos(metarig, genesis, "tongue.001", "head", "metarig_tongue_001_head")
    copyMeshPos(metarig, genesis, "tongue.002", "head", "metarig_tongue_002_head")
    copyMeshPos(metarig, genesis, "tongue.002", "tail", "metarig_tongue_002_tail")
    copyMeshPos(metarig, genesis, "brow.B.L", "head", "metarig_brow_head.B.L")
    copyMeshPos(metarig, genesis, "brow.B.L.001", "head", "metarig_brow_001_head.B.L")
    copyMeshPos(metarig, genesis, "brow.B.L.002", "head", "metarig_brow_002_head.B.L")
    copyMeshPos(metarig, genesis, "brow.B.L.003", "head", "metarig_brow_003_head.B.L")
    copyMeshPos(metarig, genesis, "brow.B.L.003", "tail", "metarig_brow_003_tail.B.L")

    if genesisGeneration == '1' or genesisGeneration == '2':
        copyBonePos(metarig, genesisRig, metarig_neck_base, "head", "neck", "head")
        copyBonePos(metarig, genesisRig, metarig_chest, "head", "chest", "head")
        copyBonePos(metarig, genesisRig, "thigh.L", "head", "lThigh", "head")
        copyBonePos(metarig, genesisRig, "shoulder.L", "tail", "lShldr", "head", offset=(0, 0, 0.03))
        copyBonePos(metarig, genesisRig, "upper_arm.L", "head", "lShldr", "head")
        copyBonePos(metarig, genesisRig, "forearm.L", "head", "lForeArm", "head")
    elif genesisGeneration == '3' or genesisGeneration == '8':
        copyBonePos(metarig, genesisRig, metarig_neck_upper, "head", "neckUpper", "head")
        copyBonePos(metarig, genesisRig, metarig_neck_base, "head", "neckLower", "head")
        copyBonePos(metarig, genesisRig, metarig_chest_upper, "head", "chestUpper", "head")
        copyBonePos(metarig, genesisRig, metarig_chest, "head", "chestLower", "head")
        copyBonePos(metarig, genesisRig, metarig_spine, "head", "abdomenLower", "head")
        copyBonePos(metarig, genesisRig, "thigh.L", "head", "lThighBend", "head")
        copyBonePos(metarig, genesisRig, "shoulder.L", "tail", "lShldrBend", "head", offset=(0, 0, 0.03))
        copyBonePos(metarig, genesisRig, "upper_arm.L", "head", "lShldrBend", "head")
        copyBonePos(metarig, genesisRig, "forearm.L", "head", "lForearmBend", "head")
        copyBonePos(metarig, genesisRig, "palm.01.L", "head", "lCarpal1", "head")
        copyBonePos(metarig, genesisRig, "palm.02.L", "head", "lCarpal2", "head")
        copyBonePos(metarig, genesisRig, "palm.03.L", "head", "lCarpal3", "head")
        copyBonePos(metarig, genesisRig, "palm.04.L", "head", "lCarpal4", "head")
    copyBonePos(metarig, genesisRig, metarig_head, "head", "head", "head")
    copyBonePos(metarig, genesisRig, "face", "head", "head", "head")
    copyBonePos(metarig, genesisRig, "face", "tail", "head", "head", offset=(0, 0, 0.092))
    copyBonePos(metarig, genesisRig, "shin.L", "head", "lShin", "head")
    copyBonePos(metarig, genesisRig, "foot.L", "head", "lFoot", "head")
    copyBonePos(metarig, genesisRig, "toe.L", "head", "lToe", "head")
    copyBonePos(metarig, genesisRig, "shoulder.L", "head", "lCollar", "head")
    copyBonePos(metarig, genesisRig, "hand.L", "head", "lHand", "head")
    copyBonePos(metarig, genesisRig, "palm.01.L", "tail", "lIndex1", "head")
    copyBonePos(metarig, genesisRig, "palm.02.L", "tail", "lMid1", "head")
    copyBonePos(metarig, genesisRig, "palm.03.L", "tail", "lRing1", "head")
    copyBonePos(metarig, genesisRig, "palm.04.L", "tail", "lPinky1", "head")
    copyBonePos(metarig, genesisRig, "thumb.01.L", "head", "lThumb1", "head")
    copyBonePos(metarig, genesisRig, "thumb.02.L", "head", "lThumb2", "head")
    copyBonePos(metarig, genesisRig, "thumb.03.L", "head", "lThumb3", "head")
    copyBonePos(metarig, genesisRig, "f_index.01.L", "head", "lIndex1", "head")
    copyBonePos(metarig, genesisRig, "f_index.02.L", "head", "lIndex2", "head")
    copyBonePos(metarig, genesisRig, "f_index.03.L", "head", "lIndex3", "head")
    copyBonePos(metarig, genesisRig, "f_middle.01.L", "head", "lMid1", "head")
    copyBonePos(metarig, genesisRig, "f_middle.02.L", "head", "lMid2", "head")
    copyBonePos(metarig, genesisRig, "f_middle.03.L", "head", "lMid3", "head")
    copyBonePos(metarig, genesisRig, "f_ring.01.L", "head", "lRing1", "head")
    copyBonePos(metarig, genesisRig, "f_ring.02.L", "head", "lRing2", "head")
    copyBonePos(metarig, genesisRig, "f_ring.03.L", "head", "lRing3", "head")
    copyBonePos(metarig, genesisRig, "f_pinky.01.L", "head", "lPinky1", "head")
    copyBonePos(metarig, genesisRig, "f_pinky.02.L", "head", "lPinky2", "head")
    copyBonePos(metarig, genesisRig, "f_pinky.03.L", "head", "lPinky3", "head")

    metarigFinishingTouches(metarig)
    setMetarigRolls(metarig)
    deleteVgroups(genesis, vgroups)

    bpy.ops.view3d.snap_cursor_to_center()
    bpy.context.view_layer.objects.active = metarig
    bpy.ops.pose.rigify_generate()
    rigifyRig = bpy.context.active_object
    rigifyRig.name = genesis.name + "-rig"
    rigifyRigFinishingTouches(rigifyRig)

    bpy.ops.object.select_all(action='DESELECT')
    bpy.context.view_layer.objects.active = genesis
    genesis.select_set(True)
    return {'FINISHED'}


def rigifyRigFinishingTouches(rigifyRig):
    rigifyRig.show_in_front = True
    # new rigify has missing DEF bones
    rigifyRig.data.bones["ORG-eye.L"].use_deform = True
    rigifyRig.data.bones["ORG-eye.R"].use_deform = True
    rigifyRig.data.bones["ORG-teeth.T"].use_deform = True
    rigifyRig.data.bones["ORG-teeth.B"].use_deform = True
    # disable ik stretch
    if is_below_2_79:
        rigifyRig.pose.bones["MCH-thigh_parent.L"]["IK_Stretch"] = 0
        rigifyRig.pose.bones["MCH-thigh_parent.R"]["IK_Stretch"] = 0
        rigifyRig.pose.bones["MCH-upper_arm_parent.L"]["IK_Stretch"] = 0
        rigifyRig.pose.bones["MCH-upper_arm_parent.R"]["IK_Stretch"] = 0
        # there's a typo in previous versions. stretch is spelt strertch
        rigifyRig.pose.bones["MCH-thigh_parent.L"]["IK_Strertch"] = 0
        rigifyRig.pose.bones["MCH-thigh_parent.R"]["IK_Strertch"] = 0
        rigifyRig.pose.bones["MCH-upper_arm_parent.L"]["IK_Strertch"] = 0
        rigifyRig.pose.bones["MCH-upper_arm_parent.R"]["IK_Strertch"] = 0
    else:
        rigifyRig.pose.bones["thigh_parent.L"]["IK_Stretch"] = 0
        rigifyRig.pose.bones["thigh_parent.R"]["IK_Stretch"] = 0
        rigifyRig.pose.bones["upper_arm_parent.L"]["IK_Stretch"] = 0
        rigifyRig.pose.bones["upper_arm_parent.R"]["IK_Stretch"] = 0

# -------------------------------------------------------------


def copyVgroup(obj, vgroup, copyName):
    if vgroup in obj.vertex_groups.keys():
        vgname = vgroup + "_copy"
        if copyName not in obj.vertex_groups.keys() and vgname not in obj.vertex_groups.keys():
            bpy.ops.object.vertex_group_set_active(group=vgroup)
            bpy.ops.object.vertex_group_copy()
            obj.vertex_groups[vgname].name = copyName


def mixVgroups(obj, vgroupA, vgroupB):
    if ((vgroupA in obj.vertex_groups.keys()) and (vgroupB in obj.vertex_groups.keys())):
        backupName = vgroupA + "_copy"
        if (backupName not in obj.vertex_groups.keys()):
            # Create backup
            bpy.ops.object.vertex_group_set_active(group=vgroupA)
            bpy.ops.object.vertex_group_copy()

        bpy.ops.object.modifier_add(type='VERTEX_WEIGHT_MIX')
        # to determine the name of the modifier just created
        mod_name = obj.modifiers[len(obj.modifiers.keys()) - 1].name

        obj.modifiers[mod_name].vertex_group_a = vgroupA
        obj.modifiers[mod_name].vertex_group_b = vgroupB
        obj.modifiers[mod_name].mix_mode = 'ADD'
        obj.modifiers[mod_name].mix_set = 'OR'
        bpy.ops.object.modifier_apply(modifier=mod_name)


def renameVgroups(obj, oldname, newname):
    if (oldname in obj.vertex_groups.keys()):
        obj.vertex_groups[oldname].name = newname
        return True
    return False


def setupArmatureModifier(obj, rigifyRig):
    if (len(obj.modifiers.keys()) > 0):
        for modifierName in obj.modifiers.keys():
            if (obj.modifiers[modifierName].type == 'ARMATURE'):
                obj.modifiers["Armature"].use_deform_preserve_volume = True
                if (rigifyRig.name in bpy.context.scene.objects.keys()):
                    obj.modifiers["Armature"].object = rigifyRig


# ------------------------------------------------------------------

def checkForMatName(obj, checkName):
    objMatList = obj.material_slots.keys()
    for key in objMatList:
        if (key == checkName):
            # normally, this checkpoint should be useless.
            # the next checkpoint should handle this possibility by itself.
            # I think it's a bug or sth...
            return key
        if (key.startswith(checkName)):
            if (isNameExtension(key, checkName)):
                return key
    return None


def setupSpecTex(mat, name):
    mat.texture_slots[0].texture.name = name+"-COL"
    texName = name+"-SPEC"
    matList = bpy.context.active_object.material_slots.keys()
    index = matList.index(name)
    bpy.context.object.active_material_index = index
    texCount = len(mat.texture_slots.keys())
    #bpy.context.object.active_material.active_texture_index = texCount
    tex = bpy.data.textures.new(texName, 'IMAGE')
    mat.texture_slots.add()
    mat.texture_slots[texCount].texture = tex
    image = mat.texture_slots[texCount-1].texture.image
    mat.texture_slots[texCount].texture.type = 'IMAGE'
    mat.texture_slots[texCount].texture.image = image
    mat.texture_slots[texCount].texture_coords = 'UV'
    mat.texture_slots[texCount].use_map_color_diffuse = False
    mat.texture_slots[texCount].use_map_specular = True
    mat.texture_slots[texCount].use_rgb_to_intensity = True


def setupBumpTex(mat, name):
    mat.texture_slots[0].texture.name = name+"-COL"
    texName = name+"-BUMP"
    matList = bpy.context.active_object.material_slots.keys()
    index = matList.index(name)
    bpy.context.object.active_material_index = index
    texCount = len(mat.texture_slots.keys())
    #bpy.context.object.active_material.active_texture_index = texCount
    tex = bpy.data.textures.new(texName, 'IMAGE')
    mat.texture_slots.add()
    mat.texture_slots[texCount].texture = tex
    image = mat.texture_slots[texCount-1].texture.image
    mat.texture_slots[texCount].texture.type = 'IMAGE'
    mat.texture_slots[texCount].texture.image = image
    mat.texture_slots[texCount].texture_coords = 'UV'
    mat.texture_slots[texCount].use_map_color_diffuse = False
    mat.texture_slots[texCount].use_map_normal = True
    mat.texture_slots[texCount].normal_factor = 0.05
    mat.texture_slots[texCount].use_rgb_to_intensity = True


def genMatMergeList(obj, originalMatList):
    matList = obj.material_slots.keys()
    originalMatListLength = len(originalMatList)
    for m in range(0, originalMatListLength-1):
        n = len(obj.material_slots[m].material.texture_slots.keys())
        if (n == 0):
            index = matList.index(originalMatList[m])
            del matList[index]
        elif (obj.material_slots[m].material.texture_slots[0].texture.type != 'IMAGE'):
            index = matList.index(originalMatList[m])
            del matList[index]
    return matList


def mergeMats(obj, originalMatList):
    matList = genMatMergeList(obj, originalMatList)
    terminationList = []
    checkList = []
    for mainMat in matList:
        if (mainMat not in checkList):
            originalIndex = originalMatList.index(mainMat)
            image = obj.material_slots[mainMat].material.texture_slots[0].texture.image
            for childMat in matList:
                if (childMat not in checkList):
                    if (childMat != mainMat):
                        childIndex = originalMatList.index(childMat)
                        if (obj.material_slots[childMat].material.texture_slots[0].texture.image == image):
                            extractMat(obj, originalIndex, childIndex)
                            checkList.append(childMat)
                            terminationList.append(childMat)
        checkList.append(mainMat)

    delMaterial(obj, terminationList, originalMatList)


def processMat(obj, defaultName, newName, mergeName):
    merge_mats = bpy.context.scene.khalibloo_merge_mats
    affect_textures = bpy.context.scene.khalibloo_affect_textures
    guessName = defaultName
    check = checkForMatName(obj, guessName)
    if (check is None):
        guessName = newName
        check = checkForMatName(obj, guessName)
        if (check is None):
            guessName = mergeName
            check = checkForMatName(obj, guessName)

    if (check is not None):
        mat = obj.material_slots[check].material
        name = newName
        if (merge_mats):
            name = mergeName
        mat.name = name
        mat.diffuse_intensity = 1
        mat.use_transparent_shadows = True
        mat.specular_intensity = 0
        if affect_textures:
            setupSpecTex(mat, name)
            setupBumpTex(mat, name)
        check = None


def toCyclesMat(mat, type="principled", roughness=0.5):
    """
        type: "principled" | "glass" | "transparent"
    """
    # get the material
    mat.use_nodes = True
    print(mat.name)
    # get the nodes
    nodes = mat.node_tree.nodes
    links = mat.node_tree.links

    # get some specific node:
    # returns None if the node does not exist
    diffuse = nodes.get("Diffuse BSDF")
    output = nodes.get("Material Output")
    nodes.remove(diffuse)
    if(type == "glass"):
        glass = nodes.new(type='ShaderNodeBsdfGlass')
        glass.location = output.location.x - 255, output.location.y
        # link nodes
        link = links.new(glass.outputs[0], output.inputs[0])
    elif(type == "principled"):
        principled = nodes.new(type='ShaderNodeBsdfPrincipled')
        principled.location = output.location.x - 255, output.location.y
        if(len(mat.texture_slots.keys()) > 0 and mat.texture_slots[0].texture and mat.texture_slots[0].texture.image):
            tex_diff = nodes.new(type='ShaderNodeTexImage')
            tex_diff.image = mat.texture_slots[0].texture.image
            tex_diff.location = principled.location.x - 255, principled.location.y
            link = links.new(
                tex_diff.outputs[0], principled.inputs[0])  # base color
        if(len(mat.texture_slots.keys()) > 1 and mat.texture_slots[1].texture and mat.texture_slots[1].texture.image):
            tex_gloss = nodes.new(type='ShaderNodeTexImage')
            tex_gloss.image = mat.texture_slots[1].texture.image
            tex_gloss.location = principled.location.x - 500, principled.location.y + 200
            invert = nodes.new(type='ShaderNodeInvert')
            invert.location = principled.location.x - 255, principled.location.y + 200
            link = links.new(tex_gloss.outputs[0], invert.inputs[1])
            link = links.new(invert.outputs[0],
                             principled.inputs[7])  # roughness
        principled.inputs[4].default_value = 0  # set metallic power to 0
        principled.inputs[5].default_value = 1  # set specular power to max
        # set specular power to max
        principled.inputs[7].default_value = roughness
        # link nodes
        link = links.new(principled.outputs[0], output.inputs[0])


def extractMat(obj, mainMatIndex, childMatIndex):
    # bpy.ops.object.mode_set(mode='OBJECT')
    #bpy.context.view_layer.objects.active = obj
    obj.active_material_index = childMatIndex
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_all(action='DESELECT')
    bpy.ops.object.material_slot_select()
    obj.active_material_index = mainMatIndex
    bpy.ops.object.material_slot_assign()
    bpy.ops.mesh.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')


def delMaterial(obj, terminationList, originalMatList):
    bpy.ops.object.mode_set(mode='OBJECT')
    #bpy.context.view_layer.objects.active = obj
    for m in terminationList:
        index = originalMatList.index(m)
        obj.active_material_index = index
        del originalMatList[index]
        bpy.ops.object.material_slot_remove()


# ============================================================================
# DEFINE OPERATORS
# ============================================================================


class KBP_OT_GenesisRigifySetup(bpy.types.Operator):
    """Generate and setup a rigify rig for the active Genesis figure"""
    bl_idname = "object.khalibloo_genesis_rigify_setup"
    bl_label = "Rigify"

    @classmethod
    def poll(cls, context):
        return ((context.active_object is not None) and (context.active_object.type == 'MESH'))

    def execute(self, context):
        genesis = bpy.context.active_object
        global rigifyRig
        global genesisRig

        if (genesis.find_armature() is not None):
            if (len(genesis.data.vertices.items()) == 19296):
                # VERTEX GROUPS RAW DATA
                head_tail = [3189, 3230]
                neck_upper_head = [167, 2049, 2050, 2797, 3186, 4099, 4422, 4447, 4451, 4452, 4453, 4454,
                                   4456, 4457, 4458, 4459, 4460, 4461, 4463, 4464, 4465, 4607, 4613, 9603,
                                   11485, 11486, 12233, 12622, 13409, 13710, 13733, 13737, 13738, 13739, 13741,
                                   13742, 13743, 13744, 13745, 13746, 13747, 13748, 13888, 13893]
                chest_upper_head = [6, 243, 244, 370, 374, 4141, 4142, 4519, 4520, 6423, 6424,
                                    6431, 6432, 6433, 6434, 6496, 6497, 9442, 9679, 9680, 9806,
                                    9810, 13448, 13449, 13801, 13802, 15646, 15647, 15654,
                                    15655, 15656, 15657, 15719, 15720]
                chest_head = [181, 182, 183, 184, 189, 190, 192, 3956, 3957, 4111, 4170, 4501,
                              4503, 4514, 4675, 4738, 4744, 4770, 4773, 4782, 4783, 4784,
                              4785, 4794, 4801, 4830, 4989, 4997, 4998, 4999, 5001, 5003,
                              5005, 5006, 5008, 5009, 5010, 5011, 5013, 5014, 5015, 5028,
                              6425, 6426, 6427, 6428, 6482, 6485, 6494, 6495, 6506, 6516,
                              6517, 6518, 6519, 9617, 9618, 9619, 9620, 9625, 9626, 9628,
                              13267, 13268, 13421, 13474, 13783, 13785, 13796, 13950, 14011,
                              14017, 14043, 14046, 14055, 14056, 14057, 14058, 14067, 14074,
                              14103, 14249, 14257, 14258, 14259, 14261, 14263, 14265, 14266,
                              14268, 14269, 14270, 14271, 14273, 14274, 14285, 15648, 15649,
                              15650, 15651, 15705, 15708, 15717, 15718, 15729, 15739, 15740,
                              15741, 15742]
                breast_tail_L = [0]
                spine_head = [3943, 4834, 4870, 4973, 4986, 5063, 5068, 5074, 5075, 5076, 5077,
                              5078, 5079, 5080, 5081, 5082, 5083, 5084, 5085, 5086, 5087, 5088,
                              5089, 5090, 5100, 5108, 5109, 5110, 5111, 5112, 5113, 5114, 5115,
                              5116, 5117, 5118, 5119, 5120, 5121, 5122, 5123, 5124, 6522, 6523,
                              6524, 6525, 13254, 14107, 14140, 14233, 14246, 14318, 14323, 14329,
                              14330, 14331, 14332, 14333, 14334, 14335, 14336, 14337, 14338, 14339,
                              14340, 14341, 14342, 14343, 14344, 14354, 14359, 14360, 14361, 14362,
                              14363, 14364, 14365, 14366, 14367, 14368, 14369, 14370, 14371, 14372,
                              14373, 14374, 15745, 15746, 15747, 15748]
                hips_head = [166, 454, 483, 484, 485, 486, 487, 488, 489, 491, 492, 502, 507, 510,
                             511, 539, 542, 608, 609, 610, 611, 612, 613, 3780, 3781, 3837, 3920,
                             3923, 3932, 3933, 4365, 6466, 6467, 6472, 6473, 9272, 9277, 9602, 9890,
                             9919, 9920, 9921, 9922, 9923, 9924, 9925, 9927, 9928, 9938, 9943, 9946,
                             9947, 9975, 9978, 10044, 10045, 10046, 10047, 10048, 10049, 13091, 13092,
                             13148, 13231, 13234, 13243, 13244, 13659, 15689, 15690, 15695, 15696,
                             18426, 18431]
                pelvis_tail_L = [179, 525, 526, 561,
                                 563, 3961, 4941, 4942, 4956]
                toe_tail_L = [1288]
                heel_tail_L = [6575, 6586]
                heel02_head_L = [1431]
                heel02_tail_L = [1410, 1451]
                hand_tail_L = [1561, 1742, 1779, 1781,
                               1783, 5140, 6400, 6643, 6644]
                thumbtip_L = [1602, 1603]
                indextip_L = [5778, 5850]
                midtip_L = [5926, 5998]
                ringtip_L = [6074, 6146]
                pinkytip_L = [6222, 6294]
                indexcarp_L = [1646, 1686, 1689, 1690,
                               1830, 1841, 1852, 1874, 6642]
                midcarp_L = [1584, 1670, 1751, 1796,
                             1798, 1801, 1809, 1873, 1874, 1875]
                ringcarp_L = [1648, 1666, 1693, 1695, 1696,
                              1697, 1767, 1768, 1788, 1791, 1801, 6404]
                pinkycarp_L = [1668, 1692, 1693, 1713, 1791, 1794]
                # face rig
                eye_head_L = [3385, 3386, 3389, 3391, 3393, 3395, 3397, 3399, 3401, 3403, 3405, 3407,
                              3409, 3411, 3413, 3415]
                eye_head_R = [12700, 12701, 12704, 12706, 12708, 12710, 12712, 12714, 12716, 12718,
                              12720, 12722, 12724, 12726, 12728, 12730]
                eye_tail_L = [3516]
                eye_tail_R = [12831]
                jaw_head = [2113, 2803, 2804, 2883, 2984, 3188, 3221, 3281, 4114, 11549, 12239, 12240,
                            12319, 12420, 13424]
                chin_head = [2190, 2470, 3004, 3007, 3012, 3013, 3029, 3030, 3042, 3043, 3046, 3277,
                             3278, 3284, 11626, 11906, 12440, 12443, 12448, 12449, 12465, 12466, 12478,
                             12479, 12482]
                chin_001_head = [3026, 3027, 3028, 3031, 3033, 3034, 3282, 3283, 12462, 12463, 12464,
                                 12467, 12469, 12470]
                chin_001_tail = [3048, 4727, 5541, 5542, 5739, 5740, 5748, 5749, 12484, 14775, 14964,
                                 14972, 14973]
                lip_head_B_L = [3293, 3294]
                lip_001_head_B_L = [2217, 2218, 2219, 2529]
                lip_head_T_L = [3302, 3303, 3304]
                lip_001_head_T_L = [2511, 2515, 2523]
                lips_corner_L = [2200, 2531, 2532, 2533]
                nose_004_tail = [3305, 4291, 4293, 13588]
                nose_004_head = [2393, 3153, 4286, 11829, 12589, 13584]
                nose_003_head = [2188, 3145, 11624, 12581]
                nose_002_head = [2392, 3262, 11828]
                nose_001_head = [3000, 3001, 3285, 5419,
                                 5421, 5422, 12436, 12437, 14662, 14664]
                nose_head = [2930, 2944, 3209, 5527, 5533, 12366, 12380, 14762]
                brow_003_head_T_L = [4472, 4473, 5583, 5584]
                brow_002_head_T_L = [2826, 2914, 5582, 5587]
                brow_001_head_T_L = [2476, 2750, 2829, 5579]
                brow_head_T_L = [2740, 2786, 2902, 4199, 5678, 5680]
                nose_001_head_L = [2194, 5361, 5362]
                nose_head_L = [3083, 3112, 5450, 5497]
                cheek_001_head_T_L = [2951, 2959, 2960, 4190]
                cheek_001_head_B_L = [2890, 2979, 2992, 2993]
                chin_head_L = [2471, 3014, 3040]
                jaw_001_head_L = [2869, 2870, 2871, 2872, 2875, 2876]
                jaw_head_L = [4205, 4206, 5616, 5617]
                temple_head_L = [5635, 5637]
                forehead_head_L = [2837, 4296, 5433, 5434]
                forehead_001_head_L = [2184, 5438, 5439]
                forehead_002_head_L = [2137, 2703, 5630]
                ear_head_L = [2266, 2310, 2685, 5613]
                ear_001_head_L = [2271, 2272, 2276]
                ear_002_head_L = [2329, 2330, 2331, 2336, 2337, 2338]
                ear_003_head_L = [2282, 2283, 2284, 2603, 2604, 2679]
                ear_004_head_L = [2267, 2306]
                eyelid_head_B_L = [2376, 2744, 3134, 5524]
                eyelid_001_head_B_L = [2367, 2368, 8781]
                eyelid_002_head_B_L = [5520, 5521, 8776]
                eyelid_003_head_B_L = [2363, 2364, 2377, 2378, 8777, 8783]
                eyelid_head_T_L = [2162, 2484, 2746, 3119]
                eyelid_001_head_T_L = [2169, 8787]
                eyelid_002_head_T_L = [2175, 8785]
                eyelid_003_head_T_L = [3106, 3107, 8775]
                teeth_head_B = [8222, 17396]
                teeth_tail_B = [8077, 17259]
                teeth_head_T = [7811, 16998]
                teeth_tail_T = [7667, 16862]
                tongue_head = [7341, 7342]
                tongue_001_head = [7285, 7289, 16508, 16512]
                tongue_002_head = [7297, 7300, 16520, 16523]
                tongue_002_tail = [7362, 16559]
                brow_head_B_L = [2907, 5555, 5566]
                brow_001_head_B_L = [2906]
                brow_002_head_B_L = [2909, 2915]
                brow_003_head_B_L = [2918, 2919]
                brow_003_tail_B_L = [2935, 5529]
                vgroupData = {
                    "head_tail": head_tail,
                    "neck_upper_head": neck_upper_head,
                    "chest_upper_head": chest_upper_head,
                    "chest_head": chest_head,
                    "breast_tail_L": breast_tail_L,
                    "spine_head": spine_head,
                    "hips_head": hips_head,
                    "pelvis_tail_L": pelvis_tail_L,
                    "toe_tail_L": toe_tail_L,
                    "heel_tail_L": heel_tail_L,
                    "heel02_head_L": heel02_head_L,
                    "heel02_tail_L": heel02_tail_L,
                    "hand_tail_L": hand_tail_L,
                    "thumbtip_L": thumbtip_L,
                    "indextip_L": indextip_L,
                    "midtip_L": midtip_L,
                    "ringtip_L": ringtip_L,
                    "pinkytip_L": pinkytip_L,
                    "indexcarp_L": indexcarp_L,
                    "midcarp_L": midcarp_L,
                    "ringcarp_L": ringcarp_L,
                    "pinkycarp_L": pinkycarp_L,
                    "eye_head_L": eye_head_L,
                    "eye_head_R": eye_head_R,
                    "eye_tail_L": eye_tail_L,
                    "eye_tail_R": eye_tail_R,
                    "jaw_head": jaw_head,
                    "chin_head": chin_head,
                    "chin_001_head": chin_001_head,
                    "chin_001_tail": chin_001_tail,
                    "lip_head_B_L": lip_head_B_L,
                    "lip_001_head_B_L": lip_001_head_B_L,
                    "lip_head_T_L": lip_head_T_L,
                    "lip_001_head_T_L": lip_001_head_T_L,
                    "lips_corner_L": lips_corner_L,
                    "nose_004_tail": nose_004_tail,
                    "nose_004_head": nose_004_head,
                    "nose_003_head": nose_003_head,
                    "nose_002_head": nose_002_head,
                    "nose_001_head": nose_001_head,
                    "nose_head": nose_head,
                    "brow_003_head_T_L": brow_003_head_T_L,
                    "brow_002_head_T_L": brow_002_head_T_L,
                    "brow_001_head_T_L": brow_001_head_T_L,
                    "brow_head_T_L": brow_head_T_L,
                    "nose_001_head_L": nose_001_head_L,
                    "nose_head_L": nose_head_L,
                    "cheek_001_head_T_L": cheek_001_head_T_L,
                    "cheek_001_head_B_L": cheek_001_head_B_L,
                    "chin_head_L": chin_head_L,
                    "jaw_001_head_L": jaw_001_head_L,
                    "jaw_head_L": jaw_head_L,
                    "temple_head_L": temple_head_L,
                    "forehead_head_L": forehead_head_L,
                    "forehead_001_head_L": forehead_001_head_L,
                    "forehead_002_head_L": forehead_002_head_L,
                    "ear_head_L": ear_head_L,
                    "ear_001_head_L": ear_001_head_L,
                    "ear_002_head_L": ear_002_head_L,
                    "ear_003_head_L": ear_003_head_L,
                    "ear_004_head_L": ear_004_head_L,
                    "eyelid_head_B_L": eyelid_head_B_L,
                    "eyelid_001_head_B_L": eyelid_001_head_B_L,
                    "eyelid_002_head_B_L": eyelid_002_head_B_L,
                    "eyelid_003_head_B_L": eyelid_003_head_B_L,
                    "eyelid_head_T_L": eyelid_head_T_L,
                    "eyelid_001_head_T_L": eyelid_001_head_T_L,
                    "eyelid_002_head_T_L": eyelid_002_head_T_L,
                    "eyelid_003_head_T_L": eyelid_003_head_T_L,
                    "teeth_head_B": teeth_head_B,
                    "teeth_tail_B": teeth_tail_B,
                    "teeth_head_T": teeth_head_T,
                    "teeth_tail_T": teeth_tail_T,
                    "tongue_head": tongue_head,
                    "tongue_001_head": tongue_001_head,
                    "tongue_002_head": tongue_002_head,
                    "tongue_002_tail": tongue_002_tail,
                    "brow_head_B_L": brow_head_B_L,
                    "brow_001_head_B_L": brow_001_head_B_L,
                    "brow_002_head_B_L": brow_002_head_B_L,
                    "brow_003_head_B_L": brow_003_head_B_L,
                    "brow_003_tail_B_L": brow_003_tail_B_L,
                }
                return rigifySetup(self, '1', vgroupData, setGenesisRolls)
        return {'CANCELLED'}


class KBP_OT_GenesisRigifyVgroups(bpy.types.Operator):
    """Mixes and renames the deformation vertex groups of a Genesis figure and/or selected Genesis item(s) to conform with Rigify. Backups are made before mixing, so no vertex groups are lost."""
    bl_idname = "object.khalibloo_genesis_rigify_vgroups"
    bl_label = "Rigify Vertex Groups"

    @classmethod
    def poll(cls, context):
        return ((context.active_object is not None) and (context.active_object.type == 'MESH'))

    def execute(self, context):
        selectionList = context.selected_objects
        objBackup = context.active_object
        global rigifyRig

        for obj in selectionList:
            context.view_layer.objects.active = bpy.data.objects[obj.name]
            if (len(obj.vertex_groups.keys()) > 0):
                # mixVgroups(obj, "head", "lowerJaw")
                # mixVgroups(obj, "head", "upperJaw")
                mixVgroups(obj, "head", "CenterBrow")
                mixVgroups(obj, "head", "lBrowOuter")
                mixVgroups(obj, "head", "rBrowOuter")
                mixVgroups(obj, "head", "lBrowMid")
                mixVgroups(obj, "head", "rBrowMid")
                mixVgroups(obj, "head", "lBrowInner")
                mixVgroups(obj, "head", "rBrowInner")
                mixVgroups(obj, "head", "MidNoseBridge")
                mixVgroups(obj, "head", "lEyelidInner")
                mixVgroups(obj, "head", "rEyelidInner")
                mixVgroups(obj, "head", "lEyelidUpperInner")
                mixVgroups(obj, "head", "rEyelidUpperInner")
                mixVgroups(obj, "head", "lEyelidUpper")
                mixVgroups(obj, "head", "rEyelidUpper")
                mixVgroups(obj, "head", "lEyelidUpperOuter")
                mixVgroups(obj, "head", "rEyelidUpperOuter")
                mixVgroups(obj, "head", "lEyelidOuter")
                mixVgroups(obj, "head", "rEyelidOuter")
                mixVgroups(obj, "head", "lEyelidLowerOuter")
                mixVgroups(obj, "head", "rEyelidLowerOuter")
                mixVgroups(obj, "head", "lEyelidLower")
                mixVgroups(obj, "head", "rEyelidLower")
                mixVgroups(obj, "head", "lEyelidLowerInner")
                mixVgroups(obj, "head", "rEyelidLowerInner")
                mixVgroups(obj, "head", "lSquintInner")
                mixVgroups(obj, "head", "rSquintInner")
                mixVgroups(obj, "head", "lSquintOuter")
                mixVgroups(obj, "head", "rSquintOuter")
                mixVgroups(obj, "head", "lCheekUpper")
                mixVgroups(obj, "head", "rCheekUpper")
                mixVgroups(obj, "head", "lCheekLower")
                mixVgroups(obj, "head", "rCheekLower")
                mixVgroups(obj, "head", "Nose")
                mixVgroups(obj, "head", "lNostril")
                mixVgroups(obj, "head", "rNostril")
                mixVgroups(obj, "head", "lLipBelowNose")
                mixVgroups(obj, "head", "rLipBelowNose")
                mixVgroups(obj, "head", "lLipUpperOuter")
                mixVgroups(obj, "head", "rLipUpperOuter")
                mixVgroups(obj, "head", "lLipUpperInner")
                mixVgroups(obj, "head", "rLipUpperInner")
                mixVgroups(obj, "head", "LipUpperMiddle")
                mixVgroups(obj, "head", "lLipNasolabialCrease")
                mixVgroups(obj, "head", "rLipNasolabialCrease")
                mixVgroups(obj, "head", "lNasolabialUpper")
                mixVgroups(obj, "head", "rNasolabialUpper")
                mixVgroups(obj, "head", "lNasolabialMiddle")
                mixVgroups(obj, "head", "rNasolabialMiddle")
                mixVgroups(obj, "head", "lNasolabialLower")
                mixVgroups(obj, "head", "rNasolabialLower")
                mixVgroups(obj, "head", "lNasolabialMouthCorner")
                mixVgroups(obj, "head", "rNasolabialMouthCorner")
                mixVgroups(obj, "head", "lLipCorner")
                mixVgroups(obj, "head", "rLipCorner")
                mixVgroups(obj, "lowerJaw", "lLipLowerOuter")
                mixVgroups(obj, "lowerJaw", "rLipLowerOuter")
                mixVgroups(obj, "lowerJaw", "LipLowerMiddle")
                mixVgroups(obj, "lowerJaw", "lLipLowerInner")
                mixVgroups(obj, "lowerJaw", "rLipLowerInner")
                mixVgroups(obj, "lowerJaw", "LipBelow")
                mixVgroups(obj, "lowerJaw", "Chin")
                mixVgroups(obj, "lowerJaw", "BelowJaw")
                mixVgroups(obj, "head", "lJawClench")
                mixVgroups(obj, "head", "rJawClench")
                mixVgroups(obj, "lowerJaw", "lowerTeeth")
                mixVgroups(obj, "upperJaw", "upperTeeth")
                mixVgroups(obj, "head", "lowerFaceRig")
                mixVgroups(obj, "head", "upperFaceRig")
                mixVgroups(obj, "head", "lEar")
                mixVgroups(obj, "head", "rEar")

                # mixVgroups(obj, "lThighBend", "lThighTwist")
                mixVgroups(obj, "lToe", "lBigToe")
                mixVgroups(obj, "lToe", "lSmallToe1")
                mixVgroups(obj, "lToe", "lSmallToe2")
                mixVgroups(obj, "lToe", "lSmallToe3")
                mixVgroups(obj, "lToe", "lSmallToe4")
                mixVgroups(obj, "lToe", "lBigToe_2")
                mixVgroups(obj, "lToe", "lSmallToe1_2")
                mixVgroups(obj, "lToe", "lSmallToe2_2")
                mixVgroups(obj, "lToe", "lSmallToe3_2")
                mixVgroups(obj, "lToe", "lSmallToe4_2")
                mixVgroups(obj, "lFoot", "lHeel")
                mixVgroups(obj, "lFoot", "lMetatarsals")

                # mixVgroups(obj, "rThighBend", "rThighTwist")
                mixVgroups(obj, "rToe", "rBigToe")
                mixVgroups(obj, "rToe", "rSmallToe1")
                mixVgroups(obj, "rToe", "rSmallToe2")
                mixVgroups(obj, "rToe", "rSmallToe3")
                mixVgroups(obj, "rToe", "rSmallToe4")
                mixVgroups(obj, "rToe", "rBigToe_2")
                mixVgroups(obj, "rToe", "rSmallToe1_2")
                mixVgroups(obj, "rToe", "rSmallToe2_2")
                mixVgroups(obj, "rToe", "rSmallToe3_2")
                mixVgroups(obj, "rToe", "rSmallToe4_2")
                mixVgroups(obj, "rFoot", "rHeel")
                mixVgroups(obj, "rFoot", "rMetatarsals")

                # mixVgroups(obj, "lShldrBend", "lShldrTwist")
                # mixVgroups(obj, "lForearmBend", "lForearmTwist")
                # mixVgroups(obj, "rShldrBend", "rShldrTwist")
                # mixVgroups(obj, "rForearmBend", "rForearmTwist")

                mixVgroups(obj, "head", "tongueBase")
                #mixVgroups(obj, "head", "tongue01")
                #mixVgroups(obj, "head", "tongue02")
                mixVgroups(obj, "tongue02", "tongue03")
                mixVgroups(obj, "tongue04", "tongue05")
                mixVgroups(obj, "tongue04", "tongueTip")

                # rename vgroups
                renameVgroups(obj, "head", "DEF-"+metarig_head)
                renameVgroups(obj, "neck", "DEF-"+metarig_neck_base)
                renameVgroups(obj, "neckLower", "DEF-"+metarig_neck_base)  # g3
                renameVgroups(obj, "neckUpper", "DEF-" +
                              metarig_neck_upper)  # g3
                renameVgroups(obj, "chest", "DEF-"+metarig_chest)
                renameVgroups(obj, "chestLower", "DEF-"+metarig_chest)  # g3
                renameVgroups(obj, "chestUpper", "DEF-" +
                              metarig_chest_upper)  # g3

                renameVgroups(obj, "abdomen", "DEF-"+metarig_hips)
                renameVgroups(obj, "abdomenLower", "DEF-"+metarig_hips)  # g3
                renameVgroups(obj, "abdomen2", "DEF-"+metarig_spine)
                renameVgroups(obj, "abdomenUpper", "DEF-"+metarig_spine)  # g3
                copyVgroup(obj, "pelvis", "DEF-pelvis.R")
                renameVgroups(obj, "pelvis", "DEF-pelvis.L")
                renameVgroups(obj, "lowerJaw", "ORG-teeth.B")
                if not renameVgroups(obj, "upperJaw", "ORG-teeth.T"):
                    # G3 has no upperJaw, use upperTeeth instead
                    renameVgroups(obj, "upperTeeth", "ORG-teeth.T")
                # renameVgroups(obj, "tongueBase", "DEF-tonguebase")
                renameVgroups(obj, "tongue01", "DEF-tongue.002")
                renameVgroups(obj, "tongue02", "DEF-tongue.001")
                # renameVgroups(obj, "tongue03", "DEF-tongue.03")
                renameVgroups(obj, "tongue04", "DEF-tongue")
                # renameVgroups(obj, "tongue05", "DEF-tongue.05")
                # renameVgroups(obj, "tongueTip", "DEF-tonguetip")

                # LEFT
                renameVgroups(obj, "lFoot", "DEF-foot.L")
                renameVgroups(obj, "lToe", "DEF-toe.L")
                renameVgroups(obj, "lCollar", "DEF-shoulder.L")
                renameVgroups(obj, "lPectoral", "DEF-breast.L")

                renameVgroups(obj, "lEye", "ORG-eye.L")
                renameVgroups(obj, "lThigh", "DEF-thigh.L")
                renameVgroups(obj, "lThighBend", "DEF-thigh.L")  # g3
                renameVgroups(obj, "lThighTwist", "DEF-thigh.L.001")  # g3
                renameVgroups(obj, "lShin", "DEF-shin.L")
                renameVgroups(obj, "lShinTwist", "DEF-shin.L.001")  # g3
                renameVgroups(obj, "lShldr", "DEF-upper_arm.L")
                renameVgroups(obj, "lShldrBend", "DEF-upper_arm.L")  # g3
                renameVgroups(obj, "lShldrTwist", "DEF-upper_arm.L.001")  # g3
                renameVgroups(obj, "lForeArm", "DEF-forearm.L")
                renameVgroups(obj, "lForearmBend", "DEF-forearm.L")  # g3
                renameVgroups(obj, "lForearmTwist", "DEF-forearm.L.001")  # g3
                renameVgroups(obj, "lHand", "DEF-hand.L")
                if("lCarpal3" in obj.vertex_groups.keys() or "lCarpal4" in obj.vertex_groups.keys()):
                    # it's a g3
                    renameVgroups(obj, "lCarpal1", "DEF-palm.01.L")
                    renameVgroups(obj, "lCarpal2", "DEF-palm.02.L")
                    renameVgroups(obj, "lCarpal3", "DEF-palm.03.L")
                    renameVgroups(obj, "lCarpal4", "DEF-palm.04.L")
                else:
                    renameVgroups(obj, "lCarpal1", "DEF-palm.01.L")
                    renameVgroups(obj, "lCarpal2", "DEF-palm.04.L")
                renameVgroups(obj, "lThumb2", "DEF-thumb.02.L")
                renameVgroups(obj, "lThumb3", "DEF-thumb.03.L")
                renameVgroups(obj, "lIndex2", "DEF-f_index.02.L")
                renameVgroups(obj, "lIndex3", "DEF-f_index.03.L")
                renameVgroups(obj, "lMid2", "DEF-f_middle.02.L")
                renameVgroups(obj, "lMid3", "DEF-f_middle.03.L")
                renameVgroups(obj, "lRing2", "DEF-f_ring.02.L")
                renameVgroups(obj, "lRing3", "DEF-f_ring.03.L")
                renameVgroups(obj, "lPinky2", "DEF-f_pinky.02.L")
                renameVgroups(obj, "lPinky3", "DEF-f_pinky.03.L")
                renameVgroups(obj, "lThumb1", "DEF-thumb.01.L")
                renameVgroups(obj, "lIndex1", "DEF-f_index.01.L")
                renameVgroups(obj, "lMid1", "DEF-f_middle.01.L")
                renameVgroups(obj, "lRing1", "DEF-f_ring.01.L")
                renameVgroups(obj, "lPinky1", "DEF-f_pinky.01.L")

                # RIGHT
                renameVgroups(obj, "rFoot", "DEF-foot.R")
                renameVgroups(obj, "rToe", "DEF-toe.R")
                renameVgroups(obj, "rCollar", "DEF-shoulder.R")
                renameVgroups(obj, "rPectoral", "DEF-breast.R")

                renameVgroups(obj, "rEye", "ORG-eye.R")
                renameVgroups(obj, "rThigh", "DEF-thigh.R")
                renameVgroups(obj, "rThighBend", "DEF-thigh.R")  # g3
                renameVgroups(obj, "rThighTwist", "DEF-thigh.R.001")  # g3
                renameVgroups(obj, "rShin", "DEF-shin.R")
                renameVgroups(obj, "rShinTwist", "DEF-shin.R.001")  # g3
                renameVgroups(obj, "rShldr", "DEF-upper_arm.R")
                renameVgroups(obj, "rShldrBend", "DEF-upper_arm.R")  # g3
                renameVgroups(obj, "rShldrTwist", "DEF-upper_arm.R.001")  # g3
                renameVgroups(obj, "rForeArm", "DEF-forearm.R")
                renameVgroups(obj, "rForearmBend", "DEF-forearm.R")  # g3
                renameVgroups(obj, "rForearmTwist", "DEF-forearm.R.001")  # g3
                renameVgroups(obj, "rHand", "DEF-hand.R")
                if("rCarpal3" in obj.vertex_groups.keys() or "rCarpal4" in obj.vertex_groups.keys()):
                    # it's a g3
                    renameVgroups(obj, "rCarpal1", "DEF-palm.01.R")
                    renameVgroups(obj, "rCarpal2", "DEF-palm.02.R")
                    renameVgroups(obj, "rCarpal3", "DEF-palm.03.R")
                    renameVgroups(obj, "rCarpal4", "DEF-palm.04.R")
                else:
                    renameVgroups(obj, "rCarpal1", "DEF-palm.01.R")
                    renameVgroups(obj, "rCarpal2", "DEF-palm.04.R")
                renameVgroups(obj, "rThumb2", "DEF-thumb.02.R")
                renameVgroups(obj, "rThumb3", "DEF-thumb.03.R")
                renameVgroups(obj, "rIndex2", "DEF-f_index.02.R")
                renameVgroups(obj, "rIndex3", "DEF-f_index.03.R")
                renameVgroups(obj, "rMid2", "DEF-f_middle.02.R")
                renameVgroups(obj, "rMid3", "DEF-f_middle.03.R")
                renameVgroups(obj, "rRing2", "DEF-f_ring.02.R")
                renameVgroups(obj, "rRing3", "DEF-f_ring.03.R")
                renameVgroups(obj, "rPinky2", "DEF-f_pinky.02.R")
                renameVgroups(obj, "rPinky3", "DEF-f_pinky.03.R")
                renameVgroups(obj, "rThumb1", "DEF-thumb.01.R")
                renameVgroups(obj, "rIndex1", "DEF-f_index.01.R")
                renameVgroups(obj, "rMid1", "DEF-f_middle.01.R")
                renameVgroups(obj, "rRing1", "DEF-f_ring.01.R")
                renameVgroups(obj, "rPinky1", "DEF-f_pinky.01.R")

                # apply parent's transforms
                if obj.parent:
                    obj.parent.hide_viewport = False
                    findCollections(obj.parent)[0].exclude = False
                    context.view_layer.objects.active = obj.parent
                    obj.parent.select_set(True)
                    bpy.ops.object.mode_set(mode='OBJECT')
                    bpy.ops.object.transform_apply(
                        location=True, rotation=True, scale=True)

                if rigifyRig is not None:
                    findCollections(rigifyRig)[0].exclude = False
                    obj.parent = rigifyRig
                    setupArmatureModifier(obj, rigifyRig)

        bpy.ops.object.select_all(action='DESELECT')
        objBackup.select_set(True)
        context.view_layer.objects.active = objBackup
        return {'FINISHED'}


class KBP_OT_GenesisUnrigifyVgroups(bpy.types.Operator):
    """Renames the vertex groups of a rigified Genesis figure and/or selected Genesis item(s) to their original names"""
    bl_idname = "object.khalibloo_genesis_unrigify_vgroups"
    bl_label = "Unrigify Vertex Groups"

    @classmethod
    def poll(cls, context):
        return ((context.active_object is not None) and (context.active_object.type == 'MESH'))

    def execute(self, context):
        selectionList = bpy.context.selected_objects
        objBackup = bpy.context.active_object
        global genesisRig

        for obj in selectionList:
            bpy.context.view_layer.objects.active = bpy.data.objects[obj.name]
            if (len(obj.vertex_groups.keys()) > 0):

                renameVgroups(obj, "DEF-head", "head")
                renameVgroups(obj, "DEF-neck", "neck")
                renameVgroups(obj, "DEF-chest", "chest")
                renameVgroups(obj, "DEF-spine", "abdomen2")
                renameVgroups(obj, "DEF-hips", "pelvis")
                renameVgroups(obj, "DEF-tonguebase", "tongueBase")
                renameVgroups(obj, "DEF-tongue.01", "tongue01")
                renameVgroups(obj, "DEF-tongue.02", "tongue02")
                renameVgroups(obj, "DEF-tongue.03", "tongue03")
                renameVgroups(obj, "DEF-tongue.04", "tongue04")
                renameVgroups(obj, "DEF-tongue.05", "tongue05")
                renameVgroups(obj, "DEF-tonguetip", "tongueTip")
                # LEFT
                renameVgroups(obj, "DEF-eye.L", "lEye")
                renameVgroups(obj, "DEF-thigh.01.L", "lThigh")
                renameVgroups(obj, "DEF-shin.01.L", "lShin")
                renameVgroups(obj, "DEF-foot.L", "lFoot")
                renameVgroups(obj, "DEF-toe.L", "lToe")
                renameVgroups(obj, "DEF-shoulder.L", "lCollar")
                renameVgroups(obj, "DEF-upper_arm.01.L", "lShldr")
                renameVgroups(obj, "DEF-forearm.01.L", "lForeArm")
                renameVgroups(obj, "DEF-forearm.02.L", "lForeArmTwist")
                renameVgroups(obj, "DEF-hand.L", "lHand")
                renameVgroups(obj, "DEF-palm.04.L", "lCarpal2")
                renameVgroups(obj, "DEF-palm.01.L", "lCarpal1")
                renameVgroups(obj, "DEF-thumb.01.L.02", "lThumb1")
                renameVgroups(obj, "DEF-thumb.02.L", "lThumb2")
                renameVgroups(obj, "DEF-thumb.03.L", "lThumb3")
                renameVgroups(obj, "DEF-f_index.01.L.01", "lIndex1")
                renameVgroups(obj, "DEF-f_index.02.L", "lIndex2")
                renameVgroups(obj, "DEF-f_index.03.L", "lIndex3")
                renameVgroups(obj, "DEF-f_middle.01.L.01", "lMid1")
                renameVgroups(obj, "DEF-f_middle.02.L", "lMid2")
                renameVgroups(obj, "DEF-f_middle.03.L", "lMid3")
                renameVgroups(obj, "DEF-f_ring.01.L.01", "lRing1")
                renameVgroups(obj, "DEF-f_ring.02.L", "lRing2")
                renameVgroups(obj, "DEF-f_ring.03.L", "lRing3")
                renameVgroups(obj, "DEF-f_pinky.01.L.01", "lPinky1")
                renameVgroups(obj, "DEF-f_pinky.02.L", "lPinky2")
                renameVgroups(obj, "DEF-f_pinky.03.L", "lPinky3")

                # RIGHT
                renameVgroups(obj, "DEF-eye.R", "rEye")
                renameVgroups(obj, "DEF-thigh.01.R", "rThigh")
                renameVgroups(obj, "DEF-shin.01.R", "rShin")
                renameVgroups(obj, "DEF-foot.R", "rFoot")
                renameVgroups(obj, "DEF-toe.R", "rToe")
                renameVgroups(obj, "DEF-shoulder.R", "rCollar")
                renameVgroups(obj, "DEF-upper_arm.01.R", "rShldr")
                renameVgroups(obj, "DEF-forearm.01.R", "rForeArm")
                renameVgroups(obj, "DEF-forearm.02.R", "rForeArmTwist")
                renameVgroups(obj, "DEF-hand.R", "rHand")
                renameVgroups(obj, "DEF-palm.04.R", "rCarpal2")
                renameVgroups(obj, "DEF-palm.01.R", "rCarpal1")
                renameVgroups(obj, "DEF-thumb.01.R.02", "rThumb1")
                renameVgroups(obj, "DEF-thumb.02.R", "rThumb2")
                renameVgroups(obj, "DEF-thumb.03.R", "rThumb3")
                renameVgroups(obj, "DEF-f_index.01.R.01", "rIndex1")
                renameVgroups(obj, "DEF-f_index.02.R", "rIndex2")
                renameVgroups(obj, "DEF-f_index.03.R", "rIndex3")
                renameVgroups(obj, "DEF-f_middle.01.R.01", "rMid1")
                renameVgroups(obj, "DEF-f_middle.02.R", "rMid2")
                renameVgroups(obj, "DEF-f_middle.03.R", "rMid3")
                renameVgroups(obj, "DEF-f_ring.01.R.01", "rRing1")
                renameVgroups(obj, "DEF-f_ring.02.R", "rRing2")
                renameVgroups(obj, "DEF-f_ring.03.R", "rRing3")
                renameVgroups(obj, "DEF-f_pinky.01.R.01", "rPinky1")
                renameVgroups(obj, "DEF-f_pinky.02.R", "rPinky2")
                renameVgroups(obj, "DEF-f_pinky.03.R", "rPinky3")

                # apply parent's transforms
                bpy.context.view_layer.objects.active = obj.parent
                obj.parent.select_set(True)
                bpy.ops.object.mode_set(mode='OBJECT')
                bpy.ops.object.transform_apply(
                    location=True, rotation=True, scale=True)

                if (genesisRig is not None):
                    findCollections(genesisRig)[0].exclude = False
                    obj.parent = genesisRig
                    setupArmatureModifier(obj, genesisRig)

        bpy.context.view_layer.objects.active = objBackup
        return {'FINISHED'}


class KBP_OT_GenesisMaterialSetup(bpy.types.Operator):
    """Fixes the necessary settings on each of the materials and textures of the active Genesis figure.
    Note:for this to work, the materials must be using their default names."""
    bl_idname = "object.khalibloo_genesis_material_setup"
    bl_label = "Setup Materials"

    @classmethod
    def poll(cls, context):
        return ((context.active_object is not None) and (len(context.active_object.material_slots.keys()) != 0))

    def execute(self, context):
        obj = bpy.context.active_object
        originalMatList = obj.material_slots.keys()
        affect_textures = bpy.context.scene.khalibloo_affect_textures
        merge_mats = bpy.context.scene.khalibloo_merge_mats

        if (merge_mats):
            mergeMats(obj, originalMatList)

        if(context.scene.render.engine == 'CYCLES'):
            for slot in obj.material_slots:
                if("EyeMoisture" in slot.material.name):
                    toCyclesMat(slot.material, type="glass", roughness=0)
                elif("Cornea" in slot.material.name):
                    toCyclesMat(slot.material, roughness=1)
                else:
                    toCyclesMat(slot.material)

        else:
            # daz_3_SkinFoot
            # processMat(obj, "Feet", "Limbs", "daz_3_SkinFoot", "Feet", "Limbs")
            guessName = "daz_3_SkinFoot"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Feet"
                check = checkForMatName(obj, guessName)
                if (check is None):
                    guessName = "Limbs"
                    check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Feet"
                if (merge_mats):
                    name = "Limbs"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 0
                if affect_textures:
                    setupSpecTex(mat, name)
                    setupBumpTex(mat, name)
                check = None

            # daz_6_Eyelash
            guessName = "daz_6_Eyelash"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Eyelashes"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Eyelashes"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                if (mat.texture_slots[0] is not None):
                    mat.texture_slots[0].use_rgb_to_intensity = True
                check = None

            # daz_5_Sclera
            guessName = "daz_5_Sclera"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Sclera"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Sclera"
                if (merge_mats):
                    name = "Irises"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 0
                check = None

            # daz_5_Pupil
            guessName = "daz_5_Pupil"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Pupils"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Pupils"
                if (merge_mats):
                    name = "Irises"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 0
                check = None

            # daz_5_Iris
            guessName = "daz_5_Iris"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Irises"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                # print(mat.name)
                name = "Irises"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 0
                if affect_textures:
                    setupBumpTex(mat, name)
                check = None

            # daz_5_Cornea if it's a Genesis 2 figure
            guessName = "Cornea"
            check = checkForMatName(obj, guessName)
            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Cornea"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_shader = 'WARDISO'
                mat.specular_intensity = 1
                mat.specular_slope = 0.05
                mat.use_transparency = True
                mat.alpha = 0
                mat.specular_alpha = 0
                check = None

            if (len(obj.data.vertices.items()) == 19296):
                # daz_5_Cornea ONLY if it's a Genesis figure
                guessName = "daz_5_Cornea"
                check = checkForMatName(obj, guessName)
                if (check is None):
                    guessName = "Cornea"
                    check = checkForMatName(obj, guessName)

                if (check is not None):
                    mat = obj.material_slots[check].material
                    name = "Cornea"
                    mat.name = name
                    mat.diffuse_intensity = 1
                    mat.use_transparent_shadows = True
                    mat.specular_shader = 'WARDISO'
                    mat.specular_intensity = 1
                    mat.specular_slope = 0.05
                    mat.use_transparency = True
                    mat.alpha = 0
                    check = None

            # EyeReflection
            guessName = "EyeReflection"
            check = checkForMatName(obj, guessName)
            if (check is not None):
                mat = obj.material_slots[check].material
                name = "EyeReflection"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_shader = 'WARDISO'
                mat.specular_intensity = 1
                mat.specular_slope = 0.05
                mat.use_transparency = True
                mat.alpha = 0
                check = None

            # daz_4_Tongue
            guessName = "daz_4_Tongue"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Tongue"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Tongue"
                if (merge_mats):
                    name = "InnerMouth"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 1
                mat.specular_hardness = 500
                if affect_textures:
                    setupBumpTex(mat, name)
                check = None

            # daz_4_Teeth
            guessName = "daz_4_Teeth"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Teeth"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Teeth"
                if (merge_mats):
                    name = "InnerMouth"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 1
                mat.specular_hardness = 500
                if affect_textures:
                    setupBumpTex(mat, name)
                check = None

            # daz_4_InnerMouth
            guessName = "daz_4_InnerMouth"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "InnerMouth"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "InnerMouth"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 1
                mat.specular_hardness = 500
                if affect_textures:
                    setupBumpTex(mat, name)
                check = None

            # daz_4_Gums
            guessName = "daz_4_Gums"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Gums"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Gums"
                if (merge_mats):
                    name = "InnerMouth"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 1
                mat.specular_hardness = 500
                if affect_textures:
                    setupBumpTex(mat, name)
                check = None

            # daz_3_SkinArm
            guessName = "daz_3_SkinArm"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Shoulders"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Shoulders"
                if (merge_mats):
                    name = "Limbs"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 0
                if affect_textures:
                    setupSpecTex(mat, name)
                    setupBumpTex(mat, name)
                check = None

            # daz_2_SkinTorso
            guessName = "daz_2_SkinTorso"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Torso"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Torso"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 0
                if affect_textures:
                    setupSpecTex(mat, name)
                    setupBumpTex(mat, name)
                check = None

            # daz_2_Nipple
            guessName = "daz_2_Nipple"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Nipples"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Nipples"
                if (merge_mats):
                    name = "Torso"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 0
                if affect_textures:
                    setupSpecTex(mat, name)
                    setupBumpTex(mat, name)
                check = None

            # daz_2_SkinNeck
            guessName = "daz_2_SkinNeck"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Neck"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Neck"
                if (merge_mats):
                    name = "Torso"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 0
                if affect_textures:
                    setupSpecTex(mat, name)
                    setupBumpTex(mat, name)
                check = None

            # daz_3_SkinForearm
            guessName = "daz_3_SkinForearm"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Forearms"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Forearms"
                if (merge_mats):
                    name = "Limbs"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 0
                if affect_textures:
                    setupSpecTex(mat, name)
                    setupBumpTex(mat, name)
                check = None

            # daz_3_SkinLeg
            guessName = "daz_3_SkinLeg"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Legs"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Legs"
                if (merge_mats):
                    name = "Limbs"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 0
                if affect_textures:
                    setupSpecTex(mat, name)
                    setupBumpTex(mat, name)
                check = None

            # daz_2_SkinHip
            guessName = "daz_2_SkinHip"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Hips"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Hips"
                if (merge_mats):
                    name = "Torso"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 0
                if affect_textures:
                    setupSpecTex(mat, name)
                    setupBumpTex(mat, name)
                check = None

            # daz_2_SkinHead
            guessName = "daz_2_SkinHead"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Head"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Head"
                if (merge_mats):
                    name = "Torso"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 0
                if affect_textures:
                    setupSpecTex(mat, name)
                    setupBumpTex(mat, name)
                check = None

            # daz_3_SkinHand
            guessName = "daz_3_SkinHand"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Hands"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Hands"
                if (merge_mats):
                    name = "Limbs"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 0
                if affect_textures:
                    setupSpecTex(mat, name)
                    setupBumpTex(mat, name)
                check = None

            # daz_7_Tear
            guessName = "daz_7_Tear"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Tears"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Tears"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_shader = 'WARDISO'
                mat.specular_intensity = 1
                mat.specular_slope = 0.05
                mat.use_transparency = True
                mat.alpha = 0
                check = None

            # daz_1_Nostril
            guessName = "daz_1_Nostril"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Nostrils"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Nostrils"
                if (merge_mats):
                    name = "Face"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 0
                if affect_textures:
                    setupSpecTex(mat, name)
                    setupBumpTex(mat, name)
                check = None

            # daz_1_Lip
            guessName = "daz_1_Lip"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Lips"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Lips"
                if (merge_mats):
                    name = "Face"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 0.6
                mat.specular_hardness = 400
                if affect_textures:
                    setupSpecTex(mat, name)
                    setupBumpTex(mat, name)
                check = None

            # daz_5_Lacrimal
            guessName = "daz_5_Lacrimal"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Lacrimals"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Lacrimals"
                if (merge_mats):
                    name = "Irises"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 0
                check = None

            # daz_1_SkinFace
            guessName = "daz_1_SkinFace"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Face"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Face"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 0
                if affect_textures:
                    setupSpecTex(mat, name)
                    setupBumpTex(mat, name)
                check = None

            # Ears
            guessName = "Ears"
            check = checkForMatName(obj, guessName)
            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Ears"
                if (merge_mats):
                    name = "Torso"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 0
                if affect_textures:
                    setupSpecTex(mat, name)
                    setupBumpTex(mat, name)
                check = None

            # daz_3_Fingernail
            guessName = "daz_3_Fingernail"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Fingernails"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Fingernails"
                if (merge_mats):
                    name = "Limbs"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 0
                if affect_textures:
                    setupSpecTex(mat, name)
                    setupBumpTex(mat, name)
                check = None

            # daz_3_Toenail
            guessName = "daz_3_Toenail"
            check = checkForMatName(obj, guessName)
            if (check is None):
                guessName = "Toenails"
                check = checkForMatName(obj, guessName)

            if (check is not None):
                mat = obj.material_slots[check].material
                name = "Toenails"
                if (merge_mats):
                    name = "Limbs"
                mat.name = name
                mat.diffuse_intensity = 1
                mat.use_transparent_shadows = True
                mat.specular_intensity = 0
                if affect_textures:
                    setupSpecTex(mat, name)
                    setupBumpTex(mat, name)
                check = None

        return {'FINISHED'}


class KBP_OT_RigifyNeckFix(bpy.types.Operator):
    """Fixes a rare condition where the rigify rig's neck bone is a lot larger than it should be"""
    bl_idname = "object.khalibloo_rigify_neck_fix"
    bl_label = "Rigify Neck Fix"

    @classmethod
    def poll(cls, context):
        return ((context.active_object is not None) and (context.active_object.type == 'ARMATURE'))

    def execute(self, context):
        rig = context.active_object
        neck = rig.pose.bones["neck"].custom_shape
        findCollections(neck)[0].exclude = False
        neck.hide_viewport = False
        bpy.ops.object.select_all(action='DESELECT')
        context.view_layer.objects.active = neck
        neck.select_set(True)
        context.space_data.pivot_point = 'MEDIAN_POINT'
        context.object.scale[0] = 0.390
        context.object.scale[1] = 0.390
        context.object.scale[2] = 0.390
        bpy.ops.view3d.snap_cursor_to_selected()
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.mesh.select_all(action='SELECT')
        pivot = context.space_data.pivot_point
        context.space_data.pivot_point = 'CURSOR'
        bpy.ops.transform.resize(value=(0.2442, 0.2442, 0.2442), constraint_axis=(False, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH',
                                 proportional_size=1, snap=False, snap_target='CLOSEST', snap_point=(0, 0, 0), snap_align=False, snap_normal=(0, 0, 0), texture_space=False, release_confirm=False)
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.select_all(action='DESELECT')
        context.view_layer.objects.active = rig
        context.space_data.pivot_point = pivot

        return{'FINISHED'}


class KBP_OT_GenesisImportMorphs(bpy.types.Operator):
    """Imports all Genesis morphs(.obj) in the path specified as shape keys of the active Genesis figure"""
    bl_idname = "object.khalibloo_import_genesis_morphs"
    bl_label = "Import Morphs"

    @classmethod
    def poll(cls, context):
        return ((len(context.selected_objects) == 1) and (context.active_object is not None) and (context.active_object.type == 'MESH'))

    def execute(self, context):
        import os
        obj = context.active_object
        morph_dir = bpy.context.scene.khalibloo_genesis_morph_dir
        if(not morph_dir.endswith("/")):
            morph_dir = morph_dir + "/"

        imported_objs = []
        for filename in os.listdir(morph_dir):
            filepath = morph_dir + filename
            name, extension = os.path.splitext(filepath)
            if (extension == ".obj"):
                try:
                    bpy.ops.import_scene.obj(filepath=filepath, filter_glob="*.obj;*.mtl",
                                             use_edges=True, use_smooth_groups=True,
                                             use_split_objects=False, use_split_groups=False,
                                             use_groups_as_vgroups=False, use_image_search=False,
                                             split_mode='OFF', global_clamp_size=0,
                                             axis_forward='-Y', axis_up='Z')
                except AttributeError:
                    self.report(
                        {'ERROR'}, "Missing Addon: 'Import-Export: Wavefront OBJ format'")
                    return {'CANCELLED'}
                # except RuntimeError:
                    # self.report({'WARNING'}, "Ensure that only the relevant OBJ files are in the folder!")
                    # continue
                bpy.ops.object.transform_apply(
                    location=False, rotation=True, scale=True)
                bpy.ops.object.join_shapes()
                obj.select_set(False)
                bpy.ops.object.delete()
                obj.select_set(True)
        return {'FINISHED'}
