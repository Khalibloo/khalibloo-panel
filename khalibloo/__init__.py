# <pep8 compliant>
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

import bpy
from bpy.props import StringProperty, BoolProperty, EnumProperty, IntProperty
from . import auto_load
# from . import utils
from . import custom_ops
# from . import genesis_tools
# from . import genesis2_tools
# from . import genesis3_tools
# from . import genesis8_tools

bl_info = {
    "name": "Khalibloo Panel",
    "version": (1, 2, 0),
    "author": "Khalifa Lame",
    "blender": (2, 80, 0),
    "description": (
        "A huge collection of tools for batch operations and "
        "handling of repetitive tasks. Also includes tools for setting "
        "up DAZ Genesis and Genesis 2 characters and clothes."
    ),
    "location": "3D Viewport > Properties(N) Panel > Tools > Khalibloo",
    "category": "Khalibloo",
}

is_280 = float(bpy.app.version_string.split()[0]) >= 2.80

# ============================================================================
# DRAW PANEL
# ============================================================================


class KBP_PT_KhaliblooPanel(bpy.types.Panel):
    """Creates a Panel in the properties context of the 3D viewport"""

    bl_label = "Khalibloo"
    bl_idname = "KBP_PT_khalibloo_panel_3d"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Tool"

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        platform = bpy.context.scene.khalibloo.platform
        genesis_subclass = bpy.context.scene.khalibloo.genesis_platform_subclass
        genesis2_subclass = bpy.context.scene.khalibloo.genesis2_platform_subclass
        genesis3_subclass = bpy.context.scene.khalibloo.genesis3_platform_subclass
        genesis8_subclass = bpy.context.scene.khalibloo.genesis8_platform_subclass
        general_subclass = bpy.context.scene.khalibloo.general_platform_subclass
        # mod_filter_mode = bpy.context.scene.khalibloo.modifier_filter_mode

        # Platforn type
        layout.prop(scene.khalibloo, "platform", text="", expand=False)

        # ---------------------------------------------------------------------------------
        # GENERAL TOOLS
        if platform == "GENERAL":
            # Category type
            layout.prop(scene.khalibloo, "general_platform_subclass", expand=True)
            layout.separator()

            # OBJECT DATA TAB
            if general_subclass == "OBJECT DATA":
                layout.separator()
                layout.operator(
                    "object.khalibloo_assign_obj_indices", text="Assign Pass Indices"
                )
                layout.separator()
                row = layout.row(align=True)
                row.operator(
                    "object.khalibloo_unhide_select",
                    text="",
                    icon="RESTRICT_SELECT_OFF",
                )
                row.operator(
                    "object.khalibloo_hide_select", text="", icon="RESTRICT_SELECT_ON"
                )
                row.operator(
                    "object.khalibloo_unhide_render",
                    text="",
                    icon="RESTRICT_RENDER_OFF",
                )
                row.operator(
                    "object.khalibloo_hide_render", text="", icon="RESTRICT_RENDER_ON"
                )

            # MESH DATA TAB
            if general_subclass == "MESH DATA":
                layout.separator()
                layout.operator("object.khalibloo_name_object_data")
                layout.operator("object.khalibloo_copy_all_shape_keys")
                layout.operator("object.khalibloo_shrinkwrap_update")
                layout.separator()
                layout.label(text="Morph data:")
                layout.operator("object.khalibloo_bind_mesh")
                layout.operator("object.khalibloo_update_bound_mesh")
                # layout.prop(scene, "khalibloo_spread_harden_vgroups")
                # layout.operator("object.khalibloo_harden_weights")

            # MATERIALS TAB
            elif general_subclass == "MATERIALS":
                layout.separator()
                layout.operator(
                    "object.khalibloo_materials_remove",
                    text="Remove Materials",
                    icon="X",
                )
                layout.operator(
                    "object.khalibloo_assign_mat_indices", text="Assign Pass Indices"
                )

                layout.separator()
                row = layout.row(align=True)
                row.label(text="Textures:", icon="TEXTURE")
                row.operator(
                    "object.khalibloo_textures_on", text="", icon="RESTRICT_VIEW_OFF"
                )
                row.operator(
                    "object.khalibloo_textures_off", text="", icon="RESTRICT_VIEW_ON"
                )

            # MODIFIERS TAB
            elif general_subclass == "MODIFIERS":
                layout.separator()
                layout.prop(scene.khalibloo, "modifier_type", expand=False)
                layout.operator(
                    "object.khalibloo_add_modifier", text="Add Modifier", icon="ZOOM_IN"
                )

                layout.separator()
                layout.prop(scene.khalibloo, "modifier_filter_mode", expand=True)

                row = layout.row(align=True)
                row.operator(
                    "object.khalibloo_modifiers_realtime_on",
                    text="",
                    icon="RESTRICT_VIEW_OFF",
                )
                row.operator(
                    "object.khalibloo_modifiers_realtime_off",
                    text="",
                    icon="RESTRICT_VIEW_ON",
                )
                row.operator(
                    "object.khalibloo_modifiers_render_on",
                    text="",
                    icon="RESTRICT_RENDER_OFF",
                )
                row.operator(
                    "object.khalibloo_modifiers_render_off",
                    text="",
                    icon="RESTRICT_RENDER_ON",
                )
                # row.operator("object.khalibloo_modifiers_editmode_on", text='', icon='EDITMODE_HLT')
                # row.operator("object.khalibloo_modifiers_editmode_off", text='', icon='VIEW3D')
                row.operator("object.khalibloo_modifiers_apply", text="Apply")
                row.operator("object.khalibloo_modifiers_remove", text="", icon="X")

            # ARMATURES TAB
            elif general_subclass == "ARMATURES":
                layout.separator()
                layout.operator("object.khalibloo_generate_gamerig")
                layout.operator("object.khalibloo_guess_bone_parenting")
                layout.operator("object.khalibloo_rigify_neck_fix")

            # CONSTRAINTS TAB
            elif general_subclass == "CONSTRAINTS":
                layout.separator()
                row = layout.row(align=True)
                row.operator(
                    "object.khalibloo_constraints_unmute",
                    text="",
                    icon="RESTRICT_VIEW_OFF",
                )
                row.operator(
                    "object.khalibloo_constraints_mute",
                    text="",
                    icon="RESTRICT_VIEW_ON",
                )
                row.operator("object.khalibloo_constraints_remove", text="", icon="X")

                layout.separator()

                row = layout.row(align=True)
                row.label(text="Bone Constraints:", icon="CONSTRAINT_BONE")
                row = layout.row(align=True)
                row.operator(
                    "object.khalibloo_bone_constraints_unmute",
                    text="",
                    icon="RESTRICT_VIEW_OFF",
                )
                row.operator(
                    "object.khalibloo_bone_constraints_mute",
                    text="",
                    icon="RESTRICT_VIEW_ON",
                )
                row.operator(
                    "object.khalibloo_bone_constraints_remove", text="", icon="X"
                )

            # CUSTOM_OPS
            elif general_subclass == "CUSTOM_OPS":
                datatype = scene.khalibloo.custom_ops.datatype
                obj_subtype = scene.khalibloo.custom_ops.obj_subtype
                use_obj_type_filters = scene.khalibloo.custom_ops.use_obj_type_filters
                mat_subtype_01 = scene.khalibloo.custom_ops.mat_subtype_01
                mat_subtype_02 = scene.khalibloo.custom_ops.mat_subtype_02
                scene_subtype = scene.khalibloo.custom_ops.scene_subtype
                mod_scope = scene.khalibloo.custom_ops.mod_scope
                const_scope = scene.khalibloo.custom_ops.const_scope

                col = layout.column(align=True)
                box = col.box()
                box.label(text="Operate on:")
                box = col.box()
                row = box.row(align=True)
                if datatype == "OBJECTS":
                    row.prop(scene.khalibloo.custom_ops, "scope03", text="")
                elif datatype in ["SCENES", "WORLDS"]:
                    row.prop(scene.khalibloo.custom_ops, "scope05", text="")
                row.prop(scene.khalibloo.custom_ops, "datatype", text="")
                if datatype == "OBJECTS":
                    if obj_subtype == "MATERIALS":
                        row = box.row(align=True)
                        row.prop(scene.khalibloo.custom_ops, "scope01", text="")
                        row.prop(scene.khalibloo.custom_ops, "obj_subtype", text="")
                        if mat_subtype_01 != "NONE":
                            row = box.row(align=True)
                            row.prop(scene.khalibloo.custom_ops, "scope04_01", text="")
                            row.prop(scene.khalibloo.custom_ops, "mat_subtype_01", text="")
                        else:
                            row = box.row()
                            row.prop(scene.khalibloo.custom_ops, "mat_subtype_01", text="")
                    elif obj_subtype == "MODIFIERS":
                        row = box.row(align=True)
                        row.prop(scene.khalibloo.custom_ops, "mod_scope", text="")
                        row.prop(scene.khalibloo.custom_ops, "obj_subtype", text="")
                        if mod_scope == "SPECIFIC":
                            box.prop(scene.khalibloo.custom_ops, "mod_subtype", text="")
                    elif obj_subtype == "CONSTRAINTS":
                        row = box.row(align=True)
                        row.prop(scene.khalibloo.custom_ops, "const_scope", text="")
                        row.prop(scene.khalibloo.custom_ops, "obj_subtype", text="")
                        if const_scope == "SPECIFIC":
                            box.prop(scene.khalibloo.custom_ops, "const_subtype", text="")
                    elif obj_subtype in ["VERTEX_GROUPS", "SHAPE_KEYS"]:
                        row = box.row(align=True)
                        row.prop(scene.khalibloo.custom_ops, "scope01", text="")
                        row.prop(scene.khalibloo.custom_ops, "obj_subtype", text="")
                    elif obj_subtype in ["UV_MAPS", "VERTEX_COLORS"]:
                        row = box.row(align=True)
                        row.prop(scene.khalibloo.custom_ops, "scope01", text="")
                        row.prop(scene.khalibloo.custom_ops, "obj_subtype", text="")
                    else:
                        row = box.row()
                        row.prop(scene.khalibloo.custom_ops, "obj_subtype", text="")
                elif datatype == "MATERIALS":
                    if mat_subtype_02 != "NONE":
                        row = box.row(align=True)
                        row.prop(scene.khalibloo.custom_ops, "scope04_02", text="")
                        row.prop(scene.khalibloo.custom_ops, "mat_subtype_02", text="")
                    else:
                        row = box.row()
                        row.prop(scene.khalibloo.custom_ops, "mat_subtype_02", text="")
                elif datatype == "SCENES":
                    row = box.row()
                    row.prop(scene.khalibloo.custom_ops, "scene_subtype", text="")

                if datatype == "OBJECTS":
                    col = layout.column(align=True)
                    box = col.box()
                    split = box.split(percentage=0.2)
                    col1 = split.column()
                    # row = col.row()
                    col1.alignment = "LEFT"
                    col1.prop(scene.khalibloo.custom_ops, "use_obj_type_filters", text="")
                    col1 = split.column()
                    col1.alignment = "LEFT"
                    # TODO: add an "ALL" toggle to filters
                    col1.label(text="Object type filters:")
                    if use_obj_type_filters:
                        box = col.box()
                        split = box.split()
                        col = split.column()
                        col.prop(
                            scene,
                            "khalibloo.custom_ops.obj_type_filters",
                            text="Meshes",
                            toggle=True,
                            index=0,
                        )
                        col.prop(
                            scene,
                            "khalibloo.custom_ops.obj_type_filters",
                            text="Curves",
                            toggle=True,
                            index=1,
                        )
                        col.prop(
                            scene,
                            "khalibloo.custom_ops.obj_type_filters",
                            text="Armatures",
                            toggle=True,
                            index=2,
                        )
                        col.prop(
                            scene,
                            "khalibloo.custom_ops.obj_type_filters",
                            text="Nurbs",
                            toggle=True,
                            index=3,
                        )
                        col = split.column()
                        col.prop(
                            scene,
                            "khalibloo.custom_ops.obj_type_filters",
                            text="Metaballs",
                            toggle=True,
                            index=4,
                        )
                        col.prop(
                            scene,
                            "khalibloo.custom_ops.obj_type_filters",
                            text="Cameras",
                            toggle=True,
                            index=5,
                        )
                        col.prop(
                            scene,
                            "khalibloo.custom_ops.obj_type_filters",
                            text="Lamps",
                            toggle=True,
                            index=6,
                        )
                        col.prop(
                            scene,
                            "khalibloo.custom_ops.obj_type_filters",
                            text="Speakers",
                            toggle=True,
                            index=7,
                        )
                        col = split.column()
                        col.prop(
                            scene,
                            "khalibloo.custom_ops.obj_type_filters",
                            text="Texts",
                            toggle=True,
                            index=8,
                        )
                        col.prop(
                            scene,
                            "khalibloo.custom_ops.obj_type_filters",
                            text="Lattices",
                            toggle=True,
                            index=9,
                        )
                        col.prop(
                            scene,
                            "khalibloo.custom_ops.obj_type_filters",
                            text="Empties",
                            toggle=True,
                            index=10,
                        )

                layout.separator()
                layout.operator("object.khalibloo_opblock_add", icon="ZOOMIN")
                for block in scene.khalibloo_opblocks:
                    if block is not None:
                        layout.separator()
                        box = layout.box()
                        split = box.split(percentage=0.6)
                        col = split.column()
                        row = col.row(align=True)
                        row.alignment = "LEFT"
                        row.label(block.name)
                        col = split.column()
                        split = col.split(percentage=0.7)
                        col = split.column()
                        row = col.row(align=True)
                        row.alignment = "RIGHT"
                        # row.operator("object.khalibloo_opblock" + block.strIndex + "_move_up", text="", icon='TRIA_UP')
                        # row.operator("object.khalibloo_opblock" + block.strIndex + "_move_down", text="", icon='TRIA_DOWN')
                        col = split.column()
                        row = col.row()
                        row.alignment = "RIGHT"
                        row.operator(
                            "object.khalibloo_opblock" + block.strIndex + "_remove",
                            text="",
                            icon="X",
                        )
                        box.separator()

                        box.prop(scene, block.prefix + "action_mode")
                        box.prop(scene, block.prefix + "action")

                layout.separator()
                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_opblocks_execute", text="Execute")

        # -----------------------------------------------------------------------------------
        # GENESIS TOOLS
        elif platform == "DAZ GENESIS":
            # Genesis Object type
            layout.prop(scene.khalibloo, "genesis_platform_subclass", expand=True)
            layout.separator()

            # if it's a Genesis figure
            if genesis_subclass == "FIGURE":
                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_genesis_rigify_setup")

                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_genesis_rigify_vgroups")

                layout.operator("object.khalibloo_genesis_unrigify_vgroups")

                # Morphs
                layout.separator()
                layout.prop(scene.khalibloo, "genesis_morph_dir")

                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_import_genesis_morphs")

            # If it's a Genesis item
            elif genesis_subclass == "ITEM":

                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_genesis_rigify_vgroups")

                layout.operator("object.khalibloo_genesis_unrigify_vgroups")

        # -----------------------------------------------------------------------------------
        # GENESIS 2 TOOLS
        elif platform == "DAZ GENESIS 2":
            # Genesis Object type
            layout.prop(scene.khalibloo, "genesis2_platform_subclass", expand=True)
            layout.separator()

            # if it's a Genesis 2 Male
            if genesis2_subclass == "MALE":
                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_genesis2male_rigify_setup")

                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_genesis_rigify_vgroups")

                layout.operator("object.khalibloo_genesis_unrigify_vgroups")

                # Morphs
                layout.separator()
                layout.prop(scene.khalibloo, "genesis_morph_dir")

                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_import_genesis_morphs")

            # if it's a Genesis 2 Female
            if genesis2_subclass == "FEMALE":
                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_genesis2female_rigify_setup")

                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_genesis_rigify_vgroups")

                layout.operator("object.khalibloo_genesis_unrigify_vgroups")

                # Morphs
                layout.separator()
                layout.prop(scene.khalibloo, "genesis_morph_dir")

                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_import_genesis_morphs")

            # If it's a Genesis Item
            elif genesis2_subclass == "ITEM":
                # Big button
                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_genesis_rigify_vgroups")

                layout.operator("object.khalibloo_genesis_unrigify_vgroups")

        # -----------------------------------------------------------------------------------
        # GENESIS 3 TOOLS
        elif platform == "DAZ GENESIS 3":
            # Genesis Object type
            layout.prop(scene.khalibloo, "genesis3_platform_subclass", expand=True)
            layout.separator()

            # if it's a Genesis 3 Male
            if genesis3_subclass == "MALE":
                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_genesis3male_rigify_setup")

                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_genesis_rigify_vgroups")

                layout.operator("object.khalibloo_genesis_unrigify_vgroups")

                # Morphs
                layout.separator()
                layout.prop(scene.khalibloo, "genesis_morph_dir")

                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_import_genesis_morphs")

            # if it's a Genesis 3 Female
            if genesis3_subclass == "FEMALE":

                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_genesis3female_rigify_setup")

                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_genesis_rigify_vgroups")

                layout.operator("object.khalibloo_genesis_unrigify_vgroups")

                # Morphs
                layout.separator()
                layout.prop(scene.khalibloo, "genesis_morph_dir")

                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_import_genesis_morphs")

            # If it's a Genesis Item
            elif genesis3_subclass == "ITEM":
                # Big button
                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_genesis_rigify_vgroups")

                layout.operator("object.khalibloo_genesis_unrigify_vgroups")

        # -----------------------------------------------------------------------------------
        # GENESIS 8 TOOLS
        elif platform == "DAZ GENESIS 8":
            # Genesis Object type
            layout.prop(scene.khalibloo, "genesis8_platform_subclass", expand=True)
            layout.separator()

            # if it's a Genesis 8 Male
            if genesis8_subclass == "MALE":
                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_genesis8male_rigify_setup")

                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_genesis_rigify_vgroups")

                layout.operator("object.khalibloo_genesis_unrigify_vgroups")

                # Morphs
                layout.separator()
                layout.prop(scene.khalibloo, "genesis_morph_dir")

                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_import_genesis_morphs")

            # if it's a Genesis 8 Female
            if genesis8_subclass == "FEMALE":
                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_genesis8female_rigify_setup")

                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_genesis_rigify_vgroups")

                layout.operator("object.khalibloo_genesis_unrigify_vgroups")

                # Morphs
                layout.separator()
                layout.prop(scene.khalibloo, "genesis_morph_dir")

                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_import_genesis_morphs")

            # If it's a Genesis Item
            elif genesis8_subclass == "ITEM":
                # Big button
                row = layout.row()
                row.scale_y = 2.0
                row.operator("object.khalibloo_genesis_rigify_vgroups")

                layout.operator("object.khalibloo_genesis_unrigify_vgroups")


class KBP_PT_KhaliblooPanelUV(bpy.types.Panel):
    """Creates a Panel in the tool shelf of the UV/Image editor"""

    bl_label = "Khalibloo Panel"
    bl_idname = "KBP_PT_khalibloo_panel_uv"
    bl_space_type = "IMAGE_EDITOR"
    bl_region_type = "UI"

    def draw(self, context):
        layout = self.layout
        # scene = context.scene

        row = layout.row(align=True)
        row.operator("uv.khalibloo_uv_flatten_x", text="Flatten X")
        row.operator("uv.khalibloo_uv_flatten_y", text="Flatten Y")

        # layout.prop(scene, "khalibloo_image")
        # layout.template_ID(bpy.context.space_data, "image", new="image.new")

        # row = layout.row(align = True)
        # row.prop(scene, "khalibloo_batchbake_startframe")
        # row.prop(scene, "khalibloo_batchbake_endframe")


class KBP_PR_SceneProps(bpy.types.PropertyGroup):
    custom_ops : bpy.props.PointerProperty(type=custom_ops.KBP_PR_CustomOpsProps)
    modifiers = bpy.types.Modifier.bl_rna.properties["type"].enum_items

    genesis_platform_subclass : EnumProperty(
        items=(("FIGURE", "Figure", ""), ("ITEM", "Item", "")),
        name=" ",
        default="FIGURE",
    )
    genesis2_platform_subclass : EnumProperty(
        items=(("MALE", "Male", ""), ("FEMALE", "Female", ""), ("ITEM", "Item", "")),
        name=" ",
        default="MALE",
    )
    genesis3_platform_subclass : EnumProperty(
        items=(("MALE", "Male", ""), ("FEMALE", "Female", ""), ("ITEM", "Item", "")),
        name=" ",
        default="MALE",
    )
    genesis8_platform_subclass : EnumProperty(
        items=(("MALE", "Male", ""), ("FEMALE", "Female", ""), ("ITEM", "Item", "")),
        name=" ",
        default="MALE",
    )
    platform : EnumProperty(
        items=(
            ("GENERAL", "General", ""),
            ("DAZ GENESIS", "DAZ Genesis", ""),
            ("DAZ GENESIS 2", "DAZ Genesis 2", ""),
            ("DAZ GENESIS 3", "DAZ Genesis 3", ""),
            ("DAZ GENESIS 8", "DAZ Genesis 8", ""),
        ),
        name="Platform Type",
        description="Choose platform type",
        default="GENERAL",
    )
    general_platform_subclass : EnumProperty(
        items=(
            ("OBJECT DATA", "Object Data", "", "OBJECT_DATA", 0),
            ("MESH DATA", "Mesh Data", "", "MESH_DATA", 1),
            ("MATERIALS", "Materials", "", "MATERIAL", 2),
            ("MODIFIERS", "Modifiers", "", "MODIFIER", 3),
            ("ARMATURES", "Armatures", "", "ARMATURE_DATA", 4),
            ("CONSTRAINTS", "Constraints", "", "CONSTRAINT", 5),
            ("CUSTOM_OPS", "Custom Ops", "", "ZOOMIN", 6),
        ),
        name="",
        description="Type of tools to display",
        default="OBJECT DATA",
    )
    modifier_type : EnumProperty(
        items=tuple(
            [
                (md.identifier, md.name, md.description, md.icon, md.value)
                for md in modifiers
            ]
        ),
        name="",
        description="Select type of modifier to be affected by the buttons below",
        default="SUBSURF",
    )
    modifier_filter_mode : EnumProperty(
        items=(
            (
                "ALL",
                "All",
                "Actions will affect all types of modifiers of all selected objects",
            ),
            (
                "SPECIFIC",
                "Specific",
                "Actions will affect a specific type of modifier of all selected objects",
            ),
        ),
        name=" ",
        description="Choose whether to affect all types of modifiers or just a specific type",
        default="ALL",
    )
    spread_harden_vgroups : BoolProperty(
        name="Spread Vertex Groups",
        description="Whether or not vertex group weights may spread to vertices that are not initially part of the vertex groups",
        default=True,
    )
    affect_textures : BoolProperty(
        name="Textures",
        description="Whether or not the material setup affects textures as well",
        default=True,
    )
    merge_mats : BoolProperty(
        name="Merge Materials",
        description="Merge materials with the same diffuse textures. Warning: This will affect EVERY material slot in the active Genesis figure, not just the default Genesis materials",
        default=False,
    )
    genesis_morph_dir : StringProperty(
        name="",
        description="Folder where your Genesis morphs of choice are located",
        subtype="NONE",
        default="",
    )
    batchbake_images : EnumProperty(
        items=(("Default", "Default", ""), ("Untitled", "Untitled", "")),
        name="",
        description="Image to save as",
        default="Default",
    )
    batchbake_startframe : IntProperty(
        name="Start Frame",
        description="The frame from which to start baking",
        default=1,
    )
    batchbake_endframe : IntProperty(
        name="End Frame", description="The frame on which to end baking", default=250
    )


classes = (
    KBP_PT_KhaliblooPanel,
    KBP_PT_KhaliblooPanelUV,
    KBP_PR_SceneProps,
)


auto_load.init()

def register():
    auto_load.register()
    if is_280:
        from bpy.utils import register_class
        for cls in classes:
            register_class(cls)
    else:
        bpy.utils.register_module(__name__)
    bpy.types.Scene.khalibloo = bpy.props.PointerProperty(type=KBP_PR_SceneProps)

def unregister():
    del bpy.types.Scene.khalibloo
    if is_280:
        from bpy.utils import unregister_class
        for cls in reversed(classes):
            unregister_class(cls)
    else:
        bpy.utils.unregister_module(__name__)
    auto_load.unregister()

# def register():
#     utils.initialize()
#     custom_ops.initialize()
#     if is_280:
#         from bpy.utils import register_class

#         for cls in classes:
#             # try:
#             print(str(cls))
#             register_class(cls)
#             # except:
#     else:
#         bpy.utils.register_module(__name__)


# def unregister():
#     utils.destroy()
#     custom_ops.destroy()
#     if is_280:
#         from bpy.utils import unregister_class

#         for cls in reversed(classes):
#             unregister_class(cls)
#     else:
#         bpy.utils.unregister_module(__name__)


if __name__ == "__main__":
    register()
