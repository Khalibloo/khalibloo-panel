﻿# <pep8 compliant>
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

import bpy
from . import genesis_tools


# ============================================================================
# DEFINE FUNCTIONS
# ============================================================================

def setGenesis3FemaleRolls(metarig):
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.context.view_layer.objects.active = metarig
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.armature.select_all(action='DESELECT')
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_head].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_neck_upper].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_chest_upper].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_neck_base].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_chest].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_spine].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_hips].roll = 3.141593
    bpy.context.active_object.data.edit_bones["thigh.L"].roll = -0.050401
    bpy.context.active_object.data.edit_bones["shin.L"].roll = -0.015987
    bpy.context.active_object.data.edit_bones["foot.L"].roll = -1.210693
    bpy.context.active_object.data.edit_bones["toe.L"].roll = 1.538201
    bpy.context.active_object.data.edit_bones["heel.02.L"].roll = 0.0000
    bpy.context.active_object.data.edit_bones["shoulder.L"].roll = -0.006807
    bpy.context.active_object.data.edit_bones["upper_arm.L"].roll = 0.037177
    bpy.context.active_object.data.edit_bones["forearm.L"].roll = 1.635199
    bpy.context.active_object.data.edit_bones["hand.L"].roll = -0.009100
    bpy.context.active_object.data.edit_bones["thumb.01.L"].roll = 1.396754
    bpy.context.active_object.data.edit_bones["thumb.02.L"].roll = 1.538887
    bpy.context.active_object.data.edit_bones["thumb.03.L"].roll = 1.702941
    bpy.context.active_object.data.edit_bones["palm.01.L"].roll = 0.059231
    bpy.context.active_object.data.edit_bones["f_index.01.L"].roll = 0.107271
    bpy.context.active_object.data.edit_bones["f_index.02.L"].roll = 0.119477
    bpy.context.active_object.data.edit_bones["f_index.03.L"].roll = 0.044151
    bpy.context.active_object.data.edit_bones["palm.02.L"].roll = 0.025885
    bpy.context.active_object.data.edit_bones["f_middle.01.L"].roll = 0.045638
    bpy.context.active_object.data.edit_bones["f_middle.02.L"].roll = 0.167271
    bpy.context.active_object.data.edit_bones["f_middle.03.L"].roll = 0.029049
    bpy.context.active_object.data.edit_bones["palm.03.L"].roll = 0.038919
    bpy.context.active_object.data.edit_bones["f_ring.01.L"].roll = 0.078443
    bpy.context.active_object.data.edit_bones["f_ring.02.L"].roll = 0.090552
    bpy.context.active_object.data.edit_bones["f_ring.03.L"].roll = 0.03262
    bpy.context.active_object.data.edit_bones["palm.04.L"].roll = 0.110783
    bpy.context.active_object.data.edit_bones["f_pinky.01.L"].roll = 0.065368
    bpy.context.active_object.data.edit_bones["f_pinky.02.L"].roll = 0.061447
    bpy.context.active_object.data.edit_bones["f_pinky.03.L"].roll = -0.007734
    bpy.ops.armature.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')


def setGenesis3MaleRolls(metarig):
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.context.view_layer.objects.active = metarig
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.armature.select_all(action='DESELECT')
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_head].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_neck_upper].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_chest_upper].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_neck_base].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_chest].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_spine].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_hips].roll = 3.141593
    bpy.context.active_object.data.edit_bones["thigh.L"].roll = -0.050401
    bpy.context.active_object.data.edit_bones["shin.L"].roll = -0.015987
    bpy.context.active_object.data.edit_bones["foot.L"].roll = -1.210693
    bpy.context.active_object.data.edit_bones["toe.L"].roll = 1.538201
    bpy.context.active_object.data.edit_bones["heel.02.L"].roll = 0.0000
    bpy.context.active_object.data.edit_bones["shoulder.L"].roll = -0.006807
    bpy.context.active_object.data.edit_bones["upper_arm.L"].roll = 0.037177
    bpy.context.active_object.data.edit_bones["forearm.L"].roll = 1.635199
    bpy.context.active_object.data.edit_bones["hand.L"].roll = -0.009100
    bpy.context.active_object.data.edit_bones["thumb.01.L"].roll = 1.396754
    bpy.context.active_object.data.edit_bones["thumb.02.L"].roll = 1.538887
    bpy.context.active_object.data.edit_bones["thumb.03.L"].roll = 1.702941
    bpy.context.active_object.data.edit_bones["palm.01.L"].roll = 0.059231
    bpy.context.active_object.data.edit_bones["f_index.01.L"].roll = 0.107271
    bpy.context.active_object.data.edit_bones["f_index.02.L"].roll = 0.119477
    bpy.context.active_object.data.edit_bones["f_index.03.L"].roll = 0.044151
    bpy.context.active_object.data.edit_bones["palm.02.L"].roll = 0.025885
    bpy.context.active_object.data.edit_bones["f_middle.01.L"].roll = 0.045638
    bpy.context.active_object.data.edit_bones["f_middle.02.L"].roll = 0.167271
    bpy.context.active_object.data.edit_bones["f_middle.03.L"].roll = 0.029049
    bpy.context.active_object.data.edit_bones["palm.03.L"].roll = 0.038919
    bpy.context.active_object.data.edit_bones["f_ring.01.L"].roll = 0.078443
    bpy.context.active_object.data.edit_bones["f_ring.02.L"].roll = 0.090552
    bpy.context.active_object.data.edit_bones["f_ring.03.L"].roll = 0.03262
    bpy.context.active_object.data.edit_bones["palm.04.L"].roll = 0.110783
    bpy.context.active_object.data.edit_bones["f_pinky.01.L"].roll = 0.065368
    bpy.context.active_object.data.edit_bones["f_pinky.02.L"].roll = 0.061447
    bpy.context.active_object.data.edit_bones["f_pinky.03.L"].roll = -0.007734
    bpy.ops.armature.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')


# ============================================================================
# DEFINE OPERATORS
# ============================================================================


class KBP_OT_Genesis3FemaleRigifySetup(bpy.types.Operator):
    """Generate and setup a rigify rig for the active Genesis 3 Female figure"""
    bl_idname = "object.khalibloo_genesis3female_rigify_setup"
    bl_label = "Rigify"

    @classmethod
    def poll(cls, context):
        return ((context.active_object is not None) and (context.active_object.type == 'MESH'))

    def execute(self, context):
        genesis = bpy.context.active_object

        if (genesis.find_armature() is not None):
            if (len(genesis.data.vertices.items()) == 17418):
                # VERTEX GROUPS RAW DATA
                hips_head = [1713, 8611]
                head_tail = [79, 3958]
                pelvis_tail_L = [130, 164]
                neck_upper_head = [2306, 2307, 3095, 9204, 9205, 9993]
                chest_upper_head = [4453, 4454, 11321, 11322]
                breast_tail_L = [6957]
                toe_tail_L = [5043, 5046]
                heel_tail_L = [4745, 4758]
                heel02_head_L = [4090, 4091]
                heel02_tail_L = [3983, 4734]
                hand_tail_L = [4369, 4388, 4389, 6589]
                thumbtip_L = [5323, 5327]
                indextip_L = [5336, 5338]
                midtip_L = [5316, 5320]
                ringtip_L = [5306, 5310]
                pinkytip_L = [5300, 5302]
                # face rig
                eye_head_L = [16648, 16649, 16651, 16655, 16657, 16658, 16659, 16660, 16674, 16675, 16677, 16681, 16683, 16684, 16685,
                              16686, 16698, 16699, 16706, 16708, 16712, 16714, 16715, 16716, 16725, 16727, 16731, 16733, 16734, 16735, 16744, 16745]
                eye_tail_L = [16704]
                eye_head_R = [16810, 16811, 16813, 16817, 16819, 16820, 16821, 16822, 16836, 16837, 16839, 16843, 16845, 16846, 16847,
                              16848, 16860, 16861, 16868, 16870, 16874, 16876, 16877, 16878, 16887, 16889, 16893, 16895, 16896, 16897, 16906, 16907]
                eye_tail_R = [16866]
                jaw_head = [11]
                chin_head = [71, 6841, 6846, 13613]
                chin_001_head = [36, 64]
                chin_001_tail = [62]
                lip_head_B_L = [6914]
                lip_001_head_B_L = [6900]
                lip_head_T_L = [19, 6912]
                lip_001_head_T_L = [6904]
                lips_corner_L = [628, 629, 636]
                chin_head_L = [1664]
                jaw_001_head_L = [1665, 2491, 2518]
                jaw_head_L = [1668, 1671, 5682]
                temple_head_L = [701, 4817, 4818]
                cheek_001_head_B_L = [4795, 4798]
                forehead_head_L = [2621, 4806, 4807]
                forehead_001_head_L = [2615, 2616]
                forehead_002_head_L = [2610, 5700]
                nose_004_tail = [5619]
                nose_004_head = [2431, 9329]
                nose_003_head = [9, 66]
                nose_002_head = [42, 60, 5615, 12471]
                nose_001_head = [2394, 9292]
                nose_head = [5595, 12452]
                brow_003_head_T_L = [2374]
                brow_002_head_T_L = [776, 2319, 2584]
                brow_001_head_T_L = [5879]
                brow_head_T_L = [2513, 2515]
                nose_001_head_L = [2382, 2384, 3945]
                nose_head_L = [2413]
                cheek_001_head_T_L = [5668, 5670]
                brow_003_tail_B_L = [6763]
                brow_003_head_B_L = [2499, 6774]
                brow_002_head_B_L = [2502, 6782]
                brow_001_head_B_L = [2507, 6778, 6779]
                brow_head_B_L = [2504]
                eyelid_head_B_L = [1693, 2341]
                eyelid_001_head_B_L = [678]
                eyelid_002_head_B_L = [989, 994]
                eyelid_003_head_B_L = [672, 681]
                eyelid_head_T_L = [2367, 2368]
                eyelid_001_head_T_L = [610]
                eyelid_002_head_T_L = [616, 619]
                eyelid_003_head_T_L = [789]
                ear_004_head_L = [5979, 5980, 6885]
                ear_head_L = [2138, 2145, 2148, 6973]
                ear_001_head_L = [5845, 5973]
                ear_002_head_L = [5971, 6855]
                ear_003_head_L = [5969, 6850]
                teeth_head_T = [6195]
                teeth_head_B = [6304]
                teeth_tail_T = [6216, 13032]
                tongue_head = [6096, 6097]
                tongue_001_head = [6522, 13303]
                tongue_002_head = [6086, 12920]
                tongue_002_tail = [6785, 13561]
                teeth_tail_B = [6281, 13097]

                vgroupData = {
                    "head_tail": head_tail,
                    "neck_upper_head": neck_upper_head,
                    "chest_upper_head": chest_upper_head,
                    "chest_head": [],
                    "breast_tail_L": breast_tail_L,
                    "spine_head": [],
                    "hips_head": hips_head,
                    "pelvis_tail_L": pelvis_tail_L,
                    "toe_tail_L": toe_tail_L,
                    "heel_tail_L": heel_tail_L,
                    "heel02_head_L": heel02_head_L,
                    "heel02_tail_L": heel02_tail_L,
                    "hand_tail_L": hand_tail_L,
                    "thumbtip_L": thumbtip_L,
                    "indextip_L": indextip_L,
                    "midtip_L": midtip_L,
                    "ringtip_L": ringtip_L,
                    "pinkytip_L": pinkytip_L,
                    "indexcarp_L": [],
                    "midcarp_L": [],
                    "ringcarp_L": [],
                    "pinkycarp_L": [],
                    "eye_head_L": eye_head_L,
                    "eye_head_R": eye_head_R,
                    "eye_tail_L": eye_tail_L,
                    "eye_tail_R": eye_tail_R,
                    "jaw_head": jaw_head,
                    "chin_head": chin_head,
                    "chin_001_head": chin_001_head,
                    "chin_001_tail": chin_001_tail,
                    "lip_head_B_L": lip_head_B_L,
                    "lip_001_head_B_L": lip_001_head_B_L,
                    "lip_head_T_L": lip_head_T_L,
                    "lip_001_head_T_L": lip_001_head_T_L,
                    "lips_corner_L": lips_corner_L,
                    "nose_004_tail": nose_004_tail,
                    "nose_004_head": nose_004_head,
                    "nose_003_head": nose_003_head,
                    "nose_002_head": nose_002_head,
                    "nose_001_head": nose_001_head,
                    "nose_head": nose_head,
                    "brow_003_head_T_L": brow_003_head_T_L,
                    "brow_002_head_T_L": brow_002_head_T_L,
                    "brow_001_head_T_L": brow_001_head_T_L,
                    "brow_head_T_L": brow_head_T_L,
                    "nose_001_head_L": nose_001_head_L,
                    "nose_head_L": nose_head_L,
                    "cheek_001_head_T_L": cheek_001_head_T_L,
                    "cheek_001_head_B_L": cheek_001_head_B_L,
                    "chin_head_L": chin_head_L,
                    "jaw_001_head_L": jaw_001_head_L,
                    "jaw_head_L": jaw_head_L,
                    "temple_head_L": temple_head_L,
                    "forehead_head_L": forehead_head_L,
                    "forehead_001_head_L": forehead_001_head_L,
                    "forehead_002_head_L": forehead_002_head_L,
                    "ear_head_L": ear_head_L,
                    "ear_001_head_L": ear_001_head_L,
                    "ear_002_head_L": ear_002_head_L,
                    "ear_003_head_L": ear_003_head_L,
                    "ear_004_head_L": ear_004_head_L,
                    "eyelid_head_B_L": eyelid_head_B_L,
                    "eyelid_001_head_B_L": eyelid_001_head_B_L,
                    "eyelid_002_head_B_L": eyelid_002_head_B_L,
                    "eyelid_003_head_B_L": eyelid_003_head_B_L,
                    "eyelid_head_T_L": eyelid_head_T_L,
                    "eyelid_001_head_T_L": eyelid_001_head_T_L,
                    "eyelid_002_head_T_L": eyelid_002_head_T_L,
                    "eyelid_003_head_T_L": eyelid_003_head_T_L,
                    "teeth_head_B": teeth_head_B,
                    "teeth_tail_B": teeth_tail_B,
                    "teeth_head_T": teeth_head_T,
                    "teeth_tail_T": teeth_tail_T,
                    "tongue_head": tongue_head,
                    "tongue_001_head": tongue_001_head,
                    "tongue_002_head": tongue_002_head,
                    "tongue_002_tail": tongue_002_tail,
                    "brow_head_B_L": brow_head_B_L,
                    "brow_001_head_B_L": brow_001_head_B_L,
                    "brow_002_head_B_L": brow_002_head_B_L,
                    "brow_003_head_B_L": brow_003_head_B_L,
                    "brow_003_tail_B_L": brow_003_tail_B_L,
                }
                return genesis_tools.rigifySetup(self, '3', vgroupData, setGenesis3FemaleRolls)
        return {'CANCELLED'}


class KBP_OT_Genesis3MaleRigifySetup(bpy.types.Operator):
    """Generate and setup a rigify rig for the active Genesis 3 Male figure"""
    bl_idname = "object.khalibloo_genesis3male_rigify_setup"
    bl_label = "Rigify"

    @classmethod
    def poll(cls, context):
        return ((context.active_object is not None) and (context.active_object.type == 'MESH'))

    def execute(self, context):
        genesis = bpy.context.active_object

        if (genesis.find_armature() is not None):
            if (len(genesis.data.vertices.items()) == 17246):
                # VERTEX GROUPS RAW DATA
                hips_head = [3363, 9918]
                head_tail = [78, 3733]
                breast_tail_L = [16902, 17214]
                pelvis_tail_L = [1651, 1652]
                chest_upper_head = [4216, 10761]
                neck_upper_head = [2027, 2158, 2902, 8601, 8732, 9476]
                toe_tail_L = [4826]
                heel_tail_L = [4465]
                heel02_tail_L = [3756, 3757]
                heel02_head_L = [3865, 3866]
                hand_tail_L = [4140, 5613, 5615, 5623]
                thumbtip_L = [5043, 5047]
                indextip_L = [5056, 5058]
                midtip_L = [5036, 5040]
                ringtip_L = [5026, 5030]
                pinkytip_L = [5020, 5022]
                # face rig
                eye_tail_L = [16049]
                eye_head_L = [15993, 15994, 15996, 16000, 16002, 16003, 16004, 16005,
                              16019, 16020, 16022, 16026, 16028, 16029, 16030, 16031,
                              16043, 16044, 16051, 16053, 16057, 16059, 16060, 16061,
                              16070, 16072, 16076, 16078, 16079, 16080, 16089, 16090]
                eye_tail_R = [16211]
                eye_head_R = [16155, 16156, 16158, 16162, 16164, 16165, 16166, 16167,
                              16181, 16182, 16184, 16188, 16190, 16191, 16192, 16193,
                              16205, 16206, 16213, 16215, 16219, 16221, 16222, 16223,
                              16232, 16234, 16238, 16240, 16241, 16242, 16251, 16252]
                jaw_head = [11]
                chin_head = [71, 2377, 6506, 6513, 8951, 12956, 12963]
                chin_001_head = [36, 64, 2191, 2239, 8765, 8813]
                chin_001_tail = [62]
                chin_head_L = [1555, 1560]
                jaw_001_head_L = [1557, 2342, 2369]
                jaw_head_L = [1567, 5377, 5378]
                temple_head_L = [681, 4536, 4537]
                lip_head_B_L = [6586]
                lip_001_head_B_L = [6572]
                lip_head_T_L = [19, 6584]
                lip_001_head_T_L = [701, 6576]
                lips_corner_L = [707, 708, 709]
                cheek_001_head_B_L = [4515, 4518, 5510]
                brow_head_T_L = [2364]
                brow_001_head_T_L = [5575, 5586]
                brow_002_head_T_L = [756, 2411]
                brow_003_head_T_L = [2225]
                nose_004_tail = [21]
                nose_004_head = [2282, 8856]
                nose_003_head = [66, 5503]
                nose_002_head = [42, 60]
                nose_001_head = [65, 2245, 8819]
                nose_head = [5291, 5292, 11826]
                nose_001_head_L = [2235, 3691, 3721]
                nose_head_L = [2264, 5368]
                cheek_001_head_T_L = [2309, 2316, 5366]
                forehead_head_L = [4523, 4526]
                forehead_001_head_L = [2442]
                forehead_002_head_L = [2437, 5397]
                ear_004_head_L = [5675, 5676, 6557, 6633]
                ear_003_head_L = [5632, 5665, 6522,
                                  6536, 6547, 6549, 6630, 6631]
                ear_002_head_L = [5452, 5657, 5667, 6527, 6530, 6629, 6639]
                ear_001_head_L = [5669, 6532, 6636]
                ear_head_L = [1998, 2005, 5647]
                brow_003_tail_B_L = [6459]
                brow_003_head_B_L = [2350, 6457, 6470]
                brow_002_head_B_L = [2353, 6478]
                brow_001_head_B_L = [2356, 2358, 6474, 6475]
                brow_head_B_L = [2355]
                eyelid_head_B_L = [1577]
                eyelid_001_head_B_L = [655, 658]
                eyelid_002_head_B_L = [936, 941]
                eyelid_003_head_B_L = [652]
                eyelid_head_T_L = [2218, 2219, 2221]
                eyelid_001_head_T_L = [590]
                eyelid_002_head_T_L = [596, 599]
                eyelid_003_head_T_L = [769]
                tongue_head = [5793]
                tongue_001_head = [6218, 12677]
                tongue_002_head = [5782, 12294]
                tongue_002_tail = [6481, 12935]
                teeth_head_T = [5891]
                teeth_head_B = [6238]
                teeth_tail_T = [5912, 12406]
                teeth_tail_B = [6593, 13039]

                vgroupData = {
                    "head_tail": head_tail,
                    "neck_upper_head": neck_upper_head,
                    "chest_upper_head": chest_upper_head,
                    "chest_head": [],
                    "breast_tail_L": breast_tail_L,
                    "spine_head": [],
                    "hips_head": hips_head,
                    "pelvis_tail_L": pelvis_tail_L,
                    "toe_tail_L": toe_tail_L,
                    "heel_tail_L": heel_tail_L,
                    "heel02_head_L": heel02_head_L,
                    "heel02_tail_L": heel02_tail_L,
                    "hand_tail_L": hand_tail_L,
                    "thumbtip_L": thumbtip_L,
                    "indextip_L": indextip_L,
                    "midtip_L": midtip_L,
                    "ringtip_L": ringtip_L,
                    "pinkytip_L": pinkytip_L,
                    "indexcarp_L": [],
                    "midcarp_L": [],
                    "ringcarp_L": [],
                    "pinkycarp_L": [],
                    "eye_head_L": eye_head_L,
                    "eye_head_R": eye_head_R,
                    "eye_tail_L": eye_tail_L,
                    "eye_tail_R": eye_tail_R,
                    "jaw_head": jaw_head,
                    "chin_head": chin_head,
                    "chin_001_head": chin_001_head,
                    "chin_001_tail": chin_001_tail,
                    "lip_head_B_L": lip_head_B_L,
                    "lip_001_head_B_L": lip_001_head_B_L,
                    "lip_head_T_L": lip_head_T_L,
                    "lip_001_head_T_L": lip_001_head_T_L,
                    "lips_corner_L": lips_corner_L,
                    "nose_004_tail": nose_004_tail,
                    "nose_004_head": nose_004_head,
                    "nose_003_head": nose_003_head,
                    "nose_002_head": nose_002_head,
                    "nose_001_head": nose_001_head,
                    "nose_head": nose_head,
                    "brow_003_head_T_L": brow_003_head_T_L,
                    "brow_002_head_T_L": brow_002_head_T_L,
                    "brow_001_head_T_L": brow_001_head_T_L,
                    "brow_head_T_L": brow_head_T_L,
                    "nose_001_head_L": nose_001_head_L,
                    "nose_head_L": nose_head_L,
                    "cheek_001_head_T_L": cheek_001_head_T_L,
                    "cheek_001_head_B_L": cheek_001_head_B_L,
                    "chin_head_L": chin_head_L,
                    "jaw_001_head_L": jaw_001_head_L,
                    "jaw_head_L": jaw_head_L,
                    "temple_head_L": temple_head_L,
                    "forehead_head_L": forehead_head_L,
                    "forehead_001_head_L": forehead_001_head_L,
                    "forehead_002_head_L": forehead_002_head_L,
                    "ear_head_L": ear_head_L,
                    "ear_001_head_L": ear_001_head_L,
                    "ear_002_head_L": ear_002_head_L,
                    "ear_003_head_L": ear_003_head_L,
                    "ear_004_head_L": ear_004_head_L,
                    "eyelid_head_B_L": eyelid_head_B_L,
                    "eyelid_001_head_B_L": eyelid_001_head_B_L,
                    "eyelid_002_head_B_L": eyelid_002_head_B_L,
                    "eyelid_003_head_B_L": eyelid_003_head_B_L,
                    "eyelid_head_T_L": eyelid_head_T_L,
                    "eyelid_001_head_T_L": eyelid_001_head_T_L,
                    "eyelid_002_head_T_L": eyelid_002_head_T_L,
                    "eyelid_003_head_T_L": eyelid_003_head_T_L,
                    "teeth_head_B": teeth_head_B,
                    "teeth_tail_B": teeth_tail_B,
                    "teeth_head_T": teeth_head_T,
                    "teeth_tail_T": teeth_tail_T,
                    "tongue_head": tongue_head,
                    "tongue_001_head": tongue_001_head,
                    "tongue_002_head": tongue_002_head,
                    "tongue_002_tail": tongue_002_tail,
                    "brow_head_B_L": brow_head_B_L,
                    "brow_001_head_B_L": brow_001_head_B_L,
                    "brow_002_head_B_L": brow_002_head_B_L,
                    "brow_003_head_B_L": brow_003_head_B_L,
                    "brow_003_tail_B_L": brow_003_tail_B_L,
                }
                return genesis_tools.rigifySetup(self, '3', vgroupData, setGenesis3MaleRolls)
        return {'CANCELLED'}
