# <pep8 compliant>
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


import bpy
import bmesh

from bpy_extras.io_utils import ImportHelper, ExportHelper, path_reference_mode
from bpy.props import StringProperty


is_below_2_79 = float(bpy.app.version_string.split()[0]) < 2.79

# ============================================================================
# DEFINE FUNCTIONS
# ============================================================================


def isShapeKeyType(obj):
    if (obj.type == "MESH") or (obj.type == "CURVE") or (obj.type == "SURFACE"):
        return True
    else:
        return False


def isMaterialType(obj):
    if (
        (obj.type == "MESH")
        or (obj.type == "CURVE")
        or (obj.type == "SURFACE")
        or (obj.type == "FONT")
        or (obj.type == "META")
    ):
        return True
    else:
        return False


def isModifierType(obj):
    if (
        (obj.type == "MESH")
        or (obj.type == "CURVE")
        or (obj.type == "SURFACE")
        or (obj.type == "FONT")
        or (obj.type == "LATTICE")
    ):
        return True
    else:
        return False


def copyAllShapeKeys(sourceObject, targetObject):
    shapeKeyIndex = 1
    totalShapeKeyCount = len(sourceObject.data.shape_keys.key_blocks.items())
    while shapeKeyIndex < totalShapeKeyCount:
        copyShapeKey(shapeKeyIndex, sourceObject, targetObject)
        shapeKeyIndex = shapeKeyIndex + 1


def copyShapeKey(shapeKeyIndex, sourceObject, targetObject):
    bpy.ops.object.mode_set(mode="OBJECT")
    bpy.context.view_layer.objects.active = sourceObject
    bpy.context.active_object.active_shape_key_index = shapeKeyIndex
    bpy.context.view_layer.objects.active = targetObject
    bpy.ops.object.shape_key_transfer()


def updateShrinkwrap(context, obj):
    # safety checks
    if len(obj.modifiers) == 0:
        return "CANCELLED"

    modeBackup = context.object.mode
    viewShrinkwraps = []
    renderShrinkwraps = []
    disabledShrinkwraps = []
    for mod in obj.modifiers:
        # list all shrinkwrap modifiers
        if mod.type == "SHRINKWRAP":
            if mod.show_viewport:
                viewShrinkwraps.append(mod)
            elif mod.show_render:
                renderShrinkwraps.append(mod)
            else:
                disabledShrinkwraps.append(mod)
    # safety check
    if len(viewShrinkwraps) + len(renderShrinkwraps) + len(disabledShrinkwraps) == 0:
        return "CANCELLED"
    # choose
    if len(viewShrinkwraps) > 0:
        modifier = viewShrinkwraps[0]
    elif len(renderShrinkwraps) > 0:
        modifier = viewShrinkwraps[0]
    elif len(disabledShrinkwraps) > 0:
        modifier = viewShrinkwraps[0]
    bpy.ops.object.mode_set(mode="OBJECT")
    bpy.ops.object.modifier_copy(modifier=modifier.name)
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier=modifier.name)
    bpy.ops.object.mode_set(mode=modeBackup)
    return "FINISHED"


def bindMesh(context, activeObj, selectedObj, file):
    # security data
    context.view_layer.objects.active = selectedObj
    bpy.ops.object.mode_set(mode="EDIT")
    bm = bmesh.from_edit_mesh(bpy.data.meshes[selectedObj.data.name])
    vCount = str(len(bm.verts))
    eCount = str(len(bm.edges))
    fCount = str(len(bm.faces))
    bpy.ops.object.mode_set(mode="OBJECT")
    file.write(vCount + " " + eCount + " " + fCount + "\n")
    context.view_layer.objects.active = activeObj
    bpy.ops.object.mode_set(mode="EDIT")
    bm = bmesh.from_edit_mesh(bpy.data.meshes[activeObj.data.name])
    vCount = str(len(bm.verts))
    eCount = str(len(bm.edges))
    fCount = str(len(bm.faces))
    bpy.ops.object.mode_set(mode="OBJECT")
    file.write(vCount + " " + eCount + " " + fCount + "\n")

    context.view_layer.objects.active = selectedObj
    for vert in activeObj.data.vertices:
        location, normal, faceIndex = selectedObj.closest_point_on_mesh(vert.co)
        bpy.ops.object.mode_set(mode="EDIT")
        bm = bmesh.from_edit_mesh(bpy.data.meshes[selectedObj.data.name])
        vertList = []
        bm.faces.ensure_lookup_table()
        for v in bm.faces[faceIndex].verts:
            vertList.append(v.index)
        # center = bm.faces[index].calc_center_bounds()
        center = selectedObj.matrix_world * bm.faces[faceIndex].calc_center_median()
        # print(str(center))
        bpy.ops.object.mode_set(mode="OBJECT")
        delta = (activeObj.matrix_world * vert.co) - center

        output = ""
        for v in vertList:
            output = output + str(v) + " "
        output = (
            str(vert.index)
            + " "
            + str(delta.x)
            + " "
            + str(delta.y)
            + " "
            + str(delta.z)
            + " "
            + output
            + "\n"
        )
        file.write(output)
    file.close()


def updateBoundMesh(context, activeObj, selectedObj, filepath):
    f = open(filepath, "r", encoding="utf-8")
    f.seek(0, 0)  # set cursor to beginning of file
    # check if this file is compatible with the selected models
    line = f.readline()
    data = line.split()
    context.view_layer.objects.active = selectedObj
    bpy.ops.object.mode_set(mode="EDIT")
    bm = bmesh.from_edit_mesh(bpy.data.meshes[selectedObj.data.name])
    vCount = len(bm.verts)
    eCount = len(bm.edges)
    fCount = len(bm.faces)
    bpy.ops.object.mode_set(mode="OBJECT")
    if (vCount != int(data[0])) or (eCount != int(data[1])) or (fCount != int(data[2])):
        raise RuntimeError(
            "Reference object data doesn't match the data in the .txt file"
        )
        # self.report({'ERROR'}, "Reference object data doesn't match the data in the .txt file")
    line = f.readline()
    data = line.split()
    context.view_layer.objects.active = activeObj
    bpy.ops.object.mode_set(mode="EDIT")
    bm = bmesh.from_edit_mesh(bpy.data.meshes[activeObj.data.name])
    vCount = len(bm.verts)
    eCount = len(bm.edges)
    fCount = len(bm.faces)
    bpy.ops.object.mode_set(mode="OBJECT")
    if (vCount != int(data[0])) or (eCount != int(data[1])) or (fCount != int(data[2])):
        raise RuntimeError("Active object data doesn't match the data in the .txt file")
        # self.report({'ERROR'}, "Active object data doesn't match the data in the .txt file")
    # duplicate activeObj
    selectedObj.select_set(False)
    bpy.ops.object.duplicate()
    dupliObj = context.active_object
    # bpy.ops.object.shape_key_remove(all=True)
    selectedObj.select_set(True)
    context.view_layer.objects.active = selectedObj
    bpy.ops.object.mode_set(mode="EDIT")
    bm = bmesh.from_edit_mesh(selectedObj.data)
    delta = dupliObj.location  # just to initialize as Vector3
    vertCount = len(dupliObj.data.vertices)
    for i in range(0, vertCount):
        line = f.readline()
        data = line.split()
        vertCount = len(data) - 4
        # data[0] is just there for human readability. it's useless here.
        delta.x = float(data[1])
        delta.y = float(data[2])
        delta.z = float(data[3])
        vertList = []
        bm.verts.ensure_lookup_table()
        for v in range(4, len(data)):
            vertList.append(bm.verts[int(data[v])])
        face = bm.faces.get(vertList)  # Returns None if no face found
        if face is None:
            raise RuntimeError("Faces in Active and Reference meshes do not match")
        center = selectedObj.matrix_world * face.calc_center_median()
        dupliObj.data.vertices[i].co = dupliObj.matrix_world.inverted() * (
            center + delta
        )
    f.close()
    bpy.ops.object.mode_set(mode="OBJECT")
    context.view_layer.objects.active = activeObj
    selectedObj.select_set(False)
    bpy.ops.object.join_shapes()
    context.view_layer.objects.active = dupliObj
    activeObj.select_set(False)
    bpy.ops.object.delete()
    selectedObj.select_set(True)
    activeObj.select_set(True)
    context.view_layer.objects.active = activeObj
    activeObj.data.shape_keys.key_blocks[-1].name = "khalibloo_mesh_bind"
    activeObj.data.shape_keys.key_blocks[-1].value = 1.0


# ----------------------------------------------------------


def hardenWeights(obj, vgroupName, spread_vgroups):
    vgroupHard = obj.vertex_groups[vgroupName]
    hardList = []
    vertCount = len(obj.data.vertices.items())
    vertCheckList = []
    bpy.context.view_layer.objects.active = obj
    bpy.ops.object.mode_set(mode="EDIT")
    bm = bmesh.from_edit_mesh(bpy.data.meshes[obj.data.name])
    bpy.ops.mesh.select_all(action="DESELECT")
    obj.vertex_groups.active_index = vgroupHard.index
    bpy.ops.object.vertex_group_select()
    bpy.context.scene.tool_settings.vertex_group_weight = 1
    for i in range(0, vertCount):
        if bm.verts[i].select:
            hardList.append(i)
    bpy.ops.mesh.hide(unselected=True)
    bpy.ops.mesh.select_all(action="DESELECT")
    for i in range(0, vertCount):
        vert = bm.verts[i]
        if (vert.index in hardList) and (vert.index not in vertCheckList):
            vert.select_set(True)
            bpy.ops.mesh.select_linked(limit=False)
            connectedVertList = []
            for i in range(0, vertCount):
                if bm.verts[i].select:
                    connectedVertList.append(i)
            print("connected: " + str(connectedVertList))
            vgroupList = massFindVgroups(obj, connectedVertList)
            for vgroup in vgroupList:
                averageWeight = findAverageWeight(obj, vgroup.name, connectedVertList)
                print("average = " + str(averageWeight))
                bpy.context.scene.tool_settings.vertex_group_weight = averageWeight
                if not spread_vgroups:
                    bpy.ops.mesh.select_all(action="DESELECT")
                    obj.vertex_groups.active_index = vgroup.index
                    bpy.ops.object.vertex_group_select()
                for i in range(0, vertCount):
                    if bm.verts[i].select:
                        if i not in connectedVertList:
                            bm.verts[i].select_set(False)
                bpy.ops.object.vertex_group_assign()
                # for i in range(0, vertCount):
                # if (bm.verts[i].select == True):
                # vertCheckList.append(i)
                bpy.ops.mesh.reveal()
                bpy.ops.mesh.select_all(action="DESELECT")
    bpy.ops.object.mode_set(mode="OBJECT")


def findAverageWeight(obj, vgroupName, vertList):
    vgroup = obj.vertex_groups[vgroupName]
    vgroupContent = findVertices(obj, vgroupName)
    weightList = []
    for v in vertList:
        if v in vgroupContent:
            weight = vgroup.weight(v)
            if vgroup.weight(v) > 0:
                weightList.append(weight)
        else:
            weightList.append(0)
    averageWeight = sum(weightList) / len(weightList)
    return averageWeight


def massFindVgroups(obj, vertList):
    vgroupList = []
    vgroupCount = len(obj.vertex_groups.keys())
    for i in range(0, vgroupCount):
        vgroup = obj.vertex_groups[i]
        vgroupContent = findVertices(obj, vgroup.name)
        for v in vertList:
            if (v in vgroupContent) and (vgroup.weight(v) > 0):
                if vgroup not in vgroupList:
                    vgroupList.append(vgroup)
    return vgroupList


def findVertices(obj, vgroupName):
    vertList = []
    vgroup = obj.vertex_groups[vgroupName]
    vertCount = len(obj.data.vertices.items())
    bpy.context.view_layer.objects.active = obj
    bpy.ops.object.mode_set(mode="EDIT")
    bm = bmesh.from_edit_mesh(bpy.data.meshes[obj.data.name])
    bpy.ops.mesh.reveal()
    bpy.ops.mesh.select_all(action="DESELECT")
    obj.vertex_groups.active_index = vgroup.index
    bpy.ops.object.vertex_group_select()
    for v in range(0, vertCount):
        if bm.verts[v].select:
            vertList.append(v)
    return vertList


def findVgroups(obj, vIndex):
    vgroupList = []
    vgroupCount = len(obj.vertex_groups.keys())
    for i in range(0, vgroupCount):
        vgroup = obj.vertex_groups[i]
        if vgroup.weight(vIndex) > 0:
            vgroupList.append(vgroup)
    return vgroupList


def createVgroup(obj, bm, vgroupName, vertList):
    bpy.ops.mesh.select_all(action="DESELECT")
    bm.verts.ensure_lookup_table()
    for v in vertList:
        bm.verts[v].select_set(True)
    bpy.ops.object.vertex_group_add()
    obj.vertex_groups.active.name = vgroupName
    bpy.ops.object.vertex_group_assign()
    bpy.ops.mesh.select_all(action="DESELECT")


def delVgroup(obj, vgroupName):
    bpy.ops.object.mode_set(mode="OBJECT")
    bpy.context.view_layer.objects.active = obj
    vgroupIndex = obj.vertex_groups[vgroupName].index
    bpy.context.active_object.vertex_groups.active_index = vgroupIndex
    bpy.ops.object.vertex_group_remove()


def findCollections(obj):
    context = bpy.context
    collections = []
    for c in context.window.view_layer.layer_collection.children:
        if obj.name in c.collection.objects.keys():
            collections.append(c)
    return collections


def hideSelect():
    for obj in bpy.context.selected_objects:
        obj.hide_select = True


def unhideSelect():
    for obj in bpy.context.scene.objects:
        obj.hide_select = False


def hideRender():
    for obj in bpy.context.selected_objects:
        obj.hide_render = True


def unhideRender():
    for obj in bpy.context.selected_objects:
        obj.hide_render = False


def moveToJunk(context, obj):
    if "Junk"not in bpy.data.collections.keys():
        # Create a new collection and link it to the scene.
        junkColl = bpy.data.collections.new("Junk")
        context.scene.collection.children.link(junkColl)
    else:
        junkColl = bpy.data.collections["Junk"]
    # Link active object to the new collection
    junkColl.objects.link(obj)


# ------------------------------------------------------------------


def isNameExtension(refName, checkName):
    lenRefName = len(refName)
    lenCheckName = len(checkName)
    if lenCheckName in lenRefName:
        if refName[lenCheckName] == ".":
            if refName[lenCheckName:].isdigit():
                return True
    return False


def assignMatIndices():
    i = 1
    for j in range(0, len(bpy.data.materials)):
        bpy.data.materials[j].pass_index = i
        i += 1


def assignObjIndices(context):
    i = 1
    for obj in context.selected_objects:
        obj.pass_index = i
        i += 1


def materialsRemove(obj):
    matCount = len(obj.material_slots.keys())
    if matCount > 0:
        bpy.ops.object.mode_set(mode="OBJECT")
        bpy.context.view_layer.objects.active = obj
        for i in range(0, matCount):
            bpy.ops.object.material_slot_remove()


# ----------------------------------------------------------------------------


def modifiersRealTimeOff(obj, modFilter="ALL", modType="SUBSURF"):
    modList = obj.modifiers
    if len(modList) > 0:
        bpy.context.view_layer.objects.active = obj
        for mod in modList:
            if modFilter == "ALL":
                mod.show_viewport = False
            else:  # modFilter is 'SPECIFIC'
                if mod.type == modType:
                    mod.show_viewport = False


def modifiersRealTimeOn(obj, modFilter="ALL", modType="SUBSURF"):
    modList = obj.modifiers
    if len(modList) > 0:
        bpy.context.view_layer.objects.active = obj
        for mod in modList:
            if modFilter == "ALL":
                mod.show_viewport = True
            else:  # modFilter is 'SPECIFIC'
                if mod.type == modType:
                    mod.show_viewport = True


def modifiersRenderOff(obj, modFilter="ALL", modType="SUBSURF"):
    modList = obj.modifiers
    if len(modList) > 0:
        bpy.context.view_layer.objects.active = obj
        for mod in modList:
            if modFilter == "ALL":
                mod.show_render = False
            else:  # modFilter is 'SPECIFIC'
                if mod.type == modType:
                    mod.show_render = False


def modifiersRenderOn(obj, modFilter="ALL", modType="SUBSURF"):
    modList = obj.modifiers
    if len(modList) > 0:
        bpy.context.view_layer.objects.active = obj
        for mod in modList:
            if modFilter == "ALL":
                mod.show_render = True
            else:  # modFilter is 'SPECIFIC'
                if mod.type == modType:
                    mod.show_render = True


def modifiersEditModeOff(obj, modFilter="ALL", modType="SUBSURF"):
    modList = obj.modifiers
    if len(modList) > 0:
        bpy.context.view_layer.objects.active = obj
        for mod in modList:
            if modFilter == "ALL":
                mod.show_in_edit_mode = False
            else:  # modFilter is 'SPECIFIC'
                if mod.type == modType:
                    mod.show_in_edit_mode = False


def modifiersEditModeOn(obj, modFilter="ALL", modType="SUBSURF"):
    modList = obj.modifiers
    if len(modList) > 0:
        bpy.context.view_layer.objects.active = obj
        for mod in modList:
            if modFilter == "ALL":
                mod.show_in_edit_mode = True
            else:  # modFilter is 'SPECIFIC'
                if mod.type == modType:
                    mod.show_in_edit_mode = True


def modifiersApply(obj, modFilter="ALL", modType="SUBSURF"):
    modList = obj.modifiers
    if len(modList) > 0:
        bpy.ops.object.mode_set(mode="OBJECT")
        bpy.context.view_layer.objects.active = obj
        i = 0
        while i < len(modList):
            if modFilter == "ALL":
                bpy.ops.object.modifier_apply(apply_as="DATA", modifier=modList[i].name)
                # del modList[i]
            else:  # modFilter is 'SPECIFIC'
                if modList[i].type == modType:
                    bpy.ops.object.modifier_apply(
                        apply_as="DATA", modifier=modList[i].name
                    )
                else:
                    i += 1


def modifiersRemove(obj, modFilter="ALL", modType="SUBSURF"):
    modList = obj.modifiers
    if len(modList) > 0:
        bpy.ops.object.mode_set(mode="OBJECT")
        bpy.context.view_layer.objects.active = obj
        for mod in modList:
            if modFilter == "ALL":
                bpy.ops.object.modifier_remove(modifier=mod.name)
            else:  # modFilter is 'SPECIFIC'
                if mod.type == modType:
                    bpy.ops.object.modifier_remove(modifier=mod.name)


# --------------------------------------------------------------------------------


class Bone:
    """Bone objects can hold important bone data"""

    name = ""
    parent = ""
    connected = False
    inheritRot = False
    inheritScale = False
    localLoc = False
    roll = 0  # radians
    head = (0, 0, 0)
    tail = (0, 0, 0)
    # BBones
    segments = 1

    def __init__(
        self,
        name,
        parent,
        connected,
        inheritRot,
        inheritScale,
        localLoc,
        roll,
        head,
        tail,
        segments,
    ):
        self.name = name
        self.parent = parent
        self.connected = connected
        self.inheritRot = inheritRot
        self.inheritScale = inheritScale
        self.localLoc = localLoc
        self.roll = roll  # radians
        self.head = (head.x, head.y, head.z)
        self.tail = (tail.x, tail.y, tail.z)
        print(self.name)
        print(str(self.head))
        print(str(self.tail))
        # BBones
        self.segments = segments

    def __str__(self):
        return self.name


def initGamerigBone(editbone, boneData):
    # assume the gamerig is in edit mode
    editbone.name = boneData.name
    print(editbone.name)
    editbone.use_connect = boneData.connected
    editbone.use_inherit_rotation = boneData.inheritRot
    editbone.use_inherit_scale = boneData.inheritScale
    editbone.use_local_location = boneData.localLoc
    editbone.roll = boneData.roll
    editbone.head.xyz = boneData.head
    print(str(editbone.head) + " :: " + str(boneData.head))
    editbone.tail.xyz = boneData.tail
    # BBone
    editbone.bbone_segments = boneData.segments


def generateGamerig(animRig, context):
    # extract data
    bpy.ops.object.mode_set(mode="EDIT")
    # bpy.ops.mesh.select_all(action='DESELECT')
    bones = []
    for editbone in animRig.data.edit_bones:
        if editbone.use_deform:
            parent = None
            # if the parent isn't a deform bone,
            # we won't have it in the gamerig, so skip it
            if editbone.parent is not None and editbone.parent.use_deform:
                parent = editbone.parent.name
            bone = Bone(
                editbone.name,
                parent,
                editbone.use_connect,
                editbone.use_inherit_rotation,
                editbone.use_inherit_scale,
                editbone.use_local_location,
                editbone.roll,
                editbone.head,
                editbone.tail,
                editbone.bbone_segments,
            )
            # print(bone.name + " - head: " + str(bone.head) + " - tail: " + str(bone.tail))
            bones.append(bone)
    bpy.ops.object.mode_set(mode="OBJECT")
    # create game rig
    bpy.ops.object.armature_add()
    gamerig = context.active_object
    gamerig.name = "GAMERIG-" + animRig.name
    gamerig.location = animRig.location
    gamerig.rotation_euler = animRig.rotation_euler
    gamerig.scale = animRig.scale
    bpy.ops.object.mode_set(mode="EDIT")
    # delete existing bone
    bpy.ops.armature.select_all(action="SELECT")
    bpy.ops.armature.delete()
    # create bones
    for i in range(0, len(bones)):
        bpy.ops.armature.bone_primitive_add()
        editbone = gamerig.data.edit_bones[i]
        editbone.select_set(True)
        initGamerigBone(editbone, bones[i])
        editbone.select_set(False)
    # setup bone relationships
    for b in bones:
        bpy.ops.object.mode_set(mode="EDIT")
        editbone = gamerig.data.edit_bones[b.name]
        # parenting
        if b.parent is not None:
            editbone.parent = gamerig.data.edit_bones[b.parent]
        # setup bone constraints
        bpy.ops.object.mode_set(mode="POSE")
        posebone = gamerig.pose.bones[b.name]
        gamerig.data.bones.active = gamerig.data.bones[b.name]
        bpy.ops.pose.constraint_add(type="COPY_TRANSFORMS")
        gamerig.pose.bones[b.name].constraints[0].target = animRig
        gamerig.pose.bones[b.name].constraints[0].subtarget = posebone.name

    bpy.ops.object.mode_set(mode="OBJECT")


def guessBoneParenting(rig, context):
    bpy.ops.object.mode_set(mode="EDIT")
    for i in range(0, len(rig.data.edit_bones)):
        editbone = rig.data.edit_bones[i]
        if editbone.parent is None:
            for j in range(0, len(rig.data.edit_bones)):
                b = rig.data.edit_bones[j]
                if b.tail == editbone.head:
                    editbone.parent = b
                    break
    bpy.ops.object.mode_set(mode="OBJECT")


# --------------------------------------------------------------------------------


def muteConstraints(obj):
    if len(obj.constraints) > 0:
        for con in obj.constraints:
            con.mute = True


def unmuteConstraints(obj):
    if len(obj.constraints) > 0:
        for con in obj.constraints:
            con.mute = False


def removeConstraints(obj):
    bpy.context.view_layer.objects.active = obj
    if len(obj.constraints) > 0:
        bpy.ops.object.constraints_clear()


# -----------------------------------------------------------------------------------


def intsToBools(intsList):
    boolsList = []
    for i in range(0, len(intsList)):
        if intsList[i] == 0:
            boolsList.append(False)
        else:
            boolsList.append(True)
    print(str(boolsList))
    return boolsList


# ============================================================================
# DEFINE OPERATORS
# ============================================================================


class KBP_OT_NameObjectData(bpy.types.Operator):
    """Names object data for all objects"""

    bl_idname = "object.khalibloo_name_object_data"
    bl_label = "Name Object Data"

    @classmethod
    def poll(cls, context):
        return len(bpy.data.objects) > 0

    def execute(self, context):
        for obj in bpy.data.objects:
            try:
                obj.data.name = obj.name
            except (AttributeError):
                pass
        return {"FINISHED"}


class KBP_OT_CopyAllShapeKeys(bpy.types.Operator):
    """Copies all shape keys of selected object(s) to active object"""

    bl_idname = "object.khalibloo_copy_all_shape_keys"
    bl_label = "Copy All Shape Keys"

    @classmethod
    def poll(cls, context):
        return (context.active_object is not None) and (
            len(context.selected_objects) > 1
        )

    def execute(self, context):
        if isShapeKeyType(context.active_object):
            targetObject = bpy.context.active_object
            targetObjectIndex = bpy.context.selected_objects.index(
                bpy.context.active_object
            )
            selectionList = bpy.context.selected_objects
            del selectionList[targetObjectIndex]
            selectionSize = len(selectionList)
            currentObjectIndex = 0

            # to avoid problems with active object not having shape keys initially
            bpy.ops.object.shape_key_add(from_mix=False)
            shapeKeysCount = len(targetObject.data.shape_keys.key_blocks)

            # but if it already had a shape key, delete the one just created
            if shapeKeysCount > 1:
                bpy.ops.object.shape_key_remove()

            while currentObjectIndex < selectionSize:
                sourceObject = selectionList[currentObjectIndex]
                bpy.ops.object.select_all(action="DESELECT")
                sourceObject.select_set(True)
                bpy.context.active_object.select_set(True)
                copyAllShapeKeys(sourceObject, targetObject)
                currentObjectIndex = currentObjectIndex + 1

        return {"FINISHED"}


class KBP_OT_ShrinkwrapUpdate(bpy.types.Operator):
    """Updates geometry to match shrinkwrap modifier data"""

    bl_idname = "object.khalibloo_shrinkwrap_update"
    bl_label = "Update Shrinkwrap"

    @classmethod
    def poll(cls, context):
        return len(context.active_object.modifiers) > 0

    def execute(self, context):
        obj = context.active_object
        result = updateShrinkwrap(context, obj)
        return {result}


class KBP_OT_HardenWeights(bpy.types.Operator):
    """Harden the weights of the vertex groups of the selected objects."""

    bl_idname = "object.khalibloo_harden_weights"
    bl_label = "Harden Weights"

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        spread_vgroups = bpy.context.scene.khalibloo_spread_harden_vgroups
        objBackup = bpy.context.active_object
        for obj in bpy.context.selected_objects:
            hardenWeights(obj, "khalibloo_hard", spread_vgroups)

        bpy.context.view_layer.objects.active = objBackup
        return {"FINISHED"}


class KBP_OT_GamerigGenerate(bpy.types.Operator):
    """Generates a game engine friendly rig from the active armature"""

    bl_idname = "object.khalibloo_generate_gamerig"
    bl_label = "Generate Game Rig"

    @classmethod
    def poll(cls, context):
        return (context.active_object is not None) and (
            context.active_object.type == "ARMATURE"
        )

    def execute(self, context):
        generateGamerig(bpy.context.active_object, context)
        return {"FINISHED"}


class KBP_OT_GuessBoneParenting(bpy.types.Operator):
    """Attempt to find bone chains in the active armature and parent them accordingly"""

    bl_idname = "object.khalibloo_guess_bone_parenting"
    bl_label = "Guess Bone Parenting"

    @classmethod
    def poll(cls, context):
        return (context.active_object is not None) and (
            context.active_object.type == "ARMATURE"
        )

    def execute(self, context):
        guessBoneParenting(bpy.context.active_object, context)
        return {"FINISHED"}


class KBP_OT_ModifiersRealTimeOn(bpy.types.Operator):
    """Turn on real time display of modifiers of the selected objects"""

    bl_idname = "object.khalibloo_modifiers_realtime_on"
    bl_label = "Real Time Display On"

    @classmethod
    def poll(cls, context):
        return (len(context.selected_objects) > 0) and (
            context.active_object is not None
        )

    def execute(self, context):
        objBackup = bpy.context.active_object
        mod_filter_mode = bpy.context.scene.khalibloo_modifier_filter_mode
        mod_type = bpy.context.scene.khalibloo_modifier_type

        for obj in bpy.context.selected_objects:
            modifiersRealTimeOn(obj, mod_filter_mode, mod_type)

        bpy.context.view_layer.objects.active = objBackup
        return {"FINISHED"}


class KBP_OT_ModifiersRealTimeOff(bpy.types.Operator):
    """Turn off real time display of modifiers of the selected objects"""

    bl_idname = "object.khalibloo_modifiers_realtime_off"
    bl_label = "Real Time Display Off"

    @classmethod
    def poll(cls, context):
        return (len(context.selected_objects) > 0) and (
            context.active_object is not None
        )

    def execute(self, context):
        objBackup = bpy.context.active_object
        mod_filter_mode = bpy.context.scene.khalibloo_modifier_filter_mode
        mod_type = bpy.context.scene.khalibloo_modifier_type

        for obj in bpy.context.selected_objects:
            modifiersRealTimeOff(obj, mod_filter_mode, mod_type)

        bpy.context.view_layer.objects.active = objBackup
        return {"FINISHED"}


class KBP_OT_ModifiersRenderOn(bpy.types.Operator):
    """Turn on modifiers of the selected objects during rendering"""

    bl_idname = "object.khalibloo_modifiers_render_on"
    bl_label = "Render Display On"

    @classmethod
    def poll(cls, context):
        return (len(context.selected_objects) > 0) and (
            context.active_object is not None
        )

    def execute(self, context):
        objBackup = bpy.context.active_object
        mod_filter_mode = bpy.context.scene.khalibloo_modifier_filter_mode
        mod_type = bpy.context.scene.khalibloo_modifier_type

        for obj in bpy.context.selected_objects:
            modifiersRenderOn(obj, mod_filter_mode, mod_type)

        bpy.context.view_layer.objects.active = objBackup
        return {"FINISHED"}


class KBP_OT_ModifiersRenderOff(bpy.types.Operator):
    """Turn off modifiers of the selected objects during rendering"""

    bl_idname = "object.khalibloo_modifiers_render_off"
    bl_label = "Render Display Off"

    @classmethod
    def poll(cls, context):
        return (len(context.selected_objects) > 0) and (
            context.active_object is not None
        )

    def execute(self, context):
        objBackup = bpy.context.active_object
        mod_filter_mode = bpy.context.scene.khalibloo_modifier_filter_mode
        mod_type = bpy.context.scene.khalibloo_modifier_type

        for obj in bpy.context.selected_objects:
            modifiersRenderOff(obj, mod_filter_mode, mod_type)

        bpy.context.view_layer.objects.active = objBackup
        return {"FINISHED"}


class KBP_OT_ModifiersEditModeOn(bpy.types.Operator):
    """Turn on edit mode display of modifiers of the selected objects"""

    bl_idname = "object.khalibloo_modifiers_editmode_on"
    bl_label = "Edit Mode Display On"

    @classmethod
    def poll(cls, context):
        return (len(context.selected_objects) > 0) and (
            context.active_object is not None
        )

    def execute(self, context):
        objBackup = bpy.context.active_object
        mod_filter_mode = bpy.context.scene.khalibloo_modifier_filter_mode
        mod_type = bpy.context.scene.khalibloo_modifier_type

        for obj in bpy.context.selected_objects:
            modifiersEditModeOn(obj, mod_filter_mode, mod_type)

        bpy.context.view_layer.objects.active = objBackup
        return {"FINISHED"}


class KBP_OT_ModifiersEditModeOff(bpy.types.Operator):
    """Turn off edit mode display of modifiers of the selected objects"""

    bl_idname = "object.khalibloo_modifiers_editmode_off"
    bl_label = "Edit Mode Display Off"

    @classmethod
    def poll(cls, context):
        return (len(context.selected_objects) > 0) and (
            context.active_object is not None
        )

    def execute(self, context):
        objBackup = bpy.context.active_object
        mod_filter_mode = bpy.context.scene.khalibloo_modifier_filter_mode
        mod_type = bpy.context.scene.khalibloo_modifier_type

        for obj in bpy.context.selected_objects:
            modifiersEditModeOff(obj, mod_filter_mode, mod_type)

        bpy.context.view_layer.objects.active = objBackup
        return {"FINISHED"}


class KBP_OT_ModifiersApply(bpy.types.Operator):
    """Apply all modifiers of the selected objects in order"""

    bl_idname = "object.khalibloo_modifiers_apply"
    bl_label = "Apply"

    @classmethod
    def poll(cls, context):
        return (len(context.selected_objects) > 0) and (
            context.active_object is not None
        )

    def execute(self, context):
        objBackup = bpy.context.active_object
        mod_filter_mode = bpy.context.scene.khalibloo_modifier_filter_mode
        mod_type = bpy.context.scene.khalibloo_modifier_type

        for obj in bpy.context.selected_objects:
            modifiersApply(obj, mod_filter_mode, mod_type)

        bpy.context.view_layer.objects.active = objBackup
        return {"FINISHED"}


class KBP_OT_ModifiersRemove(bpy.types.Operator):
    """Delete all modifiers of the selected objects"""

    bl_idname = "object.khalibloo_modifiers_remove"
    bl_label = "Delete"

    @classmethod
    def poll(cls, context):
        return (len(context.selected_objects) > 0) and (
            context.active_object is not None
        )

    def execute(self, context):
        objBackup = bpy.context.active_object
        mod_filter_mode = bpy.context.scene.khalibloo_modifier_filter_mode
        mod_type = bpy.context.scene.khalibloo_modifier_type

        for obj in bpy.context.selected_objects:
            modifiersRemove(obj, mod_filter_mode, mod_type)

        bpy.context.view_layer.objects.active = objBackup
        return {"FINISHED"}


class KBP_OT_ConstraintsMute(bpy.types.Operator):
    """Mute all constraints of the selected objects"""

    bl_idname = "object.khalibloo_constraints_mute"
    bl_label = "Mute Constraints"

    @classmethod
    def poll(cls, context):
        return len(context.selected_objects) > 0

    def execute(self, context):
        objBackup = bpy.context.active_object

        for obj in bpy.context.selected_objects:
            muteConstraints(obj)

        bpy.context.view_layer.objects.active = objBackup
        return {"FINISHED"}


class KBP_OT_ConstraintsUnmute(bpy.types.Operator):
    """Unmute all constraints of the selected objects"""

    bl_idname = "object.khalibloo_constraints_unmute"
    bl_label = "Unmute Constraints"

    @classmethod
    def poll(cls, context):
        return len(context.selected_objects) > 0

    def execute(self, context):
        objBackup = bpy.context.active_object

        for obj in bpy.context.selected_objects:
            unmuteConstraints(obj)

        bpy.context.view_layer.objects.active = objBackup
        return {"FINISHED"}


class KBP_OT_ConstraintsRemove(bpy.types.Operator):
    """Delete all constraints of the selected objects"""

    bl_idname = "object.khalibloo_constraints_remove"
    bl_label = "Delete Constraints"

    @classmethod
    def poll(cls, context):
        return len(context.selected_objects) > 0

    def execute(self, context):
        objBackup = bpy.context.active_object

        for obj in bpy.context.selected_objects:
            removeConstraints(obj)

        bpy.context.view_layer.objects.active = objBackup
        return {"FINISHED"}


class KBP_OT_BoneConstraintsMute(bpy.types.Operator):
    """Mute all constraints of the selected bones"""

    bl_idname = "object.khalibloo_bone_constraints_mute"
    bl_label = "Mute Bone Constraints"

    @classmethod
    def poll(cls, context):
        return (context.mode == "POSE") and (len(context.selected_pose_bones) > 0)
        # the 2nd parameter would spit an error if we are not in pose mode
        # so, it's importaant to check for pose mode before counting selected pose bones

    def execute(self, context):
        for bone in bpy.context.selected_pose_bones:
            muteConstraints(bone)
        return {"FINISHED"}


class KBP_OT_BoneConstraintsUnmute(bpy.types.Operator):
    """Unmute all constraints of the selected bones"""

    bl_idname = "object.khalibloo_bone_constraints_unmute"
    bl_label = "Unmute Bone Constraints"

    @classmethod
    def poll(cls, context):
        return (context.mode == "POSE") and (len(context.selected_pose_bones) > 0)
        # the 2nd parameter would spit an error if we are not in pose mode
        # so, it's importaant to check for pose mode before counting selected pose bones

    def execute(self, context):
        for bone in bpy.context.selected_pose_bones:
            unmuteConstraints(bone)
        return {"FINISHED"}


class KBP_OT_BoneConstraintsRemove(bpy.types.Operator):
    """Delete all constraints of the selected bones"""

    bl_idname = "object.khalibloo_bone_constraints_remove"
    bl_label = "Delete Bone Constraints"

    @classmethod
    def poll(cls, context):
        return (context.mode == "POSE") and (len(context.selected_pose_bones) > 0)
        # the 2nd parameter would spit an error if we are not in pose mode
        # so, it's importaant to check for pose mode before counting selected pose bones

    def execute(self, context):
        for bone in bpy.context.selected_pose_bones:
            removeConstraints(bone)
        return {"FINISHED"}


class KBP_OT_MaterialsRemove(bpy.types.Operator):
    """Remove all materials from the selected objects"""

    bl_idname = "object.khalibloo_materials_remove"
    bl_label = "Remove Materials"

    @classmethod
    def poll(cls, context):
        return len(context.selected_objects) > 0

    def execute(self, context):
        objBackup = bpy.context.active_object

        for obj in bpy.context.selected_objects:
            materialsRemove(obj)

        bpy.context.view_layer.objects.active = objBackup
        return {"FINISHED"}


class KBP_OT_AssignMatIndices(bpy.types.Operator):
    """Assign unique pass indices to all materials"""

    bl_idname = "object.khalibloo_assign_mat_indices"
    bl_label = "Assign Mat Indices"

    @classmethod
    def poll(cls, context):
        return len(bpy.data.materials) > 0

    def execute(self, context):
        assignMatIndices()
        return {"FINISHED"}


class KBP_OT_AssignObjIndices(bpy.types.Operator):
    """Assign unique pass indices to selected objects"""

    bl_idname = "object.khalibloo_assign_obj_indices"
    bl_label = "Assign Object Indices"

    @classmethod
    def poll(cls, context):
        return len(context.selected_objects) > 0

    def execute(self, context):
        assignObjIndices(context)
        return {"FINISHED"}


class KBP_OT_HideSelect(bpy.types.Operator):
    """Make selected objects unselectable"""

    bl_idname = "object.khalibloo_hide_select"
    bl_label = "Hide Select"

    @classmethod
    def poll(cls, context):
        return len(context.selected_objects) > 0

    def execute(self, context):
        hideSelect()
        return {"FINISHED"}


class KBP_OT_UnhideSelect(bpy.types.Operator):
    """Make all objects selectable"""

    bl_idname = "object.khalibloo_unhide_select"
    bl_label = "Unhide Select"

    @classmethod
    def poll(cls, context):
        return len(context.scene.objects) > 0

    def execute(self, context):
        unhideSelect()
        return {"FINISHED"}


class KBP_OT_HideRender(bpy.types.Operator):
    """Make selected objects invisible in renders"""

    bl_idname = "object.khalibloo_hide_render"
    bl_label = "Hide Render"

    @classmethod
    def poll(cls, context):
        return len(context.selected_objects) > 0

    def execute(self, context):
        hideRender()
        return {"FINISHED"}


class KBP_OT_UnhideRender(bpy.types.Operator):
    """Make selected objects visible in renders"""

    bl_idname = "object.khalibloo_unhide_render"
    bl_label = "Unhide Render"

    @classmethod
    def poll(cls, context):
        return len(context.selected_objects) > 0

    def execute(self, context):
        unhideRender()
        return {"FINISHED"}


class KBP_OT_ModifierAdd(bpy.types.Operator):
    """Add the modifier specified above to the selected objects"""

    bl_idname = "object.khalibloo_add_modifier"
    bl_label = "Add Modifier"

    @classmethod
    def poll(cls, context):
        return (len(context.selected_objects) > 0) and (
            context.active_object is not None
        )

    def execute(self, context):
        objBackup = bpy.context.active_object
        mod_type = bpy.context.scene.khalibloo_modifier_type
        nonMeshModList = [
            "MESH_CACHE",
            "ARRAY",
            "BUILD",
            "DECIMATE",
            "EDGE_SPLIT",
            "MIRROR",
            "REMESH",
            "SCREW",
            "SOLIDIFY",
            "SUBSURF",
            "TRIANGULATE",
            "ARMATURE",
            "CAST",
            "CURVE",
            "HOOK",
            "LATTICE",
            "MESH_DEFORM",
            "SHRINKWRAP",
            "SIMPLE_DEFORM",
            "SMOOTH",
            "WARP",
            "WAVE",
            "SOFT_BODY",
        ]

        for obj in bpy.context.selected_objects:
            if obj.type == "MESH":
                bpy.context.view_layer.objects.active = obj
                bpy.ops.object.modifier_add(type=mod_type)

            # if it's not a mesh, but is still a modifiable object
            elif isModifierType(obj):
                if mod_type in nonMeshModList:
                    bpy.context.view_layer.objects.active = obj
                    bpy.ops.object.modifier_add(type=mod_type)

        bpy.context.view_layer.objects.active = objBackup
        return {"FINISHED"}


class KBP_OT_BindMesh(bpy.types.Operator, ExportHelper):
    """Bind mesh"""

    bl_idname = "object.khalibloo_bind_mesh"
    bl_label = "Bind Mesh"

    # ImportHelper mixin class uses this
    filename_ext = ".txt"

    filter_glob : StringProperty(default="*.txt", options={"HIDDEN"})

    path_mode : path_reference_mode
    check_extension = True

    @classmethod
    def poll(cls, context):
        if (context.active_object is not None) and (len(context.selected_objects) == 2):
            if (context.selected_objects[0].type == "MESH") and (
                context.selected_objects[1].type == "MESH"
            ):
                return True
        return False

    def execute(self, context):
        file = open(self.filepath, "w")
        bpy.ops.object.mode_set(mode="OBJECT")
        activeObj = context.active_object
        for obj in context.selected_objects:
            if obj == activeObj:
                pass
            else:
                selectedObj = obj
        bindMesh(context, activeObj, selectedObj, file)
        context.view_layer.objects.active = activeObj
        return {"FINISHED"}


class KBP_OT_UpdateBoundMesh(bpy.types.Operator, ImportHelper):
    """Update the position of each vertex in the active mesh to match the offsets of the selected mesh"""

    bl_idname = "object.khalibloo_update_bound_mesh"
    bl_label = "Update Bound Mesh"

    # ImportHelper mixin class uses this
    filename_ext = ".txt"

    filter_glob : StringProperty(default="*.txt", options={"HIDDEN"})

    @classmethod
    def poll(cls, context):
        if (context.active_object is not None) and (len(context.selected_objects) == 2):
            if (context.selected_objects[0].type == "MESH") and (
                context.selected_objects[1].type == "MESH"
            ):
                return True
        return False

    def execute(self, context):
        bpy.ops.object.mode_set(mode="OBJECT")
        activeObj = context.active_object
        for obj in context.selected_objects:
            if obj == activeObj:
                pass
            else:
                selectedObj = obj
        updateBoundMesh(context, activeObj, selectedObj, self.filepath)
        bpy.ops.object.mode_set(mode="OBJECT")
        context.view_layer.objects.active = activeObj
        return {"FINISHED"}


# ---------------------------------------------------------------------


class KBP_OT_FlattenUVx(bpy.types.Operator):
    """Flatten selected UV elements on the X axis"""

    bl_idname = "uv.khalibloo_uv_flatten_x"
    bl_label = "Flatten X"

    @classmethod
    def poll(cls, context):
        return (
            (len(context.selected_objects) > 0)
            and (context.active_object is not None)
            and (context.active_object.type == "MESH")
        )

    def execute(self, context):
        bpy.ops.transform.resize(
            value=(0, 1, 1),
            constraint_axis=(True, False, False),
            constraint_orientation="GLOBAL",
            proportional="DISABLED",
        )
        return {"FINISHED"}


class KBP_OT_FlattenUVy(bpy.types.Operator):
    """Flatten selected UV elements on the Y axis"""

    bl_idname = "uv.khalibloo_uv_flatten_y"
    bl_label = "Flatten Y"

    @classmethod
    def poll(cls, context):
        return (
            (len(context.selected_objects) > 0)
            and (context.active_object is not None)
            and (context.active_object.type == "MESH")
        )

    def execute(self, context):
        bpy.ops.transform.resize(
            value=(1, 0, 1),
            constraint_axis=(False, True, False),
            constraint_orientation="GLOBAL",
            proportional="DISABLED",
        )
        return {"FINISHED"}

