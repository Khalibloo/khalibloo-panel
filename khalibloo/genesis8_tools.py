﻿# <pep8 compliant>
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

import bpy
from . import genesis_tools


# ============================================================================
# DEFINE FUNCTIONS
# ============================================================================


def setGenesis8FemaleRolls(metarig):
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.context.view_layer.objects.active = metarig
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.armature.select_all(action='DESELECT')
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_head].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_neck_upper].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_chest_upper].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_neck_base].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_chest].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_spine].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_hips].roll = 3.141593
    metarig.data.edit_bones['thigh.L'].roll = -0.1987539827823639
    metarig.data.edit_bones['shin.L'].roll = -0.24811576306819916
    metarig.data.edit_bones['foot.L'].roll = -1.40442
    metarig.data.edit_bones['toe.L'].roll = 1.20484
    metarig.data.edit_bones['heel.02.L'].roll = 0.0
    metarig.data.edit_bones['shoulder.L'].roll = -0.006806999910622835
    metarig.data.edit_bones['upper_arm.L'].roll = 0.8412486910820007
    metarig.data.edit_bones['forearm.L'].roll = 2.3472933769226074
    metarig.data.edit_bones['hand.L'].roll = 0.91347736120224
    metarig.data.edit_bones['palm.01.L'].roll = 1.1152300834655762
    metarig.data.edit_bones['f_index.01.L'].roll = 1.0543365478515625
    metarig.data.edit_bones['f_index.02.L'].roll = 0.881854
    metarig.data.edit_bones['f_index.03.L'].roll = 0.9757823944091797
    metarig.data.edit_bones['thumb.01.L'].roll = 2.532963275909424
    metarig.data.edit_bones['thumb.02.L'].roll = 2.6262269020080566
    metarig.data.edit_bones['thumb.03.L'].roll = 2.580841541290283
    metarig.data.edit_bones['palm.02.L'].roll = 0.9207174181938171
    metarig.data.edit_bones['f_middle.01.L'].roll = 0.8925138711929321
    metarig.data.edit_bones['f_middle.02.L'].roll = 0.950425
    metarig.data.edit_bones['f_middle.03.L'].roll = 0.897122859954834
    metarig.data.edit_bones['palm.03.L'].roll = 0.8085457682609558
    metarig.data.edit_bones['f_ring.01.L'].roll = 0.8040608167648315
    metarig.data.edit_bones['f_ring.02.L'].roll = 0.8265069723129272
    metarig.data.edit_bones['f_ring.03.L'].roll = 0.7734014391899109
    metarig.data.edit_bones['palm.04.L'].roll = 0.6767524480819702
    metarig.data.edit_bones['f_pinky.01.L'].roll = 0.7353660464286804
    metarig.data.edit_bones['f_pinky.02.L'].roll = 0.5774012804031372
    metarig.data.edit_bones['f_pinky.03.L'].roll = 0.40224096179008484
    bpy.ops.object.mode_set(mode='OBJECT')


def setGenesis8MaleRolls(metarig):
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.context.view_layer.objects.active = metarig
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.armature.select_all(action='DESELECT')
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_head].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_neck_upper].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_chest_upper].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_neck_base].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_chest].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_spine].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_hips].roll = 3.141593
    metarig.data.edit_bones['thigh.L'].roll = -0.1987539827823639
    metarig.data.edit_bones['shin.L'].roll = -0.24811576306819916
    metarig.data.edit_bones['foot.L'].roll = -1.40442
    metarig.data.edit_bones['toe.L'].roll = 1.20484
    metarig.data.edit_bones['heel.02.L'].roll = 0.0
    metarig.data.edit_bones['shoulder.L'].roll = -0.006806999910622835
    metarig.data.edit_bones['upper_arm.L'].roll = 0.8412486910820007
    metarig.data.edit_bones['forearm.L'].roll = 2.3472933769226074
    metarig.data.edit_bones['hand.L'].roll = 0.91347736120224
    metarig.data.edit_bones['palm.01.L'].roll = 1.1152300834655762
    metarig.data.edit_bones['f_index.01.L'].roll = 1.0543365478515625
    metarig.data.edit_bones['f_index.02.L'].roll = 0.881854
    metarig.data.edit_bones['f_index.03.L'].roll = 0.9757823944091797
    metarig.data.edit_bones['thumb.01.L'].roll = 2.532963275909424
    metarig.data.edit_bones['thumb.02.L'].roll = 2.6262269020080566
    metarig.data.edit_bones['thumb.03.L'].roll = 2.580841541290283
    metarig.data.edit_bones['palm.02.L'].roll = 0.9207174181938171
    metarig.data.edit_bones['f_middle.01.L'].roll = 0.8925138711929321
    metarig.data.edit_bones['f_middle.02.L'].roll = 0.950425
    metarig.data.edit_bones['f_middle.03.L'].roll = 0.897122859954834
    metarig.data.edit_bones['palm.03.L'].roll = 0.8085457682609558
    metarig.data.edit_bones['f_ring.01.L'].roll = 0.8040608167648315
    metarig.data.edit_bones['f_ring.02.L'].roll = 0.8265069723129272
    metarig.data.edit_bones['f_ring.03.L'].roll = 0.7734014391899109
    metarig.data.edit_bones['palm.04.L'].roll = 0.6767524480819702
    metarig.data.edit_bones['f_pinky.01.L'].roll = 0.7353660464286804
    metarig.data.edit_bones['f_pinky.02.L'].roll = 0.5774012804031372
    metarig.data.edit_bones['f_pinky.03.L'].roll = 0.40224096179008484
    bpy.ops.object.mode_set(mode='OBJECT')


# ============================================================================
# DEFINE OPERATORS
# ============================================================================


class KBP_OT_Genesis8FemaleRigifySetup(bpy.types.Operator):
    """Generate and setup a rigify rig for the active Genesis 8 Female figure"""
    bl_idname = "object.khalibloo_genesis8female_rigify_setup"
    bl_label = "Rigify"

    @classmethod
    def poll(cls, context):
        return ((context.active_object is not None) and (context.active_object.type == 'MESH'))

    def execute(self, context):
        genesis = bpy.context.active_object

        if (genesis.find_armature() is not None):
            if (len(genesis.data.vertices.items()) == 16556):
                # VERTEX GROUPS RAW DATA
                hips_head = [3528, 3531, 3564, 3565,
                             10021, 10024, 10055, 10056]
                head_tail = [79, 5534]
                pelvis_tail_L = [1757, 1758]
                breast_tail_L = [6569]
                chest_upper_head = [4453, 10933]
                neck_upper_head = [2307, 8817]
                toe_tail_L = [4895, 4897]
                heel_tail_L = [4093, 4094, 4745, 4758]
                heel02_head_L = [4091]
                heel02_tail_L = [3980]
                hand_tail_L = [439, 1412]
                thumbtip_L = [5049, 5053]
                indextip_L = [5062, 5064]
                midtip_L = [5042, 5046]
                ringtip_L = [5032, 5036]
                pinkytip_L = [5026, 5028]
                # face rig
                eye_head_L = [15888, 15889, 15891, 15895, 15897, 15898, 15899, 15900, 15914, 15915, 15917, 15921, 15923, 15924, 15925,
                              15926, 15938, 15939, 15946, 15948, 15952, 15954, 15955, 15956, 15965, 15967, 15971, 15973, 15974, 15975, 15984, 15985]
                eye_tail_L = [15944]
                eye_head_R = [16018, 16019, 16021, 16025, 16027, 16028, 16029, 16030, 16044, 16045, 16047, 16051, 16053, 16054, 16055,
                              16056, 16068, 16069, 16076, 16078, 16082, 16084, 16085, 16086, 16095, 16097, 16101, 16103, 16104, 16105, 16114, 16115]
                eye_tail_R = [16074]

                jaw_head = [11, 1696, 8206]
                chin_head = [6453, 6458, 12837]
                chin_001_head = [38, 64]
                chin_001_tail = [61]
                lip_head_B_L = [6526]
                lip_001_head_B_L = [6512]
                lip_head_T_L = [19, 6524]
                lip_001_head_T_L = [6516]
                lips_corner_L = [727, 728, 729]
                chin_head_L = [1664]
                jaw_001_head_L = [1665, 2518]
                jaw_head_L = [1668, 1671, 5318]
                temple_head_L = [701, 4817, 4818]
                cheek_001_head_B_L = [4795, 4798]
                forehead_head_L = [2621, 4806, 4807]
                forehead_001_head_L = [2615, 2616]
                forehead_002_head_L = [2610, 5336, 5337]
                nose_004_tail = [5255]
                nose_004_head = [2431, 8941]
                nose_003_head = [2428, 8938]
                nose_002_head = [60]
                nose_001_head = [63, 65]
                nose_head = [5231, 11700]
                brow_003_head_T_L = [2374]
                brow_002_head_T_L = [776, 2319, 2584]
                brow_001_head_T_L = [2585]
                brow_head_T_L = [2513]
                nose_001_head_L = [3916, 3917]
                nose_head_L = [2408, 5308]
                cheek_001_head_T_L = [2458, 5306]
                brow_003_tail_B_L = [6375]
                brow_003_head_B_L = [6386]
                brow_002_head_B_L = [6394]
                brow_001_head_B_L = [6391]
                brow_head_B_L = [2504]
                eyelid_head_B_L = [1682, 1692]
                eyelid_001_head_B_L = [675, 678]
                eyelid_002_head_B_L = [989, 994]
                eyelid_003_head_B_L = [672, 681]
                eyelid_head_T_L = [2336]
                eyelid_001_head_T_L = [610]
                eyelid_002_head_T_L = [616, 619]
                eyelid_003_head_T_L = [789]
                ear_004_head_L = [5615, 5616, 6497]
                ear_head_L = [2138, 6585]
                ear_001_head_L = [5609, 6582]
                ear_002_head_L = [5607, 6467]
                ear_003_head_L = [5605, 6462, 6487, 6489]
                teeth_head_T = [5831]
                teeth_head_B = [5940]
                teeth_tail_T = [5852, 12280]
                teeth_tail_B = [5917, 12345]
                tongue_head = [5732, 5733]
                tongue_001_head = [6158, 12551]
                tongue_002_head = [5722, 12168]
                tongue_002_tail = [6400, 12787]

                vgroupData = {
                    "head_tail": head_tail,
                    "neck_upper_head": neck_upper_head,
                    "chest_upper_head": chest_upper_head,
                    "chest_head": [],
                    "breast_tail_L": breast_tail_L,
                    "spine_head": [],
                    "hips_head": hips_head,
                    "pelvis_tail_L": pelvis_tail_L,
                    "toe_tail_L": toe_tail_L,
                    "heel_tail_L": heel_tail_L,
                    "heel02_head_L": heel02_head_L,
                    "heel02_tail_L": heel02_tail_L,
                    "hand_tail_L": hand_tail_L,
                    "thumbtip_L": thumbtip_L,
                    "indextip_L": indextip_L,
                    "midtip_L": midtip_L,
                    "ringtip_L": ringtip_L,
                    "pinkytip_L": pinkytip_L,
                    "indexcarp_L": [],
                    "midcarp_L": [],
                    "ringcarp_L": [],
                    "pinkycarp_L": [],
                    "eye_head_L": eye_head_L,
                    "eye_head_R": eye_head_R,
                    "eye_tail_L": eye_tail_L,
                    "eye_tail_R": eye_tail_R,
                    "jaw_head": jaw_head,
                    "chin_head": chin_head,
                    "chin_001_head": chin_001_head,
                    "chin_001_tail": chin_001_tail,
                    "lip_head_B_L": lip_head_B_L,
                    "lip_001_head_B_L": lip_001_head_B_L,
                    "lip_head_T_L": lip_head_T_L,
                    "lip_001_head_T_L": lip_001_head_T_L,
                    "lips_corner_L": lips_corner_L,
                    "nose_004_tail": nose_004_tail,
                    "nose_004_head": nose_004_head,
                    "nose_003_head": nose_003_head,
                    "nose_002_head": nose_002_head,
                    "nose_001_head": nose_001_head,
                    "nose_head": nose_head,
                    "brow_003_head_T_L": brow_003_head_T_L,
                    "brow_002_head_T_L": brow_002_head_T_L,
                    "brow_001_head_T_L": brow_001_head_T_L,
                    "brow_head_T_L": brow_head_T_L,
                    "nose_001_head_L": nose_001_head_L,
                    "nose_head_L": nose_head_L,
                    "cheek_001_head_T_L": cheek_001_head_T_L,
                    "cheek_001_head_B_L": cheek_001_head_B_L,
                    "chin_head_L": chin_head_L,
                    "jaw_001_head_L": jaw_001_head_L,
                    "jaw_head_L": jaw_head_L,
                    "temple_head_L": temple_head_L,
                    "forehead_head_L": forehead_head_L,
                    "forehead_001_head_L": forehead_001_head_L,
                    "forehead_002_head_L": forehead_002_head_L,
                    "ear_head_L": ear_head_L,
                    "ear_001_head_L": ear_001_head_L,
                    "ear_002_head_L": ear_002_head_L,
                    "ear_003_head_L": ear_003_head_L,
                    "ear_004_head_L": ear_004_head_L,
                    "eyelid_head_B_L": eyelid_head_B_L,
                    "eyelid_001_head_B_L": eyelid_001_head_B_L,
                    "eyelid_002_head_B_L": eyelid_002_head_B_L,
                    "eyelid_003_head_B_L": eyelid_003_head_B_L,
                    "eyelid_head_T_L": eyelid_head_T_L,
                    "eyelid_001_head_T_L": eyelid_001_head_T_L,
                    "eyelid_002_head_T_L": eyelid_002_head_T_L,
                    "eyelid_003_head_T_L": eyelid_003_head_T_L,
                    "teeth_head_B": teeth_head_B,
                    "teeth_tail_B": teeth_tail_B,
                    "teeth_head_T": teeth_head_T,
                    "teeth_tail_T": teeth_tail_T,
                    "tongue_head": tongue_head,
                    "tongue_001_head": tongue_001_head,
                    "tongue_002_head": tongue_002_head,
                    "tongue_002_tail": tongue_002_tail,
                    "brow_head_B_L": brow_head_B_L,
                    "brow_001_head_B_L": brow_001_head_B_L,
                    "brow_002_head_B_L": brow_002_head_B_L,
                    "brow_003_head_B_L": brow_003_head_B_L,
                    "brow_003_tail_B_L": brow_003_tail_B_L,
                }
                return genesis_tools.rigifySetup(self, '8', vgroupData, setGenesis8FemaleRolls)
        return {'CANCELLED'}


class KBP_OT_Genesis8MaleRigifySetup(bpy.types.Operator):
    """Generate and setup a rigify rig for the active Genesis 8 Male figure"""
    bl_idname = "object.khalibloo_genesis8male_rigify_setup"
    bl_label = "Rigify"

    @classmethod
    def poll(cls, context):
        return ((context.active_object is not None) and (context.active_object.type == 'MESH'))

    def execute(self, context):
        genesis = bpy.context.active_object

        if (genesis.find_armature() is not None):
            if (len(genesis.data.vertices.items()) == 16384):
                # VERTEX GROUPS RAW DATA
                hips_head = [3327, 3330, 3363, 3364, 9496, 9499, 9530, 9531]
                head_tail = [78, 5230]
                pelvis_tail_L = [114, 144]
                neck_upper_head = [2156, 2157, 2158, 8342, 8343, 8344]
                chest_upper_head = [4216, 4217, 10373, 10374]
                breast_tail_L = [13736, 14048]
                toe_tail_L = [4615, 4617]
                heel_tail_L = [3868, 3869, 4465, 4478]
                heel02_tail_L = [3755]
                heel02_head_L = [3866]
                hand_tail_L = [419, 1359]
                thumbtip_L = [4769, 4773]
                indextip_L = [4782, 4784]
                midtip_L = [4762, 4766]
                ringtip_L = [4752, 4756]
                pinkytip_L = [4746, 4748]
                # face rig
                eye_tail_L = [15772]
                eye_head_L = [15716, 15717, 15719, 15723, 15725, 15726, 15727, 15728, 15742, 15743, 15745, 15749, 15751, 15752, 15753,
                              15754, 15766, 15767, 15774, 15776, 15780, 15782, 15783, 15784, 15793, 15795, 15799, 15801, 15802, 15803, 15812, 15813]
                eye_tail_R = [15902]
                eye_head_R = [15846, 15847, 15849, 15853, 15855, 15856, 15857, 15858, 15872, 15873, 15875, 15879, 15881, 15882, 15883,
                              15884, 15896, 15897, 15904, 15906, 15910, 15912, 15913, 15914, 15923, 15925, 15929, 15931, 15932, 15933, 15942, 15943]

                jaw_head = [11]
                chin_head = [6125, 6130, 12187]
                chin_001_head = [36, 64, 2191, 2239, 8377, 8425]
                chin_001_tail = [62]
                lip_head_B_L = [6198]
                lip_001_head_B_L = [6184]
                lip_head_T_L = [19, 6196]
                lip_001_head_T_L = [6188]
                lips_corner_L = [707, 708, 709]
                chin_head_L = [1560]
                jaw_001_head_L = [1561, 2342, 2369]
                jaw_head_L = [1564, 1567, 5014]
                temple_head_L = [681, 4537, 4538]
                cheek_001_head_B_L = [4515, 4518]
                forehead_head_L = [2448, 4526, 4527]
                forehead_001_head_L = [2442, 2443]
                forehead_002_head_L = [2437, 5032]
                nose_004_tail = [4951]
                nose_004_head = [2282, 8468]
                nose_003_head = [9, 66]
                nose_002_head = [42, 60]
                nose_001_head = [2245, 8431]
                nose_head = [4927, 11074]
                brow_003_head_T_L = [2225]
                brow_002_head_T_L = [756, 2170, 2411]
                brow_001_head_T_L = [2412]
                brow_head_T_L = [2364, 2366]
                nose_001_head_L = [2233, 2235, 3721]
                nose_head_L = [2264]
                cheek_001_head_T_L = [5000, 5002]
                brow_003_tail_B_L = [6071]
                brow_003_head_B_L = [2350, 6082]
                brow_002_head_B_L = [2353, 6090]
                brow_001_head_B_L = [2358, 6086, 6087]
                brow_head_B_L = [2355]
                eyelid_head_B_L = [1577, 1587]
                eyelid_001_head_B_L = [655, 658]
                eyelid_002_head_B_L = [936, 941]
                eyelid_003_head_B_L = [652, 661]
                eyelid_head_T_L = [2218, 2219]
                eyelid_001_head_T_L = [590]
                eyelid_002_head_T_L = [596, 599]
                eyelid_003_head_T_L = [766, 769]
                ear_004_head_L = [5311, 5312, 6169]
                ear_head_L = [1998, 2005, 2008, 6252]
                ear_001_head_L = [5305, 6249]
                ear_002_head_L = [5303, 6139]
                ear_003_head_L = [5301, 6134, 6159, 6161]
                teeth_head_T = [5527]
                teeth_head_B = [5636]
                teeth_tail_T = [5548, 11654]
                teeth_tail_B = [5613, 11719]
                tongue_head = [5428, 5429]
                tongue_001_head = [5854, 11925]
                tongue_002_head = [5418, 11542]
                tongue_002_tail = [6093, 6096, 12159, 12161]

                vgroupData = {
                    "head_tail": head_tail,
                    "neck_upper_head": neck_upper_head,
                    "chest_upper_head": chest_upper_head,
                    "chest_head": [],
                    "breast_tail_L": breast_tail_L,
                    "spine_head": [],
                    "hips_head": hips_head,
                    "pelvis_tail_L": pelvis_tail_L,
                    "toe_tail_L": toe_tail_L,
                    "heel_tail_L": heel_tail_L,
                    "heel02_head_L": heel02_head_L,
                    "heel02_tail_L": heel02_tail_L,
                    "hand_tail_L": hand_tail_L,
                    "thumbtip_L": thumbtip_L,
                    "indextip_L": indextip_L,
                    "midtip_L": midtip_L,
                    "ringtip_L": ringtip_L,
                    "pinkytip_L": pinkytip_L,
                    "indexcarp_L": [],
                    "midcarp_L": [],
                    "ringcarp_L": [],
                    "pinkycarp_L": [],
                    "eye_head_L": eye_head_L,
                    "eye_head_R": eye_head_R,
                    "eye_tail_L": eye_tail_L,
                    "eye_tail_R": eye_tail_R,
                    "jaw_head": jaw_head,
                    "chin_head": chin_head,
                    "chin_001_head": chin_001_head,
                    "chin_001_tail": chin_001_tail,
                    "lip_head_B_L": lip_head_B_L,
                    "lip_001_head_B_L": lip_001_head_B_L,
                    "lip_head_T_L": lip_head_T_L,
                    "lip_001_head_T_L": lip_001_head_T_L,
                    "lips_corner_L": lips_corner_L,
                    "nose_004_tail": nose_004_tail,
                    "nose_004_head": nose_004_head,
                    "nose_003_head": nose_003_head,
                    "nose_002_head": nose_002_head,
                    "nose_001_head": nose_001_head,
                    "nose_head": nose_head,
                    "brow_003_head_T_L": brow_003_head_T_L,
                    "brow_002_head_T_L": brow_002_head_T_L,
                    "brow_001_head_T_L": brow_001_head_T_L,
                    "brow_head_T_L": brow_head_T_L,
                    "nose_001_head_L": nose_001_head_L,
                    "nose_head_L": nose_head_L,
                    "cheek_001_head_T_L": cheek_001_head_T_L,
                    "cheek_001_head_B_L": cheek_001_head_B_L,
                    "chin_head_L": chin_head_L,
                    "jaw_001_head_L": jaw_001_head_L,
                    "jaw_head_L": jaw_head_L,
                    "temple_head_L": temple_head_L,
                    "forehead_head_L": forehead_head_L,
                    "forehead_001_head_L": forehead_001_head_L,
                    "forehead_002_head_L": forehead_002_head_L,
                    "ear_head_L": ear_head_L,
                    "ear_001_head_L": ear_001_head_L,
                    "ear_002_head_L": ear_002_head_L,
                    "ear_003_head_L": ear_003_head_L,
                    "ear_004_head_L": ear_004_head_L,
                    "eyelid_head_B_L": eyelid_head_B_L,
                    "eyelid_001_head_B_L": eyelid_001_head_B_L,
                    "eyelid_002_head_B_L": eyelid_002_head_B_L,
                    "eyelid_003_head_B_L": eyelid_003_head_B_L,
                    "eyelid_head_T_L": eyelid_head_T_L,
                    "eyelid_001_head_T_L": eyelid_001_head_T_L,
                    "eyelid_002_head_T_L": eyelid_002_head_T_L,
                    "eyelid_003_head_T_L": eyelid_003_head_T_L,
                    "teeth_head_B": teeth_head_B,
                    "teeth_tail_B": teeth_tail_B,
                    "teeth_head_T": teeth_head_T,
                    "teeth_tail_T": teeth_tail_T,
                    "tongue_head": tongue_head,
                    "tongue_001_head": tongue_001_head,
                    "tongue_002_head": tongue_002_head,
                    "tongue_002_tail": tongue_002_tail,
                    "brow_head_B_L": brow_head_B_L,
                    "brow_001_head_B_L": brow_001_head_B_L,
                    "brow_002_head_B_L": brow_002_head_B_L,
                    "brow_003_head_B_L": brow_003_head_B_L,
                    "brow_003_tail_B_L": brow_003_tail_B_L,
                }
                return genesis_tools.rigifySetup(self, '8', vgroupData, setGenesis8MaleRolls)
        return {'FINISHED'}
