﻿# <pep8 compliant>
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

import bpy
from . import genesis_tools


# ============================================================================
# DEFINE FUNCTIONS
# ============================================================================


def setGenesis2FemaleRolls(metarig):
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.context.view_layer.objects.active = metarig
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.armature.select_all(action='DESELECT')
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_head].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_neck_upper].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_chest_upper].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_neck_base].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_chest].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_spine].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_hips].roll = 3.141593
    bpy.context.active_object.data.edit_bones["thigh.L"].roll = -0.050401
    bpy.context.active_object.data.edit_bones["shin.L"].roll = -0.015987
    # 1.944836
    bpy.context.active_object.data.edit_bones["foot.L"].roll = 1.789502
    bpy.context.active_object.data.edit_bones["toe.L"].roll = 1.789502
    bpy.context.active_object.data.edit_bones["heel.02.L"].roll = 0.0000
    bpy.context.active_object.data.edit_bones["shoulder.L"].roll = -0.006807
    bpy.context.active_object.data.edit_bones["upper_arm.L"].roll = 0.037177
    bpy.context.active_object.data.edit_bones["forearm.L"].roll = 1.635199
    bpy.context.active_object.data.edit_bones["hand.L"].roll = -0.394818
    bpy.context.active_object.data.edit_bones["thumb.01.L"].roll = 3.296754
    bpy.context.active_object.data.edit_bones["thumb.02.L"].roll = 1.938887
    bpy.context.active_object.data.edit_bones["thumb.03.L"].roll = 2.002941
    bpy.context.active_object.data.edit_bones["palm.01.L"].roll = -0.152829
    bpy.context.active_object.data.edit_bones["f_index.01.L"].roll = -0.089205
    bpy.context.active_object.data.edit_bones["f_index.02.L"].roll = 0.446884
    bpy.context.active_object.data.edit_bones["f_index.03.L"].roll = 0.419287
    bpy.context.active_object.data.edit_bones["palm.02.L"].roll = -0.230173
    bpy.context.active_object.data.edit_bones["f_middle.01.L"].roll = -0.134652
    bpy.context.active_object.data.edit_bones["f_middle.02.L"].roll = 0.169072
    bpy.context.active_object.data.edit_bones["f_middle.03.L"].roll = 0.037349
    bpy.context.active_object.data.edit_bones["palm.03.L"].roll = -0.232888
    bpy.context.active_object.data.edit_bones["f_ring.01.L"].roll = -0.05762
    bpy.context.active_object.data.edit_bones["f_ring.02.L"].roll = 0.096312
    bpy.context.active_object.data.edit_bones["f_ring.03.L"].roll = 0.185756
    bpy.context.active_object.data.edit_bones["palm.04.L"].roll = -0.082646
    bpy.context.active_object.data.edit_bones["f_pinky.01.L"].roll = -0.235231
    bpy.context.active_object.data.edit_bones["f_pinky.02.L"].roll = 0.008269
    bpy.context.active_object.data.edit_bones["f_pinky.03.L"].roll = 0.182735
    bpy.ops.armature.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')


def setGenesis2MaleRolls(metarig):
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.context.view_layer.objects.active = metarig
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.armature.select_all(action='DESELECT')
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_head].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_neck_upper].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_chest_upper].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_neck_base].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_chest].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_spine].roll = 3.141593
    bpy.context.active_object.data.edit_bones[genesis_tools.metarig_hips].roll = 3.141593
    bpy.context.active_object.data.edit_bones["thigh.L"].roll = -0.050401
    bpy.context.active_object.data.edit_bones["shin.L"].roll = -0.015987
    bpy.context.active_object.data.edit_bones["foot.L"].roll = 1.658602
    # 2.005925
    bpy.context.active_object.data.edit_bones["toe.L"].roll = 1.658602
    bpy.context.active_object.data.edit_bones["heel.02.L"].roll = 0.0000
    bpy.context.active_object.data.edit_bones["shoulder.L"].roll = -0.006807
    bpy.context.active_object.data.edit_bones["upper_arm.L"].roll = 0.037177
    bpy.context.active_object.data.edit_bones["forearm.L"].roll = 1.635199
    bpy.context.active_object.data.edit_bones["hand.L"].roll = -0.394818
    bpy.context.active_object.data.edit_bones["thumb.01.L"].roll = 3.296754
    bpy.context.active_object.data.edit_bones["thumb.02.L"].roll = 1.938887
    bpy.context.active_object.data.edit_bones["thumb.03.L"].roll = 2.002941
    bpy.context.active_object.data.edit_bones["palm.01.L"].roll = -0.152829
    bpy.context.active_object.data.edit_bones["f_index.01.L"].roll = -0.089205
    bpy.context.active_object.data.edit_bones["f_index.02.L"].roll = 0.446884
    bpy.context.active_object.data.edit_bones["f_index.03.L"].roll = 0.419287
    bpy.context.active_object.data.edit_bones["palm.02.L"].roll = -0.230173
    bpy.context.active_object.data.edit_bones["f_middle.01.L"].roll = -0.134652
    bpy.context.active_object.data.edit_bones["f_middle.02.L"].roll = 0.169072
    bpy.context.active_object.data.edit_bones["f_middle.03.L"].roll = 0.037349
    bpy.context.active_object.data.edit_bones["palm.03.L"].roll = -0.232888
    bpy.context.active_object.data.edit_bones["f_ring.01.L"].roll = -0.05762
    bpy.context.active_object.data.edit_bones["f_ring.02.L"].roll = 0.096312
    bpy.context.active_object.data.edit_bones["f_ring.03.L"].roll = 0.185756
    bpy.context.active_object.data.edit_bones["palm.04.L"].roll = -0.082646
    bpy.context.active_object.data.edit_bones["f_pinky.01.L"].roll = -0.235231
    bpy.context.active_object.data.edit_bones["f_pinky.02.L"].roll = 0.008269
    bpy.context.active_object.data.edit_bones["f_pinky.03.L"].roll = 0.182735
    bpy.ops.armature.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')


# ============================================================================
# DEFINE OPERATORS
# ============================================================================


class KBP_OT_Genesis2FemaleRigifySetup(bpy.types.Operator):
    """Generate and setup a rigify rig for the active Genesis 2 Female figure"""
    bl_idname = "object.khalibloo_genesis2female_rigify_setup"
    bl_label = "Rigify"

    @classmethod
    def poll(cls, context):
        return ((context.active_object is not None) and (context.active_object.type == 'MESH'))

    def execute(self, context):
        genesis = bpy.context.active_object
        if (genesis.find_armature() is not None):
            if (len(genesis.data.vertices.items()) == 21556):
                # VERTEX GROUPS RAW DATA
                head_tail = [2054, 2087]
                chest_head = [10671, 21322]
                spine_head = [2926, 13724]
                hips_head = [7848, 18528]
                pelvis_tail_L = [7908, 7909, 7927, 7929, 7932, 7933, 8675]
                breast_tail_L = [0]
                chest_upper_head = [224, 228, 4011, 4012, 4060,
                                    4061, 11152, 11156, 14787, 14788, 14836, 14837]
                neck_upper_head = [2501, 2616, 7737, 9976, 10534, 10900, 10901, 13327, 13431,
                                   18420, 20635, 21189, 21535, 21536]
                toe_tail_L = [789]
                heel_tail_L = [4128, 4133]
                heel02_head_L = [9337]
                heel02_tail_L = [8623]
                hand_tail_L = [1076, 2957, 3988, 4176, 9423, 9445, 9446]
                thumbtip_L = [964, 965]
                indextip_L = [3380, 3452]
                midtip_L = [3528, 3600]
                ringtip_L = [3676, 3748]
                pinkytip_L = [3824, 3896]
                indexcarp_L = [9404, 9409, 9692]
                midcarp_L = [1085, 9430, 9437]
                ringcarp_L = [1083, 9459, 9465]
                pinkycarp_L = [1008, 9484, 9515]
                # face rig
                eye_head_L = [2191, 2192, 2195, 2197, 8267, 8268, 8271, 8273, 8301, 8323,
                              8325, 8327, 8357, 8359, 8361, 8382]
                eye_head_R = [13023, 13024, 13027, 13029, 18938, 18939, 18942, 18944, 18972,
                              18994, 18996, 18998, 19028, 19030, 19032, 19053]
                eye_tail_L = [8313]
                eye_tail_R = [18984]
                jaw_head = [2126, 7663, 18351]
                chin_head = [1966, 1968, 1981, 2122, 2123, 12894, 12896, 12909]
                chin_001_head = [7501, 7505, 18200]
                chin_001_tail = [7592, 7598]
                lip_head_B_L = [2135, 2136]
                lip_001_head_B_L = [1403, 1404, 1405, 1642]
                lip_head_T_L = [2144]
                lip_001_head_T_L = [1394, 1636]
                lips_corner_L = [1324, 1385, 1386]
                chin_head_L = [7499, 7515]
                jaw_001_head_L = [7487, 7511, 7517]
                jaw_head_L = [1418, 2481, 2490, 2491, 7537]
                temple_head_L = [3224, 3247, 9987]
                cheek_001_head_B_L = [7698, 7708, 7711]
                forehead_head_L = [3137, 3138, 3139]
                forehead_001_head_L = [1375, 3142]
                forehead_002_head_L = [1317, 3146, 3258]
                ear_004_head_L = [1442, 1477]
                ear_head_L = [1480, 1745, 9874, 9891]
                ear_001_head_L = [1448, 1454, 1455]
                ear_002_head_L = [1438, 1491, 9803, 9829]
                ear_003_head_L = [1458, 1460]
                nose_004_tail = [7419, 7420]
                nose_004_head = [2037, 2523, 12965, 13346]
                nose_003_head = [2112, 7428, 18125]
                nose_002_head = [7311, 7344, 7431, 7432, 18128, 18129]
                nose_001_head = [7305, 7396, 18095]
                nose_head = [2065, 3192, 7382, 7384, 18081, 18083]
                nose_001_head_L = [7323, 7325, 7330]
                nose_head_L = [7558, 7716]
                cheek_001_head_T_L = [7660]
                brow_head_T_L = [2483, 3296, 3298]
                brow_001_head_T_L = [1812, 3201, 3282]
                brow_002_head_T_L = [1934, 3233]
                brow_003_head_T_L = [1939, 1941]
                brow_003_tail_B_L = [7258, 7269]
                brow_003_head_B_L = [7361, 7373]
                brow_002_head_B_L = [7468, 7473]
                brow_001_head_B_L = [7465, 7470]
                brow_head_B_L = [7463]
                eyelid_head_B_L = [7580, 7581, 7582]
                eyelid_001_head_B_L = [1506]
                eyelid_002_head_B_L = [3188]
                eyelid_003_head_B_L = [1503]
                eyelid_head_T_L = [1809, 7539, 7547]
                eyelid_001_head_T_L = [1360]
                eyelid_002_head_T_L = [1366]
                eyelid_003_head_T_L = [2011]
                teeth_tail_T = [5188, 15936]
                teeth_head_T = [5326, 5333, 16073]
                teeth_tail_B = [5598, 16333]
                teeth_head_B = [5736, 5743, 16470]
                tongue_head = [4863]
                tongue_001_head = [4806, 15582]
                tongue_002_head = [4818, 15594]
                tongue_002_tail = [4883, 15633]

                vgroupData = {
                    "head_tail": head_tail,
                    "neck_upper_head": neck_upper_head,
                    "chest_upper_head": chest_upper_head,
                    "chest_head": chest_head,
                    "breast_tail_L": breast_tail_L,
                    "spine_head": spine_head,
                    "hips_head": hips_head,
                    "pelvis_tail_L": pelvis_tail_L,
                    "toe_tail_L": toe_tail_L,
                    "heel_tail_L": heel_tail_L,
                    "heel02_head_L": heel02_head_L,
                    "heel02_tail_L": heel02_tail_L,
                    "hand_tail_L": hand_tail_L,
                    "thumbtip_L": thumbtip_L,
                    "indextip_L": indextip_L,
                    "midtip_L": midtip_L,
                    "ringtip_L": ringtip_L,
                    "pinkytip_L": pinkytip_L,
                    "indexcarp_L": indexcarp_L,
                    "midcarp_L": midcarp_L,
                    "ringcarp_L": ringcarp_L,
                    "pinkycarp_L": pinkycarp_L,
                    "eye_head_L": eye_head_L,
                    "eye_head_R": eye_head_R,
                    "eye_tail_L": eye_tail_L,
                    "eye_tail_R": eye_tail_R,
                    "jaw_head": jaw_head,
                    "chin_head": chin_head,
                    "chin_001_head": chin_001_head,
                    "chin_001_tail": chin_001_tail,
                    "lip_head_B_L": lip_head_B_L,
                    "lip_001_head_B_L": lip_001_head_B_L,
                    "lip_head_T_L": lip_head_T_L,
                    "lip_001_head_T_L": lip_001_head_T_L,
                    "lips_corner_L": lips_corner_L,
                    "nose_004_tail": nose_004_tail,
                    "nose_004_head": nose_004_head,
                    "nose_003_head": nose_003_head,
                    "nose_002_head": nose_002_head,
                    "nose_001_head": nose_001_head,
                    "nose_head": nose_head,
                    "brow_003_head_T_L": brow_003_head_T_L,
                    "brow_002_head_T_L": brow_002_head_T_L,
                    "brow_001_head_T_L": brow_001_head_T_L,
                    "brow_head_T_L": brow_head_T_L,
                    "nose_001_head_L": nose_001_head_L,
                    "nose_head_L": nose_head_L,
                    "cheek_001_head_T_L": cheek_001_head_T_L,
                    "cheek_001_head_B_L": cheek_001_head_B_L,
                    "chin_head_L": chin_head_L,
                    "jaw_001_head_L": jaw_001_head_L,
                    "jaw_head_L": jaw_head_L,
                    "temple_head_L": temple_head_L,
                    "forehead_head_L": forehead_head_L,
                    "forehead_001_head_L": forehead_001_head_L,
                    "forehead_002_head_L": forehead_002_head_L,
                    "ear_head_L": ear_head_L,
                    "ear_001_head_L": ear_001_head_L,
                    "ear_002_head_L": ear_002_head_L,
                    "ear_003_head_L": ear_003_head_L,
                    "ear_004_head_L": ear_004_head_L,
                    "eyelid_head_B_L": eyelid_head_B_L,
                    "eyelid_001_head_B_L": eyelid_001_head_B_L,
                    "eyelid_002_head_B_L": eyelid_002_head_B_L,
                    "eyelid_003_head_B_L": eyelid_003_head_B_L,
                    "eyelid_head_T_L": eyelid_head_T_L,
                    "eyelid_001_head_T_L": eyelid_001_head_T_L,
                    "eyelid_002_head_T_L": eyelid_002_head_T_L,
                    "eyelid_003_head_T_L": eyelid_003_head_T_L,
                    "teeth_head_B": teeth_head_B,
                    "teeth_tail_B": teeth_tail_B,
                    "teeth_head_T": teeth_head_T,
                    "teeth_tail_T": teeth_tail_T,
                    "tongue_head": tongue_head,
                    "tongue_001_head": tongue_001_head,
                    "tongue_002_head": tongue_002_head,
                    "tongue_002_tail": tongue_002_tail,
                    "brow_head_B_L": brow_head_B_L,
                    "brow_001_head_B_L": brow_001_head_B_L,
                    "brow_002_head_B_L": brow_002_head_B_L,
                    "brow_003_head_B_L": brow_003_head_B_L,
                    "brow_003_tail_B_L": brow_003_tail_B_L,
                }
                return genesis_tools.rigifySetup(self, '2', vgroupData, setGenesis2FemaleRolls)
        return {'CANCELLED'}


class KBP_OT_Genesis2MaleRigifySetup(bpy.types.Operator):
    """Generate and setup a rigify rig for the active Genesis 2 Male figure"""
    bl_idname = "object.khalibloo_genesis2male_rigify_setup"
    bl_label = "Rigify"

    @classmethod
    def poll(cls, context):
        return ((context.active_object is not None) and (context.active_object.type == 'MESH'))

    def execute(self, context):
        genesis = bpy.context.active_object
        if (genesis.find_armature() is not None):
            if (len(genesis.data.vertices.items()) == 21556):
                # VERTEX GROUPS RAW DATA
                head_tail = [2054, 2087]
                chest_head = [10671, 21322]
                spine_head = [2926, 13724]
                hips_head = [7848, 18528]
                pelvis_tail_L = [7908, 7909, 7927, 7929, 7932, 7933, 8675]
                breast_tail_L = [0]
                chest_upper_head = [224, 228, 4011, 4012, 4060,
                                    4061, 11152, 11156, 14787, 14788, 14836, 14837]
                neck_upper_head = [2501, 2616, 7737, 9976, 10534, 10900, 10901, 13327, 13431,
                                   18420, 20635, 21189, 21535, 21536]
                toe_tail_L = [789]
                heel_tail_L = [4128, 4133]
                heel02_head_L = [9337]
                heel02_tail_L = [8623]
                hand_tail_L = [1076, 2957, 3988, 4176, 9423, 9445, 9446]
                thumbtip_L = [964, 965]
                indextip_L = [3380, 3452]
                midtip_L = [3528, 3600]
                ringtip_L = [3676, 3748]
                pinkytip_L = [3824, 3896]
                indexcarp_L = [9404, 9409, 9692]
                midcarp_L = [1085, 9430, 9437]
                ringcarp_L = [1083, 9459, 9465]
                pinkycarp_L = [1008, 9484, 9515]
                # face rig
                eye_head_L = [2191, 2192, 2195, 2197, 8267, 8268, 8271, 8273, 8301, 8323,
                              8325, 8327, 8357, 8359, 8361, 8382]
                eye_head_R = [13023, 13024, 13027, 13029, 18938, 18939, 18942, 18944, 18972,
                              18994, 18996, 18998, 19028, 19030, 19032, 19053]
                eye_tail_L = [8313]
                eye_tail_R = [18984]
                jaw_head = [2126, 7663, 18351]
                chin_head = [1966, 1968, 1981, 2122, 2123, 12894, 12896, 12909]
                chin_001_head = [7501, 7505, 18200]
                chin_001_tail = [7592, 7598]
                lip_head_B_L = [2135, 2136]
                lip_001_head_B_L = [1403, 1404, 1405, 1642]
                lip_head_T_L = [2144]
                lip_001_head_T_L = [1394, 1636]
                lips_corner_L = [1324, 1385, 1386]
                chin_head_L = [7499, 7515]
                jaw_001_head_L = [7487, 7511, 7517]
                jaw_head_L = [1418, 2481, 2490, 2491, 7537]
                temple_head_L = [3224, 3247, 9987]
                cheek_001_head_B_L = [7698, 7708, 7711]
                forehead_head_L = [3137, 3138, 3139]
                forehead_001_head_L = [1375, 3142]
                forehead_002_head_L = [1317, 3146, 3258]
                ear_004_head_L = [1442, 1477]
                ear_head_L = [1480, 1745, 9874, 9891]
                ear_001_head_L = [1448, 1454, 1455]
                ear_002_head_L = [1438, 1491, 9803, 9829]
                ear_003_head_L = [1458, 1460]
                nose_004_tail = [7419, 7420]
                nose_004_head = [2037, 2523, 12965, 13346]
                nose_003_head = [2112, 7428, 18125]
                nose_002_head = [7311, 7344, 7431, 7432, 18128, 18129]
                nose_001_head = [7305, 7396, 18095]
                nose_head = [2065, 3192, 7382, 7384, 18081, 18083]
                nose_001_head_L = [7323, 7325, 7330]
                nose_head_L = [7558, 7716]
                cheek_001_head_T_L = [7660]
                brow_head_T_L = [2483, 3296, 3298]
                brow_001_head_T_L = [1812, 3201, 3282]
                brow_002_head_T_L = [1934, 3233]
                brow_003_head_T_L = [1939, 1941]
                brow_003_tail_B_L = [7258, 7269]
                brow_003_head_B_L = [7361, 7373]
                brow_002_head_B_L = [7468, 7473]
                brow_001_head_B_L = [7465, 7470]
                brow_head_B_L = [7463]
                eyelid_head_B_L = [7580, 7581, 7582]
                eyelid_001_head_B_L = [1506]
                eyelid_002_head_B_L = [3188]
                eyelid_003_head_B_L = [1503]
                eyelid_head_T_L = [1809, 7539, 7547]
                eyelid_001_head_T_L = [1360]
                eyelid_002_head_T_L = [1366]
                eyelid_003_head_T_L = [2011]
                teeth_tail_T = [5188, 15936]
                teeth_head_T = [5326, 5333, 16073]
                teeth_tail_B = [5598, 16333]
                teeth_head_B = [5736, 5743, 16470]
                tongue_head = [4863]
                tongue_001_head = [4806, 15582]
                tongue_002_head = [4818, 15594]
                tongue_002_tail = [4883, 15633]

                vgroupData = {
                    "head_tail": head_tail,
                    "neck_upper_head": neck_upper_head,
                    "chest_upper_head": chest_upper_head,
                    "chest_head": chest_head,
                    "breast_tail_L": breast_tail_L,
                    "spine_head": spine_head,
                    "hips_head": hips_head,
                    "pelvis_tail_L": pelvis_tail_L,
                    "toe_tail_L": toe_tail_L,
                    "heel_tail_L": heel_tail_L,
                    "heel02_head_L": heel02_head_L,
                    "heel02_tail_L": heel02_tail_L,
                    "hand_tail_L": hand_tail_L,
                    "thumbtip_L": thumbtip_L,
                    "indextip_L": indextip_L,
                    "midtip_L": midtip_L,
                    "ringtip_L": ringtip_L,
                    "pinkytip_L": pinkytip_L,
                    "indexcarp_L": indexcarp_L,
                    "midcarp_L": midcarp_L,
                    "ringcarp_L": ringcarp_L,
                    "pinkycarp_L": pinkycarp_L,
                    "eye_head_L": eye_head_L,
                    "eye_head_R": eye_head_R,
                    "eye_tail_L": eye_tail_L,
                    "eye_tail_R": eye_tail_R,
                    "jaw_head": jaw_head,
                    "chin_head": chin_head,
                    "chin_001_head": chin_001_head,
                    "chin_001_tail": chin_001_tail,
                    "lip_head_B_L": lip_head_B_L,
                    "lip_001_head_B_L": lip_001_head_B_L,
                    "lip_head_T_L": lip_head_T_L,
                    "lip_001_head_T_L": lip_001_head_T_L,
                    "lips_corner_L": lips_corner_L,
                    "nose_004_tail": nose_004_tail,
                    "nose_004_head": nose_004_head,
                    "nose_003_head": nose_003_head,
                    "nose_002_head": nose_002_head,
                    "nose_001_head": nose_001_head,
                    "nose_head": nose_head,
                    "brow_003_head_T_L": brow_003_head_T_L,
                    "brow_002_head_T_L": brow_002_head_T_L,
                    "brow_001_head_T_L": brow_001_head_T_L,
                    "brow_head_T_L": brow_head_T_L,
                    "nose_001_head_L": nose_001_head_L,
                    "nose_head_L": nose_head_L,
                    "cheek_001_head_T_L": cheek_001_head_T_L,
                    "cheek_001_head_B_L": cheek_001_head_B_L,
                    "chin_head_L": chin_head_L,
                    "jaw_001_head_L": jaw_001_head_L,
                    "jaw_head_L": jaw_head_L,
                    "temple_head_L": temple_head_L,
                    "forehead_head_L": forehead_head_L,
                    "forehead_001_head_L": forehead_001_head_L,
                    "forehead_002_head_L": forehead_002_head_L,
                    "ear_head_L": ear_head_L,
                    "ear_001_head_L": ear_001_head_L,
                    "ear_002_head_L": ear_002_head_L,
                    "ear_003_head_L": ear_003_head_L,
                    "ear_004_head_L": ear_004_head_L,
                    "eyelid_head_B_L": eyelid_head_B_L,
                    "eyelid_001_head_B_L": eyelid_001_head_B_L,
                    "eyelid_002_head_B_L": eyelid_002_head_B_L,
                    "eyelid_003_head_B_L": eyelid_003_head_B_L,
                    "eyelid_head_T_L": eyelid_head_T_L,
                    "eyelid_001_head_T_L": eyelid_001_head_T_L,
                    "eyelid_002_head_T_L": eyelid_002_head_T_L,
                    "eyelid_003_head_T_L": eyelid_003_head_T_L,
                    "teeth_head_B": teeth_head_B,
                    "teeth_tail_B": teeth_tail_B,
                    "teeth_head_T": teeth_head_T,
                    "teeth_tail_T": teeth_tail_T,
                    "tongue_head": tongue_head,
                    "tongue_001_head": tongue_001_head,
                    "tongue_002_head": tongue_002_head,
                    "tongue_002_tail": tongue_002_tail,
                    "brow_head_B_L": brow_head_B_L,
                    "brow_001_head_B_L": brow_001_head_B_L,
                    "brow_002_head_B_L": brow_002_head_B_L,
                    "brow_003_head_B_L": brow_003_head_B_L,
                    "brow_003_tail_B_L": brow_003_tail_B_L,
                }
                return genesis_tools.rigifySetup(self, '2', vgroupData, setGenesis2MaleRolls)
        return {'CANCELLED'}
