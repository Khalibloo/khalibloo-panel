import bpy
import bmesh
import os
import json

context = bpy.context
obj = context.active_object
modeBackup = context.mode
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.mesh.reveal()
bm = bmesh.from_edit_mesh(bpy.data.meshes[obj.data.name])

def getVertList(obj, bm, vgroupName):
    """Returns a list of all vertices in a vertex group and their weights"""
    bpy.ops.object.mode_set(mode='EDIT')
    vertCount = len(obj.data.vertices.values())
    vertList = []
    bpy.ops.mesh.select_all(action='DESELECT')
    bpy.ops.object.vertex_group_set_active(group=vgroupName)
    bpy.ops.object.vertex_group_select()
    
    bm.verts.ensure_lookup_table()
    for i in range(0, vertCount):
        if (bm.verts[i].select):
            vertList.append({"index": i, "weight": obj.vertex_groups[vgroupName].weight(i)})
            
    bpy.ops.mesh.select_all(action='DESELECT')
    return vertList

vgroups = []
for vgroup in obj.vertex_groups:
    vgroups.append({"name": vgroup.name, "vertices": getVertList(obj, bm, vgroup.name)})

#the 'x' parameter tells python to create and open file for writing
filepath = "C:/Path/To/Folder/metarig_vgroups.json"
if (os.path.exists(filepath)):
    f = open(filepath, 'w')
else:
    f = open(filepath, 'x')

f.write(json.dumps(vgroups, indent=4))
f.close()

bpy.ops.object.mode_set(mode=modeBackup)

